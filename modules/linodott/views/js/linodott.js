/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the GNU General Public License, version 3 (GPL-3.0).
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * @author    Li-Nó Design www.lino-design.com <contact@lino-design.com>
 * @copyright 2019 Li-Nó Design Lda
 * @license   https://opensource.org/licenses/GPL-3.0 GNU General Public License version 3
 */

$(document).ready(function () {


  if($('.checkall').length > 0) {
    $('input[type="checkbox"].checkall').click(function(){
      if($(this).prop("checked") == true) {
        $('.product-checklist').prop("checked", true);
      } else if ($(this).prop("checked") == false) {
        $('.product-checklist').prop("checked", false);
      }
    });
  }

  if($('.filter-container').length > 0) {

    $('.reset-product-filter').click(function() {
      $('#priceLow').val('');
      $('#priceUp').val('');
      $('#stockLow').val('');
      $('#stockUp').val('');
      $('#ean').val('');
      $('#reference').val('');
      $('#brand').val(null).trigger('change');
      $('#supplier').val(null).trigger('change');
      $('#category').val(null).trigger('change');
      return false;
    });

    $('.order-link').click(function () {
      var orderCol = $(this).data('ordercol');
      var orderWay = $(this).data('orderway');
      $('#orderCol').val(orderCol);
      $('#orderWay').val(orderWay);
      $('#productDottFilter').click();
      return false;
    });

    $('#resultsPerPage').change(function () {
      $('#productDottList').trigger("click");
    });

    // See combination list
    $('.see-combination').click(function () {
      var prodId = $(this).data('prodid');
      $(this).toggleClass('caret-open');
      $('tr.comb-prod-' + prodId).slideToggle(400);
      return false;
    });

    // parent check will check all the children
    $('.parent-check').change(function () {
      var prodId = $(this).data('prodid');
      if ($(this).is(":checked")) {
        $('.product-child-' + prodId).prop('checked', true);
      } else {
        $('.product-child-' + prodId).prop('checked', false);
      }
    });
    $('.product-checklist').change(function () {
      if (!$(this).is(":checked")) {
        $('.product-parent-' + $(this).data('parentid')).prop('checked', false);
      }
    });
    $('#brand').select2({
      placeholder: "Brand",
    });
    $('#supplier').select2({
      placeholder: "Supplier",
    });
    $('#category').select2({
      placeholder: "Categories",
    });
  }

});
