{**
* NOTICE OF LICENSE
*
* This file is licenced under the GNU General Public License, version 3 (GPL-3.0).
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* @author    Li-Nó Design www.lino-design.com <contact@lino-design.com>
* @copyright 2019 Li-Nó Design Lda
* @license   https://opensource.org/licenses/GPL-3.0 GNU General Public License version 3
*}

<div class="panel col-lg-12">
	<div class="panel-heading">
		{l s='Ficheiro de oferta' mod='linodott'}
	</div>
	<p>
		{l s='Comunique a seguinte URL "feed de oferta" ao seu gestor de conta Dott para eles poder sincronizar os estoques e preços com o sistema Dott:' mod='linodott'} <br>
		<strong>{$linoFeedOfertaURL|escape:'htmlall':'UTF-8'}</strong>
	</p>
</div>
<div class="panel col-lg-12">
	<div class="panel-heading">
		{l s='Ficheiro de catálogo' mod='linodott'}
	</div>
	<p>
		{l s='Comunique a seguinte URL "feed de catalog" ao seu gestor de conta Dott para eles poder sincronizar o seu catálogo com o sistema Dott:' mod='linodott'} <br>
		<strong>{$linoFeedCatalogURL|escape:'htmlall':'UTF-8'}</strong>
	</p>
</div>

<div class="panel col-lg-12">
	<div class="panel-heading">
		{l s='CRON Task' mod='linodott'}
	</div>
	<p>
		{l s='Configure esta "cron task" no seu servidor para gerar o ficheiro de oferta de maneira automatica todas as horas:' mod='linodott'} <br>
		<strong>{$linoCronDottPath|escape:'htmlall':'UTF-8'}</strong>
	</p>
	<p>
		{l s='Configure esta "cron task" no seu servidor para confirmar e/ou importar no Prestashop automaticamente as encomendas da DOTT todas as horas:' mod='linodott'} <br>
		<strong>{$linoOrderCronDottPath|escape:'htmlall':'UTF-8'}</strong>
	</p>
</div>

{if $isDebug == 1}
<div class="panel col-lg-12">
	<div class="panel-heading">
		{l s='DOTT values' mod='linodott'}
	</div>
	<p>
		{l s='Selected Value' mod='linodott'}
		{$currentSelectedValues|escape:'htmlall':'UTF-8'}<br>
		{l s='Selected Value From Configuration' mod='linodott'}
		{$selectedProductFromConf|escape:'htmlall':'UTF-8'}
	</p>
</div>
{/if}

<div class="panel col-lg-12">
	<div class="panel-heading">
      {l s='Contacte DOTT Marketplace' mod='linodott'}
	</div>
	<p>
      {l s='Se ainda não tem uma conta DOTT, entre em contacto com a equipa Dott:' mod='linodott'}
		sales@dott.pt
	</p>
</div>
<div class="clearfix"></div>