{**
* NOTICE OF LICENSE
*
* This file is licenced under the GNU General Public License, version 3 (GPL-3.0).
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* @author    Li-Nó Design www.lino-design.com <contact@lino-design.com>
* @copyright 2019 Li-Nó Design Lda
* @license   https://opensource.org/licenses/GPL-3.0 GNU General Public License version 3
*}


<header class="row">
	<div class="col-md-9">
	</div>
	<div class="col-md-3 text-right">
		<a class="btn btn-primary" href="index.php?controller=AdminDottRefunds&amp;dottaction=refundList&amp;token={$token|escape:'htmlall':'UTF-8'}" title="{l s='Go back to Dott refunds' mod='linodott'}">{l s='Go back to Dott refunds' mod='linodott'}</a>
	</div>
	{if $errorMsg and $errorMsg != ""}
		<div class="dottMsg dottError">
			{$errorMsg|escape:'htmlall':'UTF-8'}
		</div>
	{elseif $returnMsg and $returnMsg != ''}
		<div class="dottMsg dottNotice">
			{$returnMsg|escape:'htmlall':'UTF-8'}
		</div>
	{/if}
</header>

<div class="panel col-md-6">
	<div class="panel-heading">
		{l s='Refund detail' mod='linodott'} <span class="badge">{$data.id|escape:'htmlall':'UTF-8'}</span>
		<span class="panel-heading-action">
						<a class="list-toolbar-btn" href="javascript:location.reload();">
						<span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="Refresh"
						      data-html="true" data-placement="top">
							<i class="process-icon-refresh"></i>
						</span>
					  </a>
        </span>
	</div>

	<div class="form-horizontal">
		<div class="row">
			<label class="control-label col-lg-3">{l s='Id' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.id|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Merchant Order Id' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.merchantOrderId|escape:'htmlall':'UTF-8'}
				</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='OrderNr' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.orderNr|escape:'htmlall':'UTF-8'}
				</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Status' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.status|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Customer' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">
					{$data.customer.firstName|escape:'htmlall':'UTF-8'} {$data.customer.lastName|escape:'htmlall':'UTF-8'}<br>
					ID={$data.customer.id|escape:'htmlall':'UTF-8'}, username={$data.customer.username|escape:'htmlall':'UTF-8'}<br>
				</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Reason' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">
					{$data.reason|escape:'htmlall':'UTF-8'}
				</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Partial Refund' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">
					{$data.partialRefund|escape:'htmlall':'UTF-8'}
				</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Amount' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">
					{$data.amount|escape:'htmlall':'UTF-8'}
				</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Total refund amount' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">
					{$data.totalRefundAmount|escape:'htmlall':'UTF-8'}
				</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Creation date' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.createdOn|date_format|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>
	</div>
</div>

<div class="panel col-md-6">
	<div class="panel-heading">
		{l s='Return Item' mod='linodott'} <span class="badge">{$data.returnItemsRequest.returnId|escape:'htmlall':'UTF-8'}</span>
		<span class="panel-heading-action">
						<a class="list-toolbar-btn" href="javascript:location.reload();">
						<span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="Refresh"
						      data-html="true" data-placement="top">
							<i class="process-icon-refresh"></i>
						</span>
					  </a>
    </span>
	</div>

	<div class="form-horizontal">
		<div class="row">
			<label class="control-label col-lg-3">{l s='Return ID' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.returnItemsRequest.id|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Amount' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.returnItemsRequest.amount|escape:'htmlall':'UTF-8'}
				</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='offerId' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.returnItemsRequest.offerId|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='orderItemId' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">
					{$data.returnItemsRequest.orderItemId|escape:'htmlall':'UTF-8'}
				</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Creation date' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.returnItemsRequest.createdOn|date_format|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>
	</div>
</div>

<div class="panel col-lg-12 mt-2">
	<div class="panel-heading">{l s='Order items' mod='linodott'}<span class="badge">{$data.orderItems|@count|escape:'htmlall':'UTF-8'}</span>
		<span class="panel-heading-action"></span>
	</div>

	<div class="table-responsive-row clearfix">
		<table class="table customer">
			<thead>
			<tr class="nodrag nodrop">
				<th class="fixed-width-xs text-center"><span class="title_box">{l s='Order item Id' mod='linodott'}</span></th>
				<th class=""><span class="title_box">{l s='Quantity' mod='linodott'}</span></th>
				<th></th>
			</tr>
			</thead>

			<tbody>
			{if $data.orderItems|@count > 0}
				{foreach from=$data.orderItems item=prod}
					<tr class="odd">
						<td class="" >
							{$prod.orderItemId|escape:'htmlall':'UTF-8'}
						</td>
						<td class="" >
							{$prod.quantity|escape:'htmlall':'UTF-8'}
						</td>
					</tr>
				{/foreach}
			{else}
				<tr class="odd">
					<td colspan="8">{l s='No order item' mod='linodott'}</td>
				</tr>
			{/if}
			</tbody>
		</table>
	</div>
</div>