{**
* NOTICE OF LICENSE
*
* This file is licenced under the GNU General Public License, version 3 (GPL-3.0).
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* @author    Li-Nó Design www.lino-design.com <contact@lino-design.com>
* @copyright 2019 Li-Nó Design Lda
* @license   https://opensource.org/licenses/GPL-3.0 GNU General Public License version 3
*}

<header class="row">
	{if $errorMsg and $errorMsg != ""}
		<div class="dottMsg dottError">
			{$errorMsg|escape:'htmlall':'UTF-8'}
		</div>
	{elseif $returnMsg and $returnMsg != ''}
		<div class="dottMsg dottNotice">
			{$returnMsg|escape:'htmlall':'UTF-8'}
		</div>
	{/if}
	<div class="col-md-9">
		<ul class="dott-action">
			{if $data.status == 'Confirmed'}
			<li><a class="btn btn-primary" href="index.php?controller=AdminDottOrders&amp;orderId={$data.id|escape:'htmlall':'UTF-8'}&amp;dottaction=orderprocess&amp;token={$token|escape:'htmlall':'UTF-8'}" title="{l s='Process' mod='linodott'}">{l s='Processar' mod='linodott'}</a></li>
			{/if}
			{if $data.status == 'AuthorizedPayment'}
			<li><a class="btn btn-primary" href="index.php?controller=AdminDottOrders&amp;orderId={$data.id|escape:'htmlall':'UTF-8'}&amp;dottaction=orderconfirm&amp;token={$token|escape:'htmlall':'UTF-8'}" title="{l s='Confirm' mod='linodott'}">{l s='Confirmar' mod='linodott'}</a></li>
			<li><a class="btn btn-primary" href="index.php?controller=AdminDottOrders&amp;orderId={$data.id|escape:'htmlall':'UTF-8'}&amp;dottaction=ordercancel&amp;token={$token|escape:'htmlall':'UTF-8'}" title="{l s='Cancel' mod='linodott'}">{l s='Cancelar' mod='linodott'}</a></li>
			{/if}
		</ul>
	</div>
	<div class="col-md-3 text-right">
		<a class="btn btn-primary" href="index.php?controller=AdminDottOrders&amp;dottaction=orderList&amp;token={$token|escape:'htmlall':'UTF-8'}" title="{l s='Voltar as encomendas' mod='linodott'}">{l s='Voltar as encomendas' mod='linodott'}</a>
	</div>
</header>

<div class="panel col-md-6">
	<div class="panel-heading">
		{l s='Detalhe da encomenda' mod='linodott'}<span class="badge">{$data.orderNr|escape:'htmlall':'UTF-8'}</span>
		<span class="panel-heading-action">
						<a class="list-toolbar-btn" href="javascript:location.reload();">
						<span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="Refresh"
						      data-html="true" data-placement="top">
							<i class="process-icon-refresh"></i>
						</span>
					  </a>
        </span>
	</div>

	<div class="form-horizontal">
		<div class="row">
			<label class="control-label col-lg-3">ID</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.id|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='OrderNr' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.orderNr|escape:'htmlall':'UTF-8'}
				</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Status' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.status|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Customer' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">
					{$data.customer.firstName|escape:'htmlall':'UTF-8'} {$data.customer.lastName|escape:'htmlall':'UTF-8'}<br>
            {l s='Id' mod='linodott'}={$data.customer.id|escape:'htmlall':'UTF-8'}, username={$data.customer.username|escape:'htmlall':'UTF-8'}<br>
				</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Shipping Address' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">
					{$data.shippingAddress.name|escape:'htmlall':'UTF-8'}<br>
					{$data.shippingAddress.firstName|escape:'htmlall':'UTF-8'} {$data.shippingAddress.lastName|escape:'htmlall':'UTF-8'}<br>
					{$data.shippingAddress.address|escape:'htmlall':'UTF-8'}<br>
					{if $data.shippingAddress.address2|escape:'htmlall':'UTF-8'}
						{$data.shippingAddress.address2|escape:'htmlall':'UTF-8'}<br>
					{/if}
					{$data.shippingAddress.zipcode|escape:'htmlall':'UTF-8'} {$data.shippingAddress.city|escape:'htmlall':'UTF-8'}<br>
					{$data.shippingAddress.country|escape:'htmlall':'UTF-8'}<br>
					{$data.shippingAddress.fiscalNumber|escape:'htmlall':'UTF-8'}
				</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Billing address' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">
					{$data.billingAddress.name|escape:'htmlall':'UTF-8'}<br>
					{$data.billingAddress.firstName|escape:'htmlall':'UTF-8'} {$data.billingAddress.lastName|escape:'htmlall':'UTF-8'}<br>
					{$data.billingAddress.address|escape:'htmlall':'UTF-8'}<br>
					{if $data.billingAddress.address2}
						{$data.billingAddress.address2|escape:'htmlall':'UTF-8'}<br>
					{/if}
					{$data.billingAddress.zipcode|escape:'htmlall':'UTF-8'} {$data.billingAddress.city|escape:'htmlall':'UTF-8'}<br>
					{$data.billingAddress.country|escape:'htmlall':'UTF-8'}<br>
					{$data.billingAddress.fiscalNumber|escape:'htmlall':'UTF-8'}
				</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Creation date' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.createdOn|date_format|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Modification date' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.modifiedOn|date_format|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>
	</div>

</div>

<div class="panel col-md-6">
	<div class="panel-heading">{l s='Payment details' mod='linodott'} <span class="badge">{$data.merchantOrderPayment.itemsTotal|escape:'htmlall':'UTF-8'} €</span>
		<span class="panel-heading-action">
		  <a class="list-toolbar-btn" href="javascript:location.reload();">
			  <span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="Refresh list" data-html="true" data-placement="top">
							<i class="process-icon-refresh"></i>
			  </span>
		  </a>
    </span>
	</div>

	<div class="form-horizontal">
		<div class="row">
			<label class="control-label col-lg-3">{l s='Method' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.merchantOrderPayment.paymentMethod|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Total' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.merchantOrderPayment.itemsTotal|escape:'htmlall':'UTF-8'}  €</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Total(tax excl)' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.merchantOrderPayment.itemsTotalTaxExcl|escape:'htmlall':'UTF-8'}  €</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Billed' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.merchantOrderPayment.billed|escape:'htmlall':'UTF-8'}  €</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Billed(tax excl)' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.merchantOrderPayment.billedTaxExcl|escape:'htmlall':'UTF-8'}  €</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Shipping' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.merchantOrderPayment.shipping|escape:'htmlall':'UTF-8'}  €</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Shipping(tax excl)' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.merchantOrderPayment.shippingTaxExcl|escape:'htmlall':'UTF-8'}  €</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Earnings' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.merchantOrderPayment.earnings|escape:'htmlall':'UTF-8'}  €</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Earnings(tax excl)' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.merchantOrderPayment.earningsTaxExcl|escape:'htmlall':'UTF-8'}  €</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Marketplace Commission' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.merchantOrderPayment.marketplaceCommission|escape:'htmlall':'UTF-8'}  €</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Marketplace Commission(tax excl)' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.merchantOrderPayment.marketplaceCommissionTaxExcl|escape:'htmlall':'UTF-8'}  €</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Marketplace Discount' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.merchantOrderPayment.marketplaceDiscount|escape:'htmlall':'UTF-8'}  €</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Marketplace Discount(tax excl)' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.merchantOrderPayment.marketplaceDiscountTaxExcl|escape:'htmlall':'UTF-8'}  €</p>
			</div>
		</div>
	</div>

</div>

<div class="panel col-lg-12 mt-2">
	<div class="panel-heading">{l s='Produtos' mod='linodott'} <span class="badge">{$data.orderItems|@count|escape:'htmlall':'UTF-8'}</span>
		<span class="panel-heading-action"></span>
	</div>

	<div class="table-responsive-row clearfix">
		<table class="table customer">
			<thead>
			<tr class="nodrag nodrop">
				<th class="fixed-width-xs text-center"><span class="title_box">Id</span></th>
				<th class=""><span class="title_box">{l s='Image' mod='linodott'}</span></th>
				<th class=""><span class="title_box">{l s='Name' mod='linodott'}</span></th>
				<th class=""><span class="title_box">{l s='Status' mod='linodott'}</span></th>
				<th class=""><span class="title_box">{l s='Sku' mod='linodott'}</span></th>
				<th class=""><span class="title_box">{l s='Quantity' mod='linodott'}</span></th>
				<th class=""><span class="title_box">{l s='Amount' mod='linodott'}</span></th>
				<th class=""><span class="title_box">{l s='Amount (tax excl)' mod='linodott'}</span></th>
				<th class=""><span class="title_box">{l s='Marketplace discount' mod='linodott'}</span></th>
				<th></th>
			</tr>
			</thead>

			<tbody>
			{if $data.orderItems|@count > 0}
				{foreach from=$data.orderItems item=prod}
					<tr class="odd">
						<td class="" >
							{$prod.id|escape:'htmlall':'UTF-8'}
						</td>
						<td class=" image-col" >
							<img src="{$prod.imageUrl|escape:'htmlall':'UTF-8'}" alt="{$prod.name|escape:'htmlall':'UTF-8'}">
						</td>
						<td class="" >
							{$prod.name|escape:'htmlall':'UTF-8'}
						</td>
						<td class="" >
							{$prod.statusDesc|escape:'htmlall':'UTF-8'}
						</td>
						<td class="" >
							{$prod.sku|escape:'htmlall':'UTF-8'}
						</td>
						<td class="" >
							{$prod.quantity|escape:'htmlall':'UTF-8'}
						</td>
						<td class="" >
							{$prod.amount|escape:'htmlall':'UTF-8'} €
						</td>
						<td class="" >
							{$prod.amountTaxTaxExcl|escape:'htmlall':'UTF-8'} €
						</td>
						<td class="" >
							{$prod.marketplaceDiscount|escape:'htmlall':'UTF-8'} €
						</td>
					</tr>
				{/foreach}
			{else}
				<tr class="odd">
					<td colspan="8">{l s='Não ha produto' mod='linodott'}</td>
				</tr>
			{/if}
			</tbody>
		</table>
	</div>
</div>


<div class="panel col-lg-12 mt-2">
	<div class="panel-heading">{l s='Documentos' mod='linodott'}<span class="badge">{$documents|@count|escape:'htmlall':'UTF-8'}</span>
		<span class="panel-heading-action"></span>
	</div>

	<div class="table-responsive-row clearfix">
		<table class="table customer">
			<thead>
			<tr class="nodrag nodrop">
				<th class=""><span class="title_box">{l s='Id' mod='linodott'}</span></th>
				<th class=""><span class="title_box">{l s='Type' mod='linodott'}</span></th>
				<th class=""><span class="title_box">{l s='Filename' mod='linodott'}</span></th>
				<th class=""><span class="title_box">{l s='Creation date' mod='linodott'}</span></th>
				<th class=""><span class="title_box">{l s='View' mod='linodott'}</span></th>
				<th></th>
			</tr>
			</thead>

			<tbody>
			{if $documents|@count > 0}
				{foreach from=$documents item=doc}
					<tr class="odd">
						<td class="" >
							{$doc.id|escape:'htmlall':'UTF-8'}
						</td>
						<td class=" " >
							{$doc.type|escape:'htmlall':'UTF-8'}
						</td>
						<td class="" >
							{$doc.filename|escape:'htmlall':'UTF-8'}
						</td>
						<td class="" >
							{$doc.createdOn|date_format|escape:'htmlall':'UTF-8'}
						</td>
						<td class="text-right">
							<div class="btn-group-action">
								<div class="btn-group pull-right">
									<a href="index.php?controller=AdminDottOrders&amp;orderId={$data.id|escape:'htmlall':'UTF-8'}&amp;docName={$doc.filename|escape:'htmlall':'UTF-8'}&amp;docId={$doc.id|escape:'htmlall':'UTF-8'}&amp;dottaction=viewdocument&amp;token={$token|escape:'htmlall':'UTF-8'}" title="Ver" class="view btn btn-default">
										<i class="icon-eye"></i> {l s='Ver' mod='linodott'}
									</a>
								</div>
							</div>
						</td>

					</tr>
				{/foreach}
			{else}
				<tr class="odd">
					<td colspan="8">{l s='Não ha documento' mod='linodott'}</td>
				</tr>
			{/if}
			</tbody>
		</table>
	</div>
</div>
{if $ownInvoice == 1 }
<div class="panel col-lg-12 mt-2">
	<div class="panel-heading">{l s='Upload documento' mod='linodott'}<span class="badge">{$documents|@count|escape:'htmlall':'UTF-8'}</span>
		<span class="panel-heading-action"></span>
	</div>
	<div class="linoInlineForm">
		<form action="{$smarty.server.REQUEST_URI|escape:'html':'UTF-8'}" method="post" id="uploadDocDottForm" enctype="multipart/form-data">
			<input type="hidden" name="dottaction" value="uploaddocument">
			<input type="hidden" name="orderId" value="{$data.id|escape:'htmlall':'UTF-8'}">
			<select id="documentType" required name="documentType" >
				<option value="invoice">{l s='Invoice' mod='linodott'}</option>
				<option value="CreditNote">{l s='Credit Note' mod='linodott'}</option>
			</select>
			<input type="file" id="uploadDottFile" required name="uploadDottFile" >
			<input type="hidden" name="MAX_FILE_SIZE" value="2000000" />
			<input type="submit" id="uploadDocDott" name="uploadDocDott" class="btn btn-primary" value="{l s='Enviar' mod='linodott'}">
		</form>

	</div>
</div>
{/if}