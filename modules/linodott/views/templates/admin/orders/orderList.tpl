{**
* NOTICE OF LICENSE
*
* This file is licenced under the GNU General Public License, version 3 (GPL-3.0).
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* @author    Li-Nó Design www.lino-design.com <contact@lino-design.com>
* @copyright 2019 Li-Nó Design Lda
* @license   https://opensource.org/licenses/GPL-3.0 GNU General Public License version 3
*}

<header class="row">
	{if $errorMsg and $errorMsg != ""}
		<div class="dottMsg alert alert-danger">
			{$errorMsg|escape:'htmlall':'UTF-8'}
		</div>
	{elseif $returnMsg and $returnMsg != ''}
		<div class="dottMsg alert alert-success">
			{$returnMsg|escape:'htmlall':'UTF-8'}
		</div>
	{/if}
</header>

<div class="panel col-lg-12">
	<div class="panel-heading">
		{l s='Gestão das encomendas' mod='linodott'} {if $data and $data.data and $data.data|@count > 0} <span class="badge">{$data.data|@count|escape:'htmlall':'UTF-8'}</span> {/if}
		<span class="panel-heading-action">
						<a class="list-toolbar-btn" href="javascript:location.reload();">
						<span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="Refresh list"
						      data-html="true" data-placement="top">
							<i class="process-icon-refresh"></i>
						</span>
					  </a>
        </span>
	</div>



	<div class="table-responsive-row clearfix">
		<table class="table customer">
			<thead>
			<tr class="nodrag nodrop">
				<th class="fixed-width-xs text-center">
						<span class="title_box">{l s='Id' mod='linodott'}</span>
				</th>
				<th class=""><span class="title_box">{l s='OrderNr' mod='linodott'}</span></th>
				<th class=""><span class="title_box">{l s='Status' mod='linodott'}</span></th>
				<th class=""><span class="title_box">{l s='Products' mod='linodott'}</span></th>
				<th class=""><span class="title_box">{l s='Customer' mod='linodott'}</span></th>
				<th class=""><span class="title_box">{l s='Total' mod='linodott'}</span></th>
				<th class=""><span class="title_box">{l s='Creation date' mod='linodott'}</span></th>
				<th class=""><span class="title_box">{l s='Modification date' mod='linodott'}</span></th>
				<th></th>
			</tr>
			</thead>

			<tbody>
			{if $data and $data.data and $data.data|@count > 0}
				{foreach from=$data.data item=order}
					<tr class="odd">
						<td class="pointer" onclick="document.location = 'index.php?controller=AdminDottOrders&amp;orderId={$order.id|escape:'htmlall':'UTF-8'}&amp;dottaction=vieworder&amp;token={$token|escape:'htmlall':'UTF-8'}'">
							{$order.id|escape:'htmlall':'UTF-8'}
						</td>
						<td class="pointer" onclick="document.location = 'index.php?controller=AdminDottOrders&amp;orderId={$order.id|escape:'htmlall':'UTF-8'}&amp;dottaction=vieworder&amp;token={$token|escape:'htmlall':'UTF-8'}'">
							{$order.orderNr|escape:'htmlall':'UTF-8'}
						</td>
						<td class="pointer" onclick="document.location = 'index.php?controller=AdminDottOrders&amp;orderId={$order.id|escape:'htmlall':'UTF-8'}&amp;dottaction=vieworder&amp;token={$token|escape:'htmlall':'UTF-8'}'">
							{$order.status|escape:'htmlall':'UTF-8'}
						</td>
						<td class="pointer" onclick="document.location = 'index.php?controller=AdminDottOrders&amp;orderId={$order.id|escape:'htmlall':'UTF-8'}&amp;dottaction=vieworder&amp;token={$token|escape:'htmlall':'UTF-8'}'">
							{foreach from=$order.orderItems item=prod}
								{$prod.name|escape:'htmlall':'UTF-8'} ({$prod.quantity|escape:'htmlall':'UTF-8'})<br>
							{/foreach}
						</td>
						<td class="pointer" onclick="document.location = 'index.php?controller=AdminDottOrders&amp;orderId={$order.id|escape:'htmlall':'UTF-8'}&amp;dottaction=vieworder&amp;token={$token|escape:'htmlall':'UTF-8'}'">
							{$order.customer.firstName|escape:'htmlall':'UTF-8'}
							{$order.customer.lastName|escape:'htmlall':'UTF-8'}
						</td>
						<td class="pointer" onclick="document.location = 'index.php?controller=AdminDottOrders&amp;orderId={$order.id|escape:'htmlall':'UTF-8'}&amp;dottaction=vieworder&amp;token={$token|escape:'htmlall':'UTF-8'}'">
							{$order.merchantOrderPayment.itemsTotal|escape:'htmlall':'UTF-8'} €
						</td>
						<td class="pointer" onclick="document.location = 'index.php?controller=AdminDottOrders&amp;orderId={$order.id|escape:'htmlall':'UTF-8'}&amp;dottaction=vieworder&amp;token={$token|escape:'htmlall':'UTF-8'}'">
							{$order.createdOn|date_format|escape:'htmlall':'UTF-8'}
						</td>
						<td class="pointer" onclick="document.location = 'index.php?controller=AdminDottOrders&amp;orderId={$order.id|escape:'htmlall':'UTF-8'}&amp;dottaction=vieworder&amp;token={$token|escape:'htmlall':'UTF-8'}'">
							{$order.modifiedOn|date_format|escape:'htmlall':'UTF-8'}
						</td>
						<td class="text-right">
							<div class="btn-group-action">
								<div class="btn-group pull-right">
									<a href="index.php?controller=AdminDottOrders&amp;orderId={$order.id|escape:'htmlall':'UTF-8'}&amp;dottaction=vieworder&amp;token={$token|escape:'htmlall':'UTF-8'}" title="{l s='View' mod='linodott'}" class="view btn btn-default">
										<i class="icon-eye"></i> {l s='Ver' mod='linodott'}
									</a>
									<button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
										<i class="icon-caret-down"></i>&nbsp;
									</button>
									<ul class="dropdown-menu">
										<li>
											<a href="index.php?controller=AdminDottOrders&amp;orderId={$order.id|escape:'htmlall':'UTF-8'}&amp;dottaction=vieworder&amp;token={$token|escape:'htmlall':'UTF-8'}" title="{l s='View' mod='linodott'}">
												<i class="icon-search-plus"></i> {l s='Ver' mod='linodott'}
											</a>
										</li>
										<li class="divider">
										</li>
										{if $order.status == 'Confirmed'}
										<li>
											<a href="index.php?controller=AdminDottOrders&amp;orderId={$order.id|escape:'htmlall':'UTF-8'}&amp;dottaction=orderprocess&amp;token={$token|escape:'htmlall':'UTF-8'}" title="{l s='Process' mod='linodott'}" class="{l s='Process' mod='linodott'}">
												{l s='Processar' mod='linodott'}
											</a>
										</li>
										{/if}
										{if $order.status == 'AuthorizedPayment'}
										<li>
											<a href="index.php?controller=AdminDottOrders&amp;orderId={$order.id|escape:'htmlall':'UTF-8'}&amp;dottaction=orderconfirm&amp;token={$token|escape:'htmlall':'UTF-8'}" title="{l s='Confirm' mod='linodott'}" class="{l s='Confirm' mod='linodott'}">
												{l s='Confirmar' mod='linodott'}
											</a>
										</li>
										<li>
											<a href="index.php?controller=AdminDottOrders&amp;orderId={$order.id|escape:'htmlall':'UTF-8'}&amp;dottaction=ordercancel&amp;token={$token|escape:'htmlall':'UTF-8'}" title="{l s='Cancel' mod='linodott'}" class="{l s='Cancel' mod='linodott'}">
												{l s='Cancelar' mod='linodott'}
											</a>
										</li>
										{/if}
									</ul>
								</div>
							</div>
						</td>
					</tr>
				{/foreach}
			{else}
				<tr class="odd">
					<td colspan="8">{l s='Ainda não ha encomendas.' mod='linodott'}</td>
				</tr>
			{/if}
			</tbody>
		</table>
		<div class="lino-pagination">
			{if $nbPages > 0 }
				<ul class="dott-pagination">
					{assign var="paginationBreak" value=10}
					{if $nbPages > $paginationBreak}
						<li><a href="index.php?controller=AdminDottOrders&amp;page=1&amp;token={$token|escape:'htmlall':'UTF-8'}"><<</a></li>
					{/if}
					{if $page > 1}
						<li><a href="index.php?controller=AdminDottOrders&amp;page={($page-1)|escape:'htmlall':'UTF-8'}&amp;token={$token|escape:'htmlall':'UTF-8'}"><</a></li>
					{/if}
					{if $nbPages > $paginationBreak}
						{for $cpt=($page-3) to $page}
							{if $cpt > 0}
								<li {if $page == $cpt}class="active"{/if} ><a href="index.php?controller=AdminDottOrders&amp;page={$cpt|escape:'htmlall':'UTF-8'}&amp;token={$token|escape:'htmlall':'UTF-8'}">{$cpt|escape:'htmlall':'UTF-8'}</a></li>
							{/if}
						{/for}
						{for $cpt=($page+1) to $page+2}
							{if $cpt <= $pageMax}
								<li {if $page == $cpt}class="active"{/if} ><a href="index.php?controller=AdminDottOrders&amp;page={$cpt|escape:'htmlall':'UTF-8'}&amp;token={$token|escape:'htmlall':'UTF-8'}">{$cpt|escape:'htmlall':'UTF-8'}</a></li>
							{/if}
						{/for}
					{else}
						{for $cpt=1 to $nbPages}
							<li {if $page == $cpt}class="active"{/if} ><a href="index.php?controller=AdminDottOrders&amp;page={$cpt|escape:'htmlall':'UTF-8'}&amp;token={$token|escape:'htmlall':'UTF-8'}">{$cpt|escape:'htmlall':'UTF-8'}</a></li>
						{/for}
					{/if}
					{if $page < $nbPages}
						<li><a href="index.php?controller=AdminDottOrders&amp;page={($page+1)|escape:'htmlall':'UTF-8'}&amp;token={$token|escape:'htmlall':'UTF-8'}">></a></li>
					{/if}
					{if $nbPages > $paginationBreak}
						<li><a href="index.php?controller=AdminDottOrders&amp;page={$nbPages|escape:'htmlall':'UTF-8'}&amp;token={$token|escape:'htmlall':'UTF-8'}">>></a></li>
					{/if}
				</ul>
			{/if}
		</div>
	</div>

	{if $isDebug == 1}
		<div class="panel col-lg-12">
			<div class="panel-heading">
				{l s='Logs' mod='linodott'}
			</div>
			{if $logFiles|@count > 0}
				<h2>Last debug files</h2>
				<ul class="offerFeedFiles">
					{foreach from=$logFiles key=filename item=fileDate}
						<li><a href="{$base_url|escape:'htmlall':'UTF-8'}modules/linodott/logs/orderList/{$filename|escape:'htmlall':'UTF-8'}" target="_blank">{$filename|escape:'htmlall':'UTF-8'}</a> - {$fileDate|date_format:"%D %H:%M:%S"|escape:'htmlall':'UTF-8'}</li>
					{/foreach}
				</ul>
			{/if}
		</div>
	{/if}
</div>

