{**
* NOTICE OF LICENSE
*
* This file is licenced under the GNU General Public License, version 3 (GPL-3.0).
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* @author    Li-Nó Design www.lino-design.com <contact@lino-design.com>
* @copyright 2019 Li-Nó Design Lda
* @license   https://opensource.org/licenses/GPL-3.0 GNU General Public License version 3
*}

<header class="row">
    {if $errorMsg and $errorMsg != ""}
        <div class="dottMsg alert alert-danger">
            {$errorMsg|escape:'htmlall':'UTF-8'}
        </div>
    {elseif $returnMsg and $returnMsg != ''}
        <div class="dottMsg alert alert-success">
            {$returnMsg|escape:'htmlall':'UTF-8'}
        </div>
    {/if}
</header>

<form action="{$smarty.server.REQUEST_URI|escape:'html':'UTF-8'}" method="post" id="productDottListForm" enctype="multipart/form-data">
    <input type="hidden" id="orderCol" name="orderCol" value="{$orderCol|escape:'html':'UTF-8'}">
    <input type="hidden" id="orderWay" name="orderWay" value="{$orderWay|escape:'html':'UTF-8'}">
    <div class="panel col-lg-12">
        <div class="panel-heading">
            {l s='Filtro dos produtos a sincronizar' mod='linodott'} <span class="badge"></span>
        </div>

        <div class="filter-container container">
            <div class="row">
                <div class="col-md-3"><input type="text" placeholder="{l s='EAN' mod='linodott'}" value="{$ean|escape:'html':'UTF-8'}" id="ean" name="ean" ></div>
                <div class="col-md-3"><input type="text" placeholder="{l s='Reference' mod='linodott'}" value="{$reference|escape:'html':'UTF-8'}" id="reference" name="reference" ></div>
                <div class="col-md-3">
                    <select name="productEnabled" placeholder="Produtos activos">
                        <option value="all" {if $productEnabled == "all"} selected {/if}>Todos</option>
                        <option value="active" {if $productEnabled == "active"} selected {/if}>Enabled</option>
                        <option value="inactive" {if $productEnabled == "inactive"} selected {/if}>Disabled</option>
                    </select>
                </div>
                <div class="col-md-3"></div>
            </div>
            <div class="row">
                <div class="col-md-3"><input type="number" placeholder="{l s='Price min' mod='linodott'}" value="{$priceLow|escape:'html':'UTF-8'}" id="priceLow" name="priceLow" step="0.01" min="0.00"></div>
                <div class="col-md-3"><input type="number" placeholder="{l s='Price max' mod='linodott'}" value="{$priceUp|escape:'html':'UTF-8'}" id="priceUp" name="priceUp" step="0.01" min="0.00"></div>
                <div class="col-md-3"><input type="number" placeholder="{l s='Stock min' mod='linodott'}" value="{$stockLow|escape:'html':'UTF-8'}" id="stockLow" name="stockLow" step="1" min="0"></div>
                <div class="col-md-3"><input type="number" placeholder="{l s='Stock max' mod='linodott'}" value="{$stockUp|escape:'html':'UTF-8'}" id="stockUp" name="stockUp" step="1" min="0"></div>
            </div>
            <div class="row mt-2">
                <div class="col-md-3">
                    <select name="brand[]" id="brand" multiple>
{*                        <option value="" class="select-placeholder">Brand</option>*}
                        {foreach from=$brands item=brandIt}
                            <option {if in_array($brandIt.id_manufacturer, $brand)} selected {/if}  value="{$brandIt.id_manufacturer|escape:'html':'UTF-8'}">{$brandIt.name|escape:'html':'UTF-8'}</option>
                        {/foreach}
                    </select>
                </div>
                <div class="col-md-3">
                    <select name="supplier[]" id="supplier" multiple>
{*                        <option value="" class="select-placeholder">{l s='Supplier' mod='linodott'}</option>*}
                        {foreach from=$suppliers item=supplierIt}
                            <option {if in_array($supplierIt.id_supplier, $supplier)} selected {/if} value="{$supplierIt.id_supplier|escape:'html':'UTF-8'}">{$supplierIt.name|escape:'html':'UTF-8'}</option>
                        {/foreach}
                    </select>
                </div>
                <div class="col-md-3">
                    <select name="category[]" id="category" multiple>
{*                        <option value="" class="select-placeholder">Category</option>*}
                        {foreach from=$categories item=cat}
                            <option {if in_array($cat.id_category, $category)} selected {/if} value="{$cat.id_category|escape:'html':'UTF-8'}">{$cat.name|escape:'html':'UTF-8'}</option>
                        {/foreach}
                    </select>
                </div>
                <div class="col-md-3">
                    <a href="#" class=" reset-product-filter btn btn-primary">{l s='Reset' mod='linodott'}</a>
                    <input type="submit" id="productDottFilter" name="productDottFilter" class="btn btn-primary" value="{l s='Filtrar' mod='linodott'}">

                </div>
            </div>
        </div>
    </div>


    <div class="panel col-lg-12">
        <div class="panel-heading">
            {l s='Produtos a sincronizar' mod='linodott'} <span class="badge">{$totalResults|escape:'htmlall':'UTF-8'}</span>
            <span class="panel-heading-action">
                <input type="submit" id="productDottSaveNotFiltered" name="productDottSaveNotFiltered" class="btn btn-primary dott-button-r"
                       value="{l s='Deselecionar os produtos filtrados' mod='linodott'}">
                <input type="submit" id="productDottSaveFiltered" name="productDottSaveFiltered" class="btn btn-primary dott-button-r"
                       value="{l s='Selecionar os produtos filtrados' mod='linodott'}">
                <select name="resultsPerPage" id="resultsPerPage">
                    <option value="5" {if $resultsPerPage == 5} selected {/if}>5</option>
                    <option value="10" {if $resultsPerPage == 10} selected {/if}>10</option>
                    <option value="20" {if $resultsPerPage == 20} selected {/if}>20</option>
                    <option value="30" {if $resultsPerPage == 30} selected {/if}>30</option>
                    <option value="40" {if $resultsPerPage == 40} selected {/if}>40</option>
                    <option value="50" {if $resultsPerPage == 50} selected {/if}>50</option>
                    <option value="50" {if $resultsPerPage == 100} selected {/if}>100</option>
                </select>
                <a class="list-toolbar-btn" href="javascript:location.reload();">
                    <span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="Refresh list" data-html="true" data-placement="top">
                        <i class="process-icon-refresh"></i>
                    </span>
                </a>
	        </span>
        </div>

        <div class="table-responsive-row clearfix">
            <table class="table customer">
                <thead>
                <tr class="nodrag nodrop">
                    <th class="fixed-width-xs text-center">
                        <span class="title_box">{l s='Id' mod='linodott'}<a href="#" class="order-link {if $orderCol == "id_product" and $orderWay == "desc" } order-link-active {/if}" data-ordercol="id_product" data-orderway="desc" ><i class="icon-caret-down"></i></a><a href="#" class="order-link {if $orderCol == "id_product" and $orderWay == "asc" } order-link-active {/if}" data-ordercol="id_product" data-orderway="asc" ><i class="icon-caret-up"></i></a></span>
                    </th>
                    <th class=""><span class="title_box">{l s='Name' mod='linodott'}<a href="#" class="order-link {if $orderCol == "name" and $orderWay == "desc" } order-link-active {/if}" data-ordercol="name" data-orderway="desc" ><i class="icon-caret-down"></i></a><a href="#" class="order-link {if $orderCol == "name" and $orderWay == "asc" } order-link-active {/if}" data-ordercol="name" data-orderway="asc" ><i class="icon-caret-up"></i></a></span></th>
                    <th class=""><span class="title_box">{l s='Comb' mod='linodott'}</span></th>
                    <th class=""><span class="title_box">{l s='Image' mod='linodott'}</span></th>
                    <th class=""><span class="title_box">{l s='Ref/Upc' mod='linodott'}<a href="#" class="order-link {if $orderCol == "reference" and $orderWay == "desc" } order-link-active {/if}" data-ordercol="reference" data-orderway="desc" ><i class="icon-caret-down"></i></a><a href="#" class="order-link {if $orderCol == "reference" and $orderWay == "asc" } order-link-active {/if}" data-ordercol="reference" data-orderway="asc" ><i class="icon-caret-up"></i></a></span></th>
                    <th class=""><span class="title_box">{l s='Ean' mod='linodott'}<a href="#" class="order-link {if $orderCol == "ean13" and $orderWay == "desc" } order-link-active {/if}" data-ordercol="ean13" data-orderway="desc" ><i class="icon-caret-down"></i></a><a href="#" class="order-link {if $orderCol == "ean13" and $orderWay == "asc" } order-link-active {/if}" data-ordercol="ean13" data-orderway="asc" ><i class="icon-caret-up"></i></a></span></th>
                    <th class=""><span class="title_box">{l s='Quantity' mod='linodott'}</span></th>
                    <th class=""><span class="title_box">{l s='Price' mod='linodott'}<a href="#" class="order-link {if $orderCol == "price" and $orderWay == "desc" } order-link-active {/if}" data-ordercol="price" data-orderway="desc" ><i class="icon-caret-down"></i></a><a href="#" class="order-link {if $orderCol == "price" and $orderWay == "asc" } order-link-active {/if}" data-ordercol="price" data-orderway="asc" ><i class="icon-caret-up"></i></a></span></th>
                    <th class=""><span class="title_box">{l s='Discount' mod='linodott'}</span></th>
                    <th class=""><span class="title_box">{l s='Rate' mod='linodott'}</span></th>
                    <th class=""><span class="title_box">{l s='Available' mod='linodott'}</span></th>
                    <th class=""><span class="title_box">{l s='Open Combinations' mod='linodott'}</span></th>
                    <th><input type="checkbox" class="checkall"></th>
                </tr>
                </thead>

                <tbody>
                {if $data|@count > 0}
                    {foreach from=$data item=prod}
                        <tr class="odd">
                            <td class="">
                                {$prod.id_product|escape:'htmlall':'UTF-8'}
                            </td>
                            <td class="">
                                {$prod.name|escape:'htmlall':'UTF-8'}
                            </td>
                            <td class="">
                            </td>
                            <td class="image-col">
                                <img src="{$link->getImageLink($prod.link_rewrite, $prod.image.id_image, 'home_default')|escape:'htmlall':'UTF-8'}"
                                     alt="{$prod.name|escape:'htmlall':'UTF-8'}">
                            </td>
                            <td class="">
                                {l s='Ref' mod='linodott'}: {$prod.reference|escape:'htmlall':'UTF-8'}<br>
                                {l s='Upc' mod='linodott'}: {$prod.upc|escape:'htmlall':'UTF-8'}<br>
                            </td>
                            <td class="">
                                {$prod.ean13|escape:'htmlall':'UTF-8'}<br>
                            </td>
                            <td class="">
                                {$prod.quantity|escape:'htmlall':'UTF-8'}
                            </td>
                            <td class="">
                                {$prod.price|escape:'htmlall':'UTF-8'}
                            </td>
                            <td class="">
                                {$prod.quantity_discount|escape:'htmlall':'UTF-8'}
                            </td>
                            <td class="">
                                {$prod.rate|escape:'htmlall':'UTF-8'}
                            </td>
                            <td class="">
                                {$prod.available_now|escape:'htmlall':'UTF-8'}
                            </td>
                            <td class="center">
                                {if $prod.combinations && $prod.combinations|@count > 0}
                                <a class="see-combination" data-prodid="{$prod.id_product|escape:'htmlall':'UTF-8'}"
                                   href="#">
                                    <i class="icon-caret-left"></i>
                                    <i class="icon-caret-down"></i>
                                </a>
                                {/if}
                            </td>
                            <td class="">
                                <input type="checkbox"
                                       value="{$prod.id_product|escape:'htmlall':'UTF-8'}" {if $prod.id_product|in_array:$productList} checked {/if}
                                       name="product_dott[]" class="product-checklist parent-check product-parent-{$prod.id_product|escape:'htmlall':'UTF-8'}"
                                       data-prodid="{$prod.id_product|escape:'htmlall':'UTF-8'}">
                                {if not $prod.id_product|in_array:$productList and $prod.combSelected > 0}
								<span class="combNotif">{$prod.combSelected|escape:'htmlall':'UTF-8'}</span>
                                {/if}
                            </td>
                        </tr>
                        {if $prod.combinations && $prod.combinations|@count > 0}
                            {foreach from=$prod.combinations item=comb}
                                <tr class="odd comb-prod comb-prod-{$prod.id_product|escape:'htmlall':'UTF-8'}">
                                    <td class="">
                                        {$prod.id_product|escape:'htmlall':'UTF-8'}
                                        - {$comb.id_product_attribute|escape:'htmlall':'UTF-8'}
                                    </td>
                                    <td class="">
                                        {$prod.name|escape:'htmlall':'UTF-8'}
                                    </td>
                                    <td class="">
                                        {$comb.attribute_designation|escape:'htmlall':'UTF-8'}
                                    </td>
                                    <td class="image-col">
                                        {if $prod.combinations_imgs[$comb.id_product_attribute]|@count > 0}
                                        <img src="{$link->getImageLink($prod.link_rewrite, $prod.combinations_imgs[$comb.id_product_attribute][0].id_image, 'home_default')|escape:'htmlall':'UTF-8'}"
                                             alt="{$prod.name|escape:'htmlall':'UTF-8'}">
                                        {else}
                                        <img src="{$link->getImageLink($prod.link_rewrite, $prod.image.id_image, 'home_default')|escape:'htmlall':'UTF-8'}"*}
                                                 alt="{$prod.name|escape:'htmlall':'UTF-8'}">
                                        {/if}
                                    </td>
                                    <td class="">
                                        {l s='Ref' mod='linodott'}: {$prod.reference|escape:'htmlall':'UTF-8'}<br>
                                        {l s='Upc' mod='linodott'}: {$prod.upc|escape:'htmlall':'UTF-8'}<br>
                                        {l s='Comb Ref' mod='linodott'}: {$comb.reference|escape:'htmlall':'UTF-8'}<br>
                                        {l s='Comb Upc' mod='linodott'}: {$comb.upc|escape:'htmlall':'UTF-8'}<br>
                                    </td>
                                    <td class="">
                                        {l s='Ean' mod='linodott'}: {$prod.ean13|escape:'htmlall':'UTF-8'}<br>
                                        {l s='Comb Ean' mod='linodott'}: {$comb.ean13|escape:'htmlall':'UTF-8'}
                                    </td>
                                    <td class="">
                                        {l s='product' mod='linodott'}: {$prod.quantity|escape:'htmlall':'UTF-8'}<br>
                                        {l s='combination' mod='linodott'}: {$comb.quantity|escape:'htmlall':'UTF-8'}
                                    </td>
                                    <td class="">
                                        {l s='product' mod='linodott'}
                                        : {$prod.price|string_format:"%.2f"|escape:'htmlall':'UTF-8'}<br>
                                        {l s='combination' mod='linodott'}
                                        : {($prod.price + $comb.price)|string_format:"%.2f"|escape:'htmlall':'UTF-8'}
                                    </td>
                                    <td class="">
                                        {$prod.quantity_discount|escape:'htmlall':'UTF-8'}
                                    </td>
                                    <td class="">
                                        {$prod.rate|escape:'htmlall':'UTF-8'}
                                    </td>
                                    <td class="">
                                        {l s='product' mod='linodott'}: {$prod.available_now|escape:'htmlall':'UTF-8'}
                                        <br>
                                        {l s='combination' mod='linodott'}
                                        : {$comb.available_date|escape:'htmlall':'UTF-8'}
                                    </td>
                                    <td class="">

                                    </td>
                                    <td class="">
                                        <input type="checkbox"
                                               class="product-checklist product-child-{$prod.id_product|escape:'htmlall':'UTF-8'}" {if $prod.id_product|cat:"_"|cat:$comb.id_product_attribute|in_array:$productList} checked {/if}
                                               value="{$prod.id_product|escape:'htmlall':'UTF-8'}_{$comb.id_product_attribute|escape:'htmlall':'UTF-8'}"
                                               data-parentid="{$prod.id_product|escape:'htmlall':'UTF-8'}"
                                               name="product_dott[]">
                                    </td>
                                </tr>
                            {/foreach}

                        {/if}


                    {/foreach}
                {else}
                    <tr class="odd">
                        <td colspan="8">{l s='No order yet!' mod='linodott'}</td>
                    </tr>
                {/if}
                </tbody>
            </table>
            <input type="hidden" name="product_dott_value_list" value="{$productDottValueList|escape:'htmlall':'UTF-8'}">

            <div class="lino-pagination">
                {if $pageMax > 0 }
                    <ul class="dott-pagination">
                    {assign var="paginationBreak" value=10}
                    {if $pageMax > $paginationBreak}
                        <li><a href="index.php?controller=AdminDottFeedProducts&amp;page=1&amp;token={$token|escape:'htmlall':'UTF-8'}"><<</a></li>
                    {/if}
                    {if $page > 1}
                        <li><a href="index.php?controller=AdminDottFeedProducts&amp;page={($page-1)|escape:'htmlall':'UTF-8'}&amp;token={$token|escape:'htmlall':'UTF-8'}"><</a></li>
                    {/if}
                    {if $pageMax > $paginationBreak}
                        {for $cpt=($page-3) to $page}
                            {if $cpt > 0}
                                <li {if $page == $cpt}class="active"{/if} ><a href="index.php?controller=AdminDottFeedProducts&amp;page={$cpt|escape:'htmlall':'UTF-8'}&amp;token={$token|escape:'htmlall':'UTF-8'}">{$cpt|escape:'htmlall':'UTF-8'}</a></li>
                            {/if}
                        {/for}
                        {for $cpt=($page+1) to $page+2}
                            {if $cpt <= $pageMax}
                                <li {if $page == $cpt}class="active"{/if} ><a href="index.php?controller=AdminDottFeedProducts&amp;page={$cpt|escape:'htmlall':'UTF-8'}&amp;token={$token|escape:'htmlall':'UTF-8'}">{$cpt|escape:'htmlall':'UTF-8'}</a></li>
                            {/if}
                        {/for}
                    {else}
                        {for $cpt=1 to $pageMax}
                            <li {if $page == $cpt}class="active"{/if} ><a href="index.php?controller=AdminDottFeedProducts&amp;page={$cpt|escape:'htmlall':'UTF-8'}&amp;token={$token|escape:'htmlall':'UTF-8'}">{$cpt|escape:'htmlall':'UTF-8'}</a></li>
                        {/for}
                    {/if}
                    {if $page < $pageMax}
                        <li><a href="index.php?controller=AdminDottFeedProducts&amp;page={($page+1)|escape:'htmlall':'UTF-8'}&amp;token={$token|escape:'htmlall':'UTF-8'}">></a></li>
                    {/if}
                    {if $pageMax > $paginationBreak}
                        <li><a href="index.php?controller=AdminDottFeedProducts&amp;page={$pageMax|escape:'htmlall':'UTF-8'}&amp;token={$token|escape:'htmlall':'UTF-8'}">>></a></li>
                    {/if}
                    </ul>
                {/if}
            </div>
            <input type="submit" id="productDottList" name="productDottList" class="btn btn-primary dott-button-r"
                   value="{l s='Gravar' mod='linodott'}">
        </div>
    </div>

    <div class="panel col-lg-12">
        <div class="panel-heading">
            {l s='Selecione todos / Deselecione todos' mod='linodott'}
        </div>
        <input type="submit" name="selectAll" class="btn btn-primary dott-button-r"
               value="{l s='Selecione todos os produtos' mod='linodott'}">
        <input type="submit" name="unSelectAll" class="btn btn-primary dott-button-r "
               value="{l s='Deselecione todos os produtos' mod='linodott'}">
    </div>

    <div class="panel col-lg-12">
        <div class="panel-heading">
            {l s='Selecionar com um ficheiro csv' mod='linodott'}
        </div>
        <div class="field-left">
            <p>
                {l s='Envia um ficheiro csv com uma só coluna (id_product) que sera a lista dos produtos a selecionar. Exemplo' mod='linodott'}<br>
                id_product;<br>
                123;<br>
                120;<br>
                126;<br>
                etc.
            </p>
        </div>
        <input type="submit" name="uploadFile" class="btn btn-primary dott-button-r " value="{l s='Enviar' mod='linodott'}">
        <input type="file" name="csvupdate" class="dott-button-r" >
    </div>

    <div class="panel col-lg-12">
        <div class="panel-heading">
            {l s='Preço especifico' mod='linodott'}
        </div>
        <div class="field-left priceVariation">
            <p>{l s='Aumentar ou diminuir o preço Dott dos produtos selecionados (para o ficheiro de Oferta e o ficheiro de catalógo)' mod='linodott'}</p>
            <select name="priceVariationDirection">
                <option value="" {if $priceVariationDirection == ""} selected {/if}></option>
                <option value="more" {if $priceVariationDirection == "more"} selected {/if}>Aumentar</option>
                <option value="less" {if $priceVariationDirection == "less"} selected {/if}>Diminuir</option>
            </select>
            <p>{l s='Valor a aumentar ou diminuir no preço Dott dos produtos selecionados (para o ficheiro de Oferta e o ficheiro de catalógo)' mod='linodott'}</p>
            <input type="number" min="0" max="200" step="0.5" value="{$priceVariationQuantity|escape:'htmlall':'UTF-8'}" id="priceVariationQuantity" name="priceVariationQuantity" >
            <p>{l s='Variação em % ou em numerico no preço Dott dos produtos selecionados (para o ficheiro de Oferta e o ficheiro de catalógo)' mod='linodott'}</p>
            <select name="priceVariationType">
                <option value="perc" {if $priceVariationType == "perc"} selected {/if}>%</option>
                <option value="num" {if $priceVariationType == "num"} selected {/if}>numerico</option>
            </select>
        </div>
        <input type="submit" id="productDottList" name="productDottList" class="btn btn-primary dott-button-r"
               value="{l s='Gravar' mod='linodott'}">
    </div>

    <div class="panel col-lg-12">
        <div class="panel-heading">
            {l s='Gerar o ficheiro de oferta (sincronização do estoque e do preço)' mod='linodott'}
        </div>
        <input type="submit" name="feedOferta" class="btn btn-primary dott-button-r"
               value="{l s='Gerar manualmente o ficheiro de oferta' mod='linodott'}">
        <h2>Ficheiro de oferta</h2>
        <ul class="offerFeedFiles">
            <li><a href="{$base_url|escape:'htmlall':'UTF-8'}modules/linodott/feedOferta/oferta.csv" target="_blank">oferta.csv</a></li>
        </ul>
        {if $isDebug == 1 && $offerFeedFiles|@count > 0}
            <h2>Log files</h2>
            <ul class="offerFeedFiles">
                {foreach from=$offerFeedFiles key=filename item=fileDate}
                    <li><a href="{$base_url|escape:'htmlall':'UTF-8'}modules/linodott/logs/feedOferta/{$filename|escape:'htmlall':'UTF-8'}" target="_blank">{$filename|escape:'htmlall':'UTF-8'}</a> - {$fileDate|date_format:"%D %H:%M:%S"|escape:'htmlall':'UTF-8'}</li>
                {/foreach}
            </ul>
        {/if}
    </div>


    <div class="panel col-lg-12">
        <div class="panel-heading">
            {l s='Gerar ficheiro de catálogo' mod='linodott'}
        </div>
        <input type="submit" name="feedCatalog" class="btn btn-primary dott-button-r"
               value="{l s='Gerar o ficheiro de catálogo' mod='linodott'}">
            <h2>Ficheiro de catálogo</h2>
            <ul class="offerFeedFiles">
               <li><a href="{$base_url|escape:'htmlall':'UTF-8'}modules/linodott/feedCatalog/catalog.csv" target="_blank">catalog.csv</a></li>
            </ul>
    </div>

    {if $isDebug == 1}
        <div class="panel col-lg-12">
            <div class="panel-heading">
                {l s='Feed product logs' mod='linodott'}
            </div>
            {if $logFiles|@count > 0}
                <h2>Last debug files</h2>
                <ul class="offerFeedFiles">
                    {foreach from=$logFiles key=filename item=fileDate}
                        <li><a href="{$base_url|escape:'htmlall':'UTF-8'}modules/linodott/logs/feedProduct/{$filename|escape:'htmlall':'UTF-8'}" target="_blank">{$filename|escape:'htmlall':'UTF-8'}</a> - {$fileDate|date_format:"%D %H:%M:%S"|escape:'htmlall':'UTF-8'}</li>
                    {/foreach}
                </ul>
            {/if}
        </div>
        <div class="panel col-lg-12">
            <div class="panel-heading">
                {l s='Cron Oferta logs' mod='linodott'}
            </div>
            <h2>Last cron oferta debug files</h2>
            {if $cronOfertalogFiles|@count > 0}
                <ul class="offerFeedFiles">
                    {foreach from=$cronOfertalogFiles key=filename item=fileDate}
                        <li><a href="{$base_url|escape:'htmlall':'UTF-8'}modules/linodott/logs/cronOferta/{$filename|escape:'htmlall':'UTF-8'}" target="_blank">{$filename|escape:'htmlall':'UTF-8'}</a> - {$fileDate|date_format:"%D %H:%M:%S"|escape:'htmlall':'UTF-8'}</li>
                    {/foreach}
                </ul>
            {/if}
        </div>
        <div class="panel col-lg-12">
            <div class="panel-heading">
                {l s='Cron Orders logs' mod='linodott'}
            </div>
            <h2>Last cron orders debug files</h2>
            {if $cronOrderslogFiles|@count > 0}
                <ul class="offerFeedFiles">
                    {foreach from=$cronOrderslogFiles key=filename item=fileDate}
                        <li><a href="{$base_url|escape:'htmlall':'UTF-8'}modules/linodott/logs/cronOrders/{$filename|escape:'htmlall':'UTF-8'}" target="_blank">{$filename|escape:'htmlall':'UTF-8'}</a> - {$fileDate|date_format:"%D %H:%M:%S"|escape:'htmlall':'UTF-8'}</li>
                    {/foreach}
                </ul>
            {/if}
        </div>
    {/if}

</form>