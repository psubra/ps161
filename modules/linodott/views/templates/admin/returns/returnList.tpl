{**
* NOTICE OF LICENSE
*
* This file is licenced under the GNU General Public License, version 3 (GPL-3.0).
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* @author    Li-Nó Design www.lino-design.com <contact@lino-design.com>
* @copyright 2019 Li-Nó Design Lda
* @license   https://opensource.org/licenses/GPL-3.0 GNU General Public License version 3
*}


<header class="row">
	{if $errorMsg and $errorMsg != ""}
		<div class="dottMsg alert alert-danger">
			{$errorMsg|escape:'htmlall':'UTF-8'}
		</div>
	{elseif $returnMsg and $returnMsg != ''}
		<div class="dottMsg alert alert-success">
			{$returnMsg|escape:'htmlall':'UTF-8'}
		</div>
	{/if}
</header>

<div class="panel col-lg-12">
	<div class="panel-heading">
		{l s='Gestão dos retornos' mod='linodott'} <span class="badge">{$data.data|@count|escape:'htmlall':'UTF-8'}</span>
		<span class="panel-heading-action">
						<a class="list-toolbar-btn" href="javascript:location.reload();">
						<span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="Refresh list"
						      data-html="true" data-placement="top">
							<i class="process-icon-refresh"></i>
						</span>
					  </a>
        </span>
	</div>


	<div class="table-responsive-row clearfix">
		<table class="table customer">
			<thead>
			<tr class="nodrag nodrop">
				<th class="fixed-width-xs text-center">
						<span class="title_box">{l s='Id' mod='linodott'}</span>
				</th>
				<th class=""><span class="title_box">{l s='Merchant Order' mod='linodott'}</span></th>
				<th class=""><span class="title_box">{l s='Order Nr' mod='linodott'}</span></th>
				<th class=""><span class="title_box">{l s='Status' mod='linodott'}</span></th>
				<th class=""><span class="title_box">{l s='Reason' mod='linodott'}</span></th>
				<th class=""><span class="title_box">{l s='Products' mod='linodott'}</span></th>
				<th class=""><span class="title_box">{l s='Customer' mod='linodott'}</span></th>
				<th class=""><span class="title_box">{l s='Total' mod='linodott'}</span></th>
				<th class=""><span class="title_box">{l s='Creation date' mod='linodott'}</span></th>
				<th class=""><span class="title_box">{l s='Modification date' mod='linodott'}</span></th>
				<th></th>
			</tr>
			</thead>

			<tbody>
			{if $data.data|@count > 0}
				{foreach from=$data.data item=return}
					<tr class="odd">
						<td class="pointer" onclick="document.location = 'index.php?controller=AdminDottReturns&amp;returnId={$return.id|escape:'htmlall':'UTF-8'}&amp;dottaction=returnview&amp;token={$token|escape:'htmlall':'UTF-8'}'">
							{$return.id|escape:'htmlall':'UTF-8'}
						</td>
						<td class="pointer" onclick="document.location = 'index.php?controller=AdminDottReturns&amp;returnId={$return.id|escape:'htmlall':'UTF-8'}&amp;dottaction=returnview&amp;token={$token|escape:'htmlall':'UTF-8'}'">
							{$return.merchantOrderId|escape:'htmlall':'UTF-8'}
						</td>
						<td class="pointer" onclick="document.location = 'index.php?controller=AdminDottReturns&amp;returnId={$return.id|escape:'htmlall':'UTF-8'}&amp;dottaction=returnview&amp;token={$token|escape:'htmlall':'UTF-8'}'">
							{$return.orderNumber|escape:'htmlall':'UTF-8'}
						</td>
						<td class="pointer" onclick="document.location = 'index.php?controller=AdminDottReturns&amp;returnId={$return.id|escape:'htmlall':'UTF-8'}&amp;dottaction=returnview&amp;token={$token|escape:'htmlall':'UTF-8'}'">
							{$return.status|escape:'htmlall':'UTF-8'}
						</td>
						<td class="pointer" onclick="document.location = 'index.php?controller=AdminDottReturns&amp;returnId={$return.id|escape:'htmlall':'UTF-8'}&amp;dottaction=returnview&amp;token={$token|escape:'htmlall':'UTF-8'}'">
							{$return.reason|escape:'htmlall':'UTF-8'}
						</td>
						<td class="pointer" onclick="document.location = 'index.php?controller=AdminDottReturns&amp;returnId={$return.id|escape:'htmlall':'UTF-8'}&amp;dottaction=returnview&amp;token={$token|escape:'htmlall':'UTF-8'}'">
							{$return.item.name|escape:'htmlall':'UTF-8'} ({$return.item.quantityToReturn|escape:'htmlall':'UTF-8'}/{$return.item.quantityOrdered|escape:'htmlall':'UTF-8'})<br>
						</td>
						<td class="pointer" onclick="document.location = 'index.php?controller=AdminDottReturns&amp;returnId={$return.id|escape:'htmlall':'UTF-8'}&amp;dottaction=returnview&amp;token={$token|escape:'htmlall':'UTF-8'}'">
							{$return.customer.firstName|escape:'htmlall':'UTF-8'}
							{$return.customer.lastName|escape:'htmlall':'UTF-8'}
						</td>
						<td class="pointer" onclick="document.location = 'index.php?controller=AdminDottReturns&amp;returnId={$return.id|escape:'htmlall':'UTF-8'}&amp;dottaction=returnview&amp;token={$token|escape:'htmlall':'UTF-8'}'">
							{$return.amount|escape:'htmlall':'UTF-8'} €
						</td>
						<td class="pointer" onclick="document.location = 'index.php?controller=AdminDottReturns&amp;returnId={$return.id|escape:'htmlall':'UTF-8'}&amp;dottaction=returnview&amp;token={$token|escape:'htmlall':'UTF-8'}'">
							{$return.createdOn|date_format|escape:'htmlall':'UTF-8'}
						</td>
						<td class="pointer" onclick="document.location = 'index.php?controller=AdminDottReturns&amp;returnId={$return.id|escape:'htmlall':'UTF-8'}&amp;dottaction=returnview&amp;token={$token|escape:'htmlall':'UTF-8'}'">
							{$return.modifiedOn|date_format|escape:'htmlall':'UTF-8'}
						</td>
						<td class="text-right">
							<div class="btn-group-action">
								<div class="btn-group pull-right">
									<a href="index.php?controller=AdminDottReturns&amp;returnId={$return.id|escape:'htmlall':'UTF-8'}&amp;dottaction=returnview&amp;token={$token|escape:'htmlall':'UTF-8'}" title="Ver" class="view btn btn-default">
										<i class="icon-eye"></i> {l s='Ver' mod='linodott'}
									</a>
									<button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
										<i class="icon-caret-down"></i>&nbsp;
									</button>
									<ul class="dropdown-menu">
										<li>
											<a href="index.php?controller=AdminDottReturns&amp;returnId={$return.id|escape:'htmlall':'UTF-8'}&amp;dottaction=returnview&amp;token={$token|escape:'htmlall':'UTF-8'}" title="{l s='Ver' mod='linodott'}">
												<i class="icon-search-plus"></i> {l s='Ver' mod='linodott'}
											</a>
										</li>
										<li class="divider">
										</li>
										{if $return.status == 'Accepted'}
										<li>
											<a href="index.php?controller=AdminDottReturns&amp;returnId={$return.id|escape:'htmlall':'UTF-8'}&amp;orderId={$return.merchantOrderId|escape:'htmlall':'UTF-8'}&amp;dottaction=returnreceive&amp;token={$token|escape:'htmlall':'UTF-8'}" title="{l s='Marcar como recebido' mod='linodott'}" class="{l s='Marcar como recebido' mod='linodott'}">
												{l s='Marcar como recebido' mod='linodott'}
											</a>
										</li>
										{/if}
										{if $return.status == 'Open'}
										<li>
											<a href="index.php?controller=AdminDottReturns&amp;returnId={$return.id|escape:'htmlall':'UTF-8'}&amp;dottaction=returnaccept&amp;token={$token|escape:'htmlall':'UTF-8'}" title="{l s='Aceitar' mod='linodott'}" class="{l s='Aceitar' mod='linodott'}">
												{l s='Aceitar' mod='linodott'}
											</a>
										</li>
										<li>
											<a href="index.php?controller=AdminDottReturns&amp;returnId={$return.id|escape:'htmlall':'UTF-8'}&amp;dottaction=returncancel&amp;token={$token|escape:'htmlall':'UTF-8'}" title="{l s='Cancelar' mod='linodott'}" class="{l s='Cancelar' mod='linodott'}">
												{l s='Cancelar' mod='linodott'}
											</a>
										</li>
										{/if}
{*										{if $return.status == 'PackageReceived'}*}
{*											<li>*}
{*												<a href="index.php?controller=AdminDottReturns&amp;returnId={$return.id|escape:'htmlall':'UTF-8'}&amp;orderId={$return.merchantOrderId}&amp;dottaction=returnrefund&amp;token={$token|escape:'htmlall':'UTF-8'}" title="{l s='Refund' mod='linodott'}" class="{l s='Refund' mod='linodott'}">*}
{*													{l s='Refund' mod='linodott'}*}
{*												</a>*}
{*											</li>*}
{*										{/if}*}
									</ul>
								</div>
							</div>
						</td>
					</tr>
				{/foreach}
			{else}
				<tr class="odd">
					<td colspan="8">{l s='Não ha retorno' mod='linodott'}</td>
				</tr>
			{/if}
			</tbody>
		</table>
		<div class="lino-pagination">
			{if $nbPages > 0 }
				<ul class="dott-pagination">
					{assign var="paginationBreak" value=10}
					{if $nbPages > $paginationBreak}
						<li><a href="index.php?controller=AdminDottReturns&amp;page=1&amp;token={$token|escape:'htmlall':'UTF-8'}"><<</a></li>
					{/if}
					{if $page > 1}
						<li><a href="index.php?controller=AdminDottReturns&amp;page={($page-1)|escape:'htmlall':'UTF-8'}&amp;token={$token|escape:'htmlall':'UTF-8'}"><</a></li>
					{/if}
					{if $nbPages > $paginationBreak}
						{for $cpt=($page-3) to $page}
							{if $cpt > 0}
								<li {if $page == $cpt}class="active"{/if} ><a href="index.php?controller=AdminDottReturns&amp;page={$cpt|escape:'htmlall':'UTF-8'}&amp;token={$token|escape:'htmlall':'UTF-8'}">{$cpt|escape:'htmlall':'UTF-8'}</a></li>
							{/if}
						{/for}
						{for $cpt=($page+1) to $page+2}
							{if $cpt <= $pageMax}
								<li {if $page == $cpt}class="active"{/if} ><a href="index.php?controller=AdminDottReturns&amp;page={$cpt|escape:'htmlall':'UTF-8'}&amp;token={$token|escape:'htmlall':'UTF-8'}">{$cpt|escape:'htmlall':'UTF-8'}</a></li>
							{/if}
						{/for}
					{else}
						{for $cpt=1 to $nbPages}
							<li {if $page == $cpt}class="active"{/if} ><a href="index.php?controller=AdminDottReturns&amp;page={$cpt|escape:'htmlall':'UTF-8'}&amp;token={$token|escape:'htmlall':'UTF-8'}">{$cpt|escape:'htmlall':'UTF-8'}</a></li>
						{/for}
					{/if}
					{if $page < $nbPages}
						<li><a href="index.php?controller=AdminDottReturns&amp;page={($page+1)|escape:'htmlall':'UTF-8'}&amp;token={$token|escape:'htmlall':'UTF-8'}">></a></li>
					{/if}
					{if $nbPages > $paginationBreak}
						<li><a href="index.php?controller=AdminDottReturns&amp;page={$nbPages|escape:'htmlall':'UTF-8'}&amp;token={$token|escape:'htmlall':'UTF-8'}">>></a></li>
					{/if}
				</ul>
			{/if}
		</div>
	</div>
</div>

