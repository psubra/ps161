{**
* NOTICE OF LICENSE
*
* This file is licenced under the GNU General Public License, version 3 (GPL-3.0).
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* @author    Li-Nó Design www.lino-design.com <contact@lino-design.com>
* @copyright 2019 Li-Nó Design Lda
* @license   https://opensource.org/licenses/GPL-3.0 GNU General Public License version 3
*}


<header class="row">
	<div class="col-md-9">
		<ul class="dott-action">
		{if $data.status == 'Accepted'}
			<li><a class="btn btn-primary" href="index.php?controller=AdminDottReturns&amp;returnId={$data.id|escape:'htmlall':'UTF-8'}&amp;dottaction=returnreceive&amp;token={$token|escape:'htmlall':'UTF-8'}" title="{l s='Marcar como recebido' mod='linodott'}">{l s='Marcar como recebido' mod='linodott'}</a></li>
			{/if}
			{if $data.status == 'Open'}
			<li><a class="btn btn-primary" href="index.php?controller=AdminDottReturns&amp;returnId={$data.id|escape:'htmlall':'UTF-8'}&amp;dottaction=returnaccept&amp;token={$token|escape:'htmlall':'UTF-8'}" title="{l s='Aceitar' mod='linodott'}">{l s='Aceitar' mod='linodott'}</a></li>
			<li><a class="btn btn-primary" href="index.php?controller=AdminDottReturns&amp;returnId={$data.id|escape:'htmlall':'UTF-8'}&amp;dottaction=returncancel&amp;token={$token|escape:'htmlall':'UTF-8'}" title="{l s='Cancelar' mod='linodott'}">{l s='Cancelar' mod='linodott'}</a></li>
			{/if}
			{if $data.status == 'PackageReceived'}
{*			<li><a class="btn btn-primary" href="index.php?controller=AdminDottReturns&amp;returnId={$data.id|escape:'htmlall':'UTF-8'}&amp;orderId={$data.merchantOrderId}&amp;dottaction=returnrefund&amp;token={$token|escape:'htmlall':'UTF-8'}" title="{l s='Refund' mod='linodott'}">{l s='Refund' mod='linodott'}</a></li>*}
			{/if}
		</ul>
	</div>
	<div class="col-md-3 text-right">
		<a class="btn btn-primary" href="index.php?controller=AdminDottReturns&amp;dottaction=returnList&amp;token={$token|escape:'htmlall':'UTF-8'}" title="{l s='Voltar aos retornos' mod='linodott'}">{l s='Voltar aos retornos' mod='linodott'}</a>
	</div>
	{if $errorMsg and $errorMsg != ""}
		<div class="dottMsg dottError">
			{$errorMsg|escape:'htmlall':'UTF-8'}
		</div>
	{elseif $returnMsg and $returnMsg != ''}
		<div class="dottMsg dottNotice">
			{$returnMsg|escape:'htmlall':'UTF-8'}
		</div>
	{/if}
</header>

<div class="panel col-md-6">
	<div class="panel-heading">
		{l s='Detalhe do retorno' mod='linodott'} <span class="badge">{$data.id|escape:'htmlall':'UTF-8'}</span>
		<span class="panel-heading-action">
						<a class="list-toolbar-btn" href="javascript:location.reload();">
						<span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="Refresh"
						      data-html="true" data-placement="top">
							<i class="process-icon-refresh"></i>
						</span>
					  </a>
        </span>
	</div>

	<div class="form-horizontal">
		<div class="row">
			<label class="control-label col-lg-3">{l s='Id' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.id|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Merchant Order Id' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.merchantOrderId|escape:'htmlall':'UTF-8'}
				</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='OrderNr' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.orderNumber|escape:'htmlall':'UTF-8'}
				</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Status' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.status|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Customer' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">
					{$data.customer.firstName|escape:'htmlall':'UTF-8'} {$data.customer.lastName|escape:'htmlall':'UTF-8'}<br>
				</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Reason' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">
					{$data.reason|escape:'htmlall':'UTF-8'}
				</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Comment' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">
					{$data.status|escape:'htmlall':'UTF-8'}
				</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Amount' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">
					{$data.amount|escape:'htmlall':'UTF-8'}
				</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Amount tax excl' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">
					{$data.amountTaxExcl|escape:'htmlall':'UTF-8'}
				</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Creation date' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.createdOn|date_format|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Modification date' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.modifiedOn|date_format|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>
	</div>
</div>

<div class="panel col-md-6">
	<div class="panel-heading">{l s='Produtos' mod='linodott'} <span class="badge">{$data.item.name|escape:'htmlall':'UTF-8'}</span>
		<span class="panel-heading-action"></span>
	</div>
	<div class="form-horizontal">
		<div class="row">
			<label class="control-label col-lg-3">{l s='Order ID' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.item.orderItemId|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Image' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static image-col"><img src="{$data.item.imageUrl|escape:'htmlall':'UTF-8'}" alt="{$data.item.name|escape:'htmlall':'UTF-8'}"></p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='VAT' mod='linodott'}</label>
			<div class="col-lg-9 ">
				<p class="form-control-static">{$data.item.vat|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Gtin' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.item.gtin|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Sku' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.item.sku|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Quantity ordered' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.item.quantityOrdered|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Quantity to return' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.item.quantityToReturn|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Amount' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.item.amount|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Amount Tax Excl' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.item.amountTaxExcl|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>
	</div>
</div>