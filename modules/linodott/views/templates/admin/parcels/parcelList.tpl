{**
* NOTICE OF LICENSE
*
* This file is licenced under the GNU General Public License, version 3 (GPL-3.0).
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* @author    Li-Nó Design www.lino-design.com <contact@lino-design.com>
* @copyright 2019 Li-Nó Design Lda
* @license   https://opensource.org/licenses/GPL-3.0 GNU General Public License version 3
*}

<header class="row">
	{if $errorMsg and $errorMsg != ""}
		<div class="dottMsg alert alert-danger">
			{$errorMsg|escape:'htmlall':'UTF-8'}
		</div>
	{elseif $returnMsg and $returnMsg != ''}
		<div class="dottMsg alert alert-success">
			{$returnMsg|escape:'htmlall':'UTF-8'}
		</div>
	{/if}
</header>


<div class="panel col-lg-12">
	<div class="panel-heading">
		{l s='Gestão das parcelas' mod='linodott'} <span class="badge">{$data.totalRow|escape:'htmlall':'UTF-8'}</span>
		<span class="panel-heading-action">
						<a class="list-toolbar-btn" href="javascript:location.reload();">
						<span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="Refresh"
						      data-html="true" data-placement="top">
							<i class="process-icon-refresh"></i>
						</span>
					  </a>
        </span>
	</div>


	<div class="table-responsive-row clearfix">
		<table class="table customer">
			<thead>
			<tr class="nodrag nodrop">
				<th class="fixed-width-xs text-center">
						<span class="title_box">{l s='Id' mod='linodott'}</span>
				</th>
				<th class=""><span class="title_box">{l s='Status' mod='linodott'}</span></th>
				<th class=""><span class="title_box">{l s='Shipping Type Method' mod='linodott'}</span></th>
				<th class=""><span class="title_box">{l s='Shipping Type Name' mod='linodott'}</span></th>
				<th class=""><span class="title_box">{l s='Merchant Order Id' mod='linodott'}</span></th>
				<th class=""><span class="title_box">{l s='Products' mod='linodott'}</span></th>
				<th class=""><span class="title_box">{l s='Creation date' mod='linodott'}</span></th>
				<th class=""><span class="title_box">{l s='Modification date' mod='linodott'}</span></th>
				<th class=""><span class="title_box">{l s='Expected Arrival' mod='linodott'}</span></th>
				<th class=""><span class="title_box"></span></th>
				<th></th>
			</tr>
			</thead>

			<tbody>
			{if $data.totalRow > 0}
				{foreach from=$data.data item=parcel}
					<tr class="odd">
						<td class="pointer" onclick="document.location = 'index.php?controller=AdminDottParcels&amp;parcelId={$parcel.id|escape:'htmlall':'UTF-8'}&amp;dottaction=viewparcel&amp;token={$token|escape:'htmlall':'UTF-8'}'">
							{$parcel.id|escape:'htmlall':'UTF-8'}
						</td>
						<td class="pointer" onclick="document.location = 'index.php?controller=AdminDottParcels&amp;parcelId={$parcel.id|escape:'htmlall':'UTF-8'}&amp;dottaction=viewparcel&amp;token={$token|escape:'htmlall':'UTF-8'}'">
							{$parcel.status|escape:'htmlall':'UTF-8'}
						</td>
						<td class="pointer" onclick="document.location = 'index.php?controller=AdminDottParcels&amp;parcelId={$parcel.id|escape:'htmlall':'UTF-8'}&amp;dottaction=viewparcel&amp;token={$token|escape:'htmlall':'UTF-8'}'">
							{$parcel.shippingTypeMethod|escape:'htmlall':'UTF-8'}
						</td>
						<td class="pointer" onclick="document.location = 'index.php?controller=AdminDottParcels&amp;parcelId={$parcel.id|escape:'htmlall':'UTF-8'}&amp;dottaction=viewparcel&amp;token={$token|escape:'htmlall':'UTF-8'}'">
							{$parcel.shippingTypeName|escape:'htmlall':'UTF-8'}
						</td>
						<td class="pointer" onclick="document.location = 'index.php?controller=AdminDottParcels&amp;parcelId={$parcel.id|escape:'htmlall':'UTF-8'}&amp;dottaction=viewparcel&amp;token={$token|escape:'htmlall':'UTF-8'}'">
							<a href="index.php?controller=AdminDottOrders&amp;orderId={$parcel.merchantOrderId|escape:'htmlall':'UTF-8'}&amp;dottaction=vieworder&amp;token={$orderToken|escape:'htmlall':'UTF-8'}" title="View" class="view btn btn-default">
							{$parcel.merchantOrderId|escape:'htmlall':'UTF-8'}
							</a>
						</td>
						<td class="pointer" onclick="document.location = 'index.php?controller=AdminDottParcels&amp;parcelId={$parcel.id|escape:'htmlall':'UTF-8'}&amp;dottaction=viewparcel&amp;token={$token|escape:'htmlall':'UTF-8'}'">
							{foreach from=$parcel.orderItems item=prod}
								{$prod.name|escape:'htmlall':'UTF-8'} ({$prod.quantity|escape:'htmlall':'UTF-8'})<br>
							{/foreach}
						</td>
						<td class="pointer" onclick="document.location = 'index.php?controller=AdminDottParcels&amp;parcelId={$parcel.id|escape:'htmlall':'UTF-8'}&amp;dottaction=viewparcel&amp;token={$token|escape:'htmlall':'UTF-8'}'">
							{$parcel.createdOn|date_format|escape:'htmlall':'UTF-8'}
						</td>
						<td class="pointer" onclick="document.location = 'index.php?controller=AdminDottParcels&amp;parcelId={$parcel.id|escape:'htmlall':'UTF-8'}&amp;dottaction=viewparcel&amp;token={$token|escape:'htmlall':'UTF-8'}'">
							{$parcel.modifiedOn|date_format|escape:'htmlall':'UTF-8'}
						</td>
						<td class="pointer" onclick="document.location = 'index.php?controller=AdminDottParcels&amp;parcelId={$parcel.id|escape:'htmlall':'UTF-8'}&amp;dottaction=viewparcel&amp;token={$token|escape:'htmlall':'UTF-8'}'">
							{if $parcel.parcelTimeLine and $parcel.parcelTimeLine.expectedArrival}
								{$parcel.parcelTimeLine.expectedArrival|date_format|escape:'htmlall':'UTF-8'}
							{/if}
						</td>
						<td class="text-right">
							<div class="btn-group-action">
								<div class="btn-group pull-right">
									<a href="index.php?controller=AdminDottParcels&amp;parcelId={$parcel.id|escape:'htmlall':'UTF-8'}&amp;dottaction=viewparcel&amp;token={$token|escape:'htmlall':'UTF-8'}" title="{l s='View' mod='linodott'}" class="view btn btn-default">
										<i class="icon-eye"></i> {l s='Ver' mod='linodott'}
									</a>
								</div>
							</div>
						</td>
					</tr>
				{/foreach}
			{else}
				<tr class="odd">
					<td colspan="8">{l s='Não ha parcela' mod='linodott'}</td>
				</tr>
			{/if}
			</tbody>
		</table>
		<div class="lino-pagination">
			{if $nbPages > 0 }
				<ul class="dott-pagination">
					{assign var="paginationBreak" value=10}
					{if $nbPages > $paginationBreak}
						<li><a href="index.php?controller=AdminDottParcels&amp;page=1&amp;token={$token|escape:'htmlall':'UTF-8'}"><<</a></li>
					{/if}
					{if $page > 1}
						<li><a href="index.php?controller=AdminDottParcels&amp;page={($page-1)|escape:'htmlall':'UTF-8'}&amp;token={$token|escape:'htmlall':'UTF-8'}"><</a></li>
					{/if}
					{if $nbPages > $paginationBreak}
						{for $cpt=($page-3) to $page}
							{if $cpt > 0}
								<li {if $page == $cpt}class="active"{/if} ><a href="index.php?controller=AdminDottParcels&amp;page={$cpt|escape:'htmlall':'UTF-8'}&amp;token={$token|escape:'htmlall':'UTF-8'}">{$cpt|escape:'htmlall':'UTF-8'}</a></li>
							{/if}
						{/for}
						{for $cpt=($page+1) to $page+2}
							{if $cpt <= $pageMax}
								<li {if $page == $cpt}class="active"{/if} ><a href="index.php?controller=AdminDottParcels&amp;page={$cpt|escape:'htmlall':'UTF-8'}&amp;token={$token|escape:'htmlall':'UTF-8'}">{$cpt|escape:'htmlall':'UTF-8'}</a></li>
							{/if}
						{/for}
					{else}
						{for $cpt=1 to $nbPages}
							<li {if $page == $cpt}class="active"{/if} ><a href="index.php?controller=AdminDottParcels&amp;page={$cpt|escape:'htmlall':'UTF-8'}&amp;token={$token|escape:'htmlall':'UTF-8'}">{$cpt|escape:'htmlall':'UTF-8'}</a></li>
						{/for}
					{/if}
					{if $page < $nbPages}
						<li><a href="index.php?controller=AdminDottParcels&amp;page={($page+1)|escape:'htmlall':'UTF-8'}&amp;token={$token|escape:'htmlall':'UTF-8'}">></a></li>
					{/if}
					{if $nbPages > $paginationBreak}
						<li><a href="index.php?controller=AdminDottParcels&amp;page={$nbPages|escape:'htmlall':'UTF-8'}&amp;token={$token|escape:'htmlall':'UTF-8'}">>></a></li>
					{/if}
				</ul>
			{/if}
		</div>
	</div>
</div>

