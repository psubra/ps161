{**
* NOTICE OF LICENSE
*
* This file is licenced under the GNU General Public License, version 3 (GPL-3.0).
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* @author    Li-Nó Design www.lino-design.com <contact@lino-design.com>
* @copyright 2019 Li-Nó Design Lda
* @license   https://opensource.org/licenses/GPL-3.0 GNU General Public License version 3
*}

<header class="row">
	{if $errorMsg and $errorMsg != ""}
		<div class="dottMsg alert alert-danger">
			{$errorMsg|escape:'htmlall':'UTF-8'}
		</div>
	{elseif $returnMsg and $returnMsg != ''}
		<div class="dottMsg alert alert-success">
			{$returnMsg|escape:'htmlall':'UTF-8'}
		</div>
	{/if}
	<div class="col-md-9">
	</div>
	<div class="col-md-3 text-right">
		<a class="btn btn-primary" href="index.php?controller=AdminDottParcels&amp;dottaction=parcelList&amp;token={$token|escape:'htmlall':'UTF-8'}" title="{l s='Go back to Dott parcels' mod='linodott'}">{l s='Go back to Dott parcels' mod='linodott'}</a>
	</div>
</header>

<div class="panel col-md-6">
	<div class="panel-heading">
		{l s='Detalhe da parcela' mod='linodott'} <span class="badge">{$data.merchantOrderId|escape:'htmlall':'UTF-8'}</span>
		<span class="panel-heading-action">
						<a class="list-toolbar-btn" href="javascript:location.reload();">
						<span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="Refresh"
						      data-html="true" data-placement="top">
							<i class="process-icon-refresh"></i>
						</span>
					  </a>
        </span>
	</div>

	<div class="form-horizontal">
		<div class="row">
			<label class="control-label col-lg-3">{l s='Id' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.id|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Status' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.status|escape:'htmlall':'UTF-8'}
				</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Shipping Type Method' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.shippingTypeMethod|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Shipping Type Name' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.shippingTypeName|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Order' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">
					<a href="index.php?controller=AdminDottOrders&amp;orderId={$data.merchantOrderId|escape:'htmlall':'UTF-8'}&amp;dottaction=vieworder&amp;token={$orderToken|escape:'htmlall':'UTF-8'}" title="Ver" class="view btn btn-default">
						{$data.merchantOrderId|escape:'htmlall':'UTF-8'}
					</a>
				</p>
			</div>
		</div>
		{if isset($data.buyerDeliveryTimeWindow)}
			<div class="row">
				<label class="control-label col-lg-3">{l s='Buyer Delivery Time Window' mod='linodott'}</label>
				<div class="col-lg-9">
					<p class="form-control-static">{$data.buyerDeliveryTimeWindow|escape:'htmlall':'UTF-8'}</p>
				</div>
			</div>
		{/if}
		<div class="row">
			<label class="control-label col-lg-3">{l s='Creation date' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.createdOn|date_format|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Modification date' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.modifiedOn|date_format|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>
	</div>

</div>

<div class="panel col-md-6">
	<div class="panel-heading">{l s='Parcel Time' mod='linodott'}<span class="badge">{$data.parcelTimeLine.expectedArrival|date_format|escape:'htmlall':'UTF-8'}</span>
		<span class="panel-heading-action">
		  <a class="list-toolbar-btn" href="javascript:location.reload();">
			  <span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="Refresh list" data-html="true" data-placement="top">
							<i class="process-icon-refresh"></i>
			  </span>
		  </a>
    </span>
	</div>

	<div class="form-horizontal">
		<div class="row">
			<label class="control-label col-lg-3">{l s='Expected Arrival' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.parcelTimeLine.expectedArrival|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Expected Delivery Date' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.parcelTimeLine.expectedDeliveryDate|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Collection Within Hours' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.parcelTimeLine.collectionWithinHours|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Delivery Within Hours' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.parcelTimeLine.deliveryWithinHours|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Message for User' mod='linodott'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$data.parcelTimeLine.messageForUser|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>
	</div>

</div>

<div class="panel col-lg-12 mt-2">
	<div class="panel-heading">{l s='Produtos' mod='linodott'}<span class="badge">{$data.orderItems|@count|escape:'htmlall':'UTF-8'}</span>
		<span class="panel-heading-action"></span>
	</div>

	<div class="table-responsive-row clearfix">
		<table class="table customer">
			<thead>
			<tr class="nodrag nodrop">
				<th class="fixed-width-xs text-center"><span class="title_box">{l s='OrderItemId' mod='linodott'}</span></th>
				<th class=""><span class="title_box">{l s='Name' mod='linodott'}</span></th>
				<th class=""><span class="title_box">{l s='Quantity' mod='linodott'}</span></th>
				<th class=""><span class="title_box">{l s='ProductId' mod='linodott'}</span></th>
				<th class=""><span class="title_box">{l s='Product name' mod='linodott'}</span></th>
			</tr>
			</thead>

			<tbody>
			{if $data.orderItems|@count > 0}
				{foreach from=$data.orderItems item=prod}
					<tr class="odd">
						<td class="" >
							{$prod.orderItemId|escape:'htmlall':'UTF-8'}
						</td>
						<td class="" >
							{$prod.name|escape:'htmlall':'UTF-8'}
						</td>
						<td class="" >
							{$prod.quantity|escape:'htmlall':'UTF-8'}
						</td>
						<td class="" >
							{$prod.product.productId|escape:'htmlall':'UTF-8'}
						</td>
						<td class="" >
							{$prod.product.name|escape:'htmlall':'UTF-8'}
						</td>
					</tr>
				{/foreach}
			{else}
				<tr class="odd">
					<td colspan="8">{l s='Não ha produto' mod='linodott'}</td>
				</tr>
			{/if}
			</tbody>
		</table>
	</div>
</div>

{if $data.status == 'Initial' and $dottLogisticSystem != 1}
	<div class="panel col-lg-12 mt-2">
		<div class="panel-heading">{l s='Enviar número de Tracking' mod='linodott'}
			<span class="panel-heading-action"></span>
		</div>
		<div class="linoInlineForm">
			<form action="{$smarty.server.REQUEST_URI|escape:'html':'UTF-8'}" method="post" id="parcelTrackingDottForm">
				<input type="hidden" name="dottaction" value="postTracking">
				<input type="hidden" name="parcelId" value="{$data.id|escape:'htmlall':'UTF-8'}">
				<input type="text" id="trackingNumber" required name="trackingNumber" placeholder="{l s='Tracking Number' mod='linodott'}">
				<input type="text" id="trackingUrl" required name="trackingUrl"  placeholder="{l s='Tracking URL' mod='linodott'}">
				<input type="submit" id="parcelTrackingDott" name="parcelTrackingDott" class="btn btn-primary" value="{l s='Enviar' mod='linodott'}">
			</form>

		</div>
	</div>
{/if}