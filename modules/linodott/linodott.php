<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the GNU General Public License, version 3 (GPL-3.0).
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * @author    Li-Nó Design www.lino-design.com <contact@lino-design.com>
 * @copyright 2019 Li-Nó Design Lda
 * @license   https://opensource.org/licenses/GPL-3.0 GNU General Public License version 3
 *
 * Created by Li-Nó Design Lda
 * User: Li-Nó Design
 * Date: 15/11/2019
 * Time: 15:52
 */

require_once _PS_MODULE_DIR_ . 'linodott/classes/FeedDott.php';

if (!defined('_PS_VERSION_')) {
    exit;
}

class Linodott extends Module
{
    public function __construct()
    {
        $this->name                   = 'linodott';
        $this->tab                    = 'market_place';
        $this->version                = '1.2.16';
        $this->author                 = 'Li-No Design Lda';
        $this->need_instance          = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => '1.7');
        $this->bootstrap              = true;
        $this->module_key             = '86bc34d5c8ec5b21f591057c5eca4883';

        parent::__construct();
        $this->displayName      = $this->l('DOTT Marketplace');
        $this->description = $this->l('Manage DOTT Marketplace: manage orders, parcels, refunds, returns');
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

        if (! Configuration::get('MYMODULE_NAME')) {
            $this->warning = $this->l('No name provided');
        }

        // init CRON key
        $secure_key = Configuration::get('LINO_DOTT_CRON_KEY');
        if ($secure_key === false) {
            Configuration::updateValue('LINO_DOTT_CRON_KEY', Tools::strtoupper(Tools::passwdGen(16)));
        }
    }

    public function install()
    {
        // Create table linodott to register product selection
        $sqlTable = "CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."lino_dott` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `name` VARCHAR(255) NOT NULL,
			  `value` LONGTEXT  NOT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE="._MYSQL_ENGINE_." DEFAULT CHARSET=utf8;";
        Db::getInstance()->execute($sqlTable);

        // Create table lino_dott_ to register product selection
        $sqlTable = "CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."lino_dott_orders` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `order_id` VARCHAR(255) NOT NULL,
			  `order_number` VARCHAR(255) NOT NULL,
			  `ps_order_id` VARCHAR(255) NULL,
			  `order_state` VARCHAR(255) NOT NULL,
			  `update_date` DATETIME NOT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE="._MYSQL_ENGINE_." DEFAULT CHARSET=utf8;";
        Db::getInstance()->execute($sqlTable);

        $productLists = Configuration::get('LINO_DOTT_PRODUCTS_LIST');
        // Check if record already exists
        $linoDottExist = Db::getInstance()->getRow("Select value from `"._DB_PREFIX_."lino_dott` where name='products_list'");
        if (!$linoDottExist) {
            Db::getInstance()->execute("INSERT INTO `"._DB_PREFIX_."lino_dott` (`id`, `name`, `value`) VALUES (NULL, 'products_list', '".$productLists."')");
        }

        // Install Tabs
        if (!(int)Tab::getIdFromClassName('AdminDottManagement')) {
            $parent_tab = new Tab();

            // Need a foreach for the language
            foreach (Language::getLanguages() as $language) {
                $parent_tab->name[$language['id_lang']] = $this->l('Dott');
            }
            $parent_tab->class_name = 'AdminDottManagement';
            $parent_tab->id_parent  = 0; // Home tab
            $parent_tab->icon       = 'track_changes';
            $parent_tab->module     = $this->name;
            $parent_tab->add();
        }

        // Orders tab
        if (!(int)Tab::getIdFromClassName('AdminDottOrders')) {
            $tab1 = new Tab();

            foreach (Language::getLanguages() as $language) {
                $tab1->name[$language['id_lang']] = $this->l('Encomendas');
            }

            $tab1->class_name = 'AdminDottOrders';
            $tab1->icon       = 'track_changes';
            $tab1->id_parent  = (int)Tab::getIdFromClassName('AdminDottManagement');
            $tab1->module     = $this->name;
            $tab1->add();
        }
        // Parcel tab
        if (!(int)Tab::getIdFromClassName('AdminDottParcel')) {
            $tab1 = new Tab();

            foreach (Language::getLanguages() as $language) {
                $tab1->name[$language['id_lang']] = $this->l('Parcelas');
            }

            $tab1->class_name = 'AdminDottParcels';
            $tab1->id_parent  = (int)Tab::getIdFromClassName('AdminDottManagement');
            $tab1->module     = $this->name;
            $tab1->add();
        }
        // Returned tab
        if (!(int)Tab::getIdFromClassName('AdminDottReturn')) {
            $tab1 = new Tab();

            foreach (Language::getLanguages() as $language) {
                $tab1->name[$language['id_lang']] = $this->l('Retornos');
            }

            $tab1->class_name = 'AdminDottReturns';
            $tab1->id_parent  = (int)Tab::getIdFromClassName('AdminDottManagement');
            $tab1->module     = $this->name;
            $tab1->add();
        }
        // Refund tab
        if (!(int)Tab::getIdFromClassName('AdminDottRefunds')) {
            $tab1 = new Tab();

            foreach (Language::getLanguages() as $language) {
                $tab1->name[$language['id_lang']] = $this->l('Reembolsos');
            }

            $tab1->class_name = 'AdminDottRefunds';
            $tab1->id_parent  = (int)Tab::getIdFromClassName('AdminDottManagement');
            $tab1->module     = $this->name;
            $tab1->add();
        }
        // Feed tab
        if (!(int)Tab::getIdFromClassName('AdminDottFeedProducts')) {
            $tab1 = new Tab();

            foreach (Language::getLanguages() as $language) {
                $tab1->name[$language['id_lang']] = $this->l('Produtos a sincronizar');
            }

            $tab1->class_name = 'AdminDottFeedProducts';
            $tab1->icon       = 'track_changes';
            $tab1->id_parent  = (int)Tab::getIdFromClassName('AdminDottManagement');
            $tab1->module     = $this->name;
            $tab1->add();
        }
        if (!(parent::install()
                && $this->registerHook('actionAdminControllerSetMedia'))) {
            return false;
        }

        return true;
    }

    public function uninstall()
    {
        // Uninstall Tabs
        $moduleTabs = Tab::getCollectionFromModule($this->name);
        if (! empty($moduleTabs)) {
            foreach ($moduleTabs as $moduleTab) {
                $moduleTab->delete();
            }
        }

        // Remove configuration values
        Configuration::deleteByName('LINO_DOTT_KEY');
        Configuration::deleteByName('LINO_DOTT_API_URL');
        Configuration::deleteByName('LINO_DOTT_LANG');
        Configuration::deleteByName('LINO_DOTT_IMG_SIZE');
        Configuration::deleteByName('LINO_DOTT_MERCHANT_ID');
        Configuration::deleteByName('LINO_DOTT_DELEVERY_TIME');
        Configuration::deleteByName('LINO_DOTT_LOGISTIC_SYSTEM');
        Configuration::deleteByName('LINO_DOTT_OWN_INVOICE');
        Configuration::deleteByName('LINO_DOTT_SEPARATE_SHIPPING');
        Configuration::deleteByName('LINO_DOTT_CONFIRM_ORDERS');
        Configuration::deleteByName('LINO_DOTT_CRON_SHOP');
        Configuration::deleteByName('LINO_DOTT_IMPORT_ORDERS');
        Configuration::deleteByName('LINO_DOTT_IMPORT_ORDER_STATE');
        Configuration::deleteByName('LINO_DOTT_MODE');
        Configuration::deleteByName('LINO_DOTT_DEBUG_MODE');
        Configuration::deleteByName('LINO_DOTT_PRODUCTS_LIST');

        // Remove table
        $sqlTable = "DROP TABLE IF EXISTS `"._DB_PREFIX_."lino_dott_orders`;";
        Db::getInstance()->execute($sqlTable);

        $sqlTable = "DROP TABLE IF EXISTS `"._DB_PREFIX_."lino_dott`;";
        Db::getInstance()->execute($sqlTable);

        // Uninstall Module
        if (! parent::uninstall()) {
            return false;
        }

        return true;
    }


    public function hookActionAdminControllerSetMedia()
    {
        $controller = get_class($this->context->controller);
        if ($controller == 'AdminDottReturnsController' || $controller == 'AdminDottOrdersController'
            || $controller == 'AdminDottFeedProductsController'
            || $controller == 'AdminDottParcelsController' || $controller == 'AdminDottRefundsController') {
            $this->context->controller->addJs($this->_path . 'views/js/' . $this->name . '.js');
            $this->context->controller->addJs($this->_path . 'views/js/select2.min.js');
            $this->context->controller->addCSS($this->_path . 'views/css/' . $this->name . '.css');
            $this->context->controller->addCSS($this->_path . 'views/css/select2.min.css');
        }
        $this->context->controller->addCSS($this->_path . 'views/css/' . $this->name . '_global.css');
    }

    public function postProcess()
    {
        if (Tools::isSubmit('submitStoreConf')) {
            Configuration::updateValue('LINO_DOTT_KEY', Tools::getValue('LINO_DOTT_KEY'));
            Configuration::updateValue('LINO_DOTT_API_URL', Tools::getValue('LINO_DOTT_API_URL'));
            Configuration::updateValue('LINO_DOTT_MERCHANT_ID', Tools::getValue('LINO_DOTT_MERCHANT_ID'));
            Configuration::updateValue('LINO_DOTT_DELEVERY_TIME', Tools::getValue('LINO_DOTT_DELEVERY_TIME'));
            Configuration::updateValue('LINO_DOTT_LOGISTIC_SYSTEM', Tools::getValue('LINO_DOTT_LOGISTIC_SYSTEM'));
            Configuration::updateValue('LINO_DOTT_OWN_INVOICE', Tools::getValue('LINO_DOTT_OWN_INVOICE'));
            Configuration::updateValue('LINO_DOTT_SEPARATE_SHIPPING', Tools::getValue('LINO_DOTT_SEPARATE_SHIPPING'));
            Configuration::updateValue('LINO_DOTT_CONFIRM_ORDERS', Tools::getValue('LINO_DOTT_CONFIRM_ORDERS'));
            Configuration::updateValue('LINO_DOTT_CRON_SHOP', Tools::getValue('LINO_DOTT_CRON_SHOP'));
            Configuration::updateValue('LINO_DOTT_IMPORT_ORDERS', Tools::getValue('LINO_DOTT_IMPORT_ORDERS'));
            Configuration::updateValue('LINO_DOTT_IMPORT_ORDER_STATE', Tools::getValue('LINO_DOTT_IMPORT_ORDER_STATE'));
            Configuration::updateValue('LINO_DOTT_MODE', Tools::getValue('LINO_DOTT_MODE'));
            Configuration::updateValue('LINO_DOTT_DEBUG_MODE', Tools::getValue('LINO_DOTT_DEBUG_MODE'));
            Configuration::updateValue('LINO_DOTT_LANG', Tools::getValue('LINO_DOTT_LANG'));
            Configuration::updateValue('LINO_DOTT_IMG_SIZE', Tools::getValue('LINO_DOTT_IMG_SIZE'));


            // Add ps_order_id if not present
            $val = Db::getInstance()->executeS("SHOW COLUMNS FROM `"._DB_PREFIX_."lino_dott_orders` LIKE 'ps_order_id'");
            if (sizeof($val) == 0) {
                Db::getInstance()->execute("ALTER TABLE `"._DB_PREFIX_."lino_dott_orders` ADD  `ps_order_id` VARCHAR(255) NULL");
            }

            return $this->displayConfirmation($this->l('The settings have been updated.'));
        }

        return '';
    }

    public function getContent()
    {
        $this->_html = '';
        $this->_html .= $this->postProcess() . $this->renderForm();
        $this->_html .= $this->displayDottInformation();

        return $this->_html;
    }

    private function displayDottInformation()
    {
        $res = Db::getInstance()->getRow("Select value from `"._DB_PREFIX_."lino_dott` where name='products_list'");
        $selectedProductsId = "";
        if (is_array($res) && sizeof($res) > 0) {
            $selectedProductsId = $res["value"];
        }
        $selectedProductFromConf = Configuration::get('LINO_DOTT_PRODUCTS_LIST');

        $cronFeedLink = $this->context->link->getModuleLink('linodott', 'cronorder', array('cron_key' => Configuration::get('LINO_DOTT_CRON_KEY'), 'content_only' => '1', 'cronaction' => 'feed'));
        $cronOrderLink = $this->context->link->getModuleLink('linodott', 'cronorder', array('cron_key' => Configuration::get('LINO_DOTT_CRON_KEY'), 'content_only' => '1', 'cronaction' => 'order'));
        // $linoCronFeedDottPath = '(0 * * * *) wget -q -O/dev/null '.$cronFeedLink;
        // $linoCronOrderDottPath = '(0 * * * *) wget -q -O/dev/null '.$cronOrderLink;

        $linoCronFeedDottPath = '(0 * * * *) wget -q -O/dev/null '.$this->context->shop->getBaseURL().'modules/'.$this->name.'/cron.php?cron_key='.Configuration::get('LINO_DOTT_CRON_KEY');
        $linoCronOrderDottPath = '(0 * * * *) wget -q -O/dev/null '.$this->context->shop->getBaseURL().'modules/'.$this->name.'/cronOrders.php?cron_key='.Configuration::get('LINO_DOTT_CRON_KEY');

        $this->smarty->assign(array(
            'linoCronDottPath' => $linoCronFeedDottPath,
            'linoOrderCronDottPath' => $linoCronOrderDottPath,
            'linoFeedOfertaURL' => $this->context->shop->getBaseURL().'modules/'.$this->name.'/feedOferta/oferta.csv',
            'linoFeedCatalogURL' => $this->context->shop->getBaseURL().'modules/'.$this->name.'/feedCatalog/catalog.csv',
            'isDebug' => Configuration::get('LINO_DOTT_DEBUG_MODE') == 1 ? true : false,
            'currentSelectedValues' => $selectedProductsId,
            'selectedProductFromConf' => $selectedProductFromConf,
        ));
        return $this->display(__FILE__, 'dott.tpl');
    }

    public function renderForm()
    {
        $languages = $this->context->controller->getLanguages();
        $linoDottLangOptionsQ = array();
        foreach ($languages as $la) {
            $linoDottLangOptionsQ[] = array(
                'id' => $la["id_lang"],
                'name' => $la["name"]
            );
        }
        $imageTypes = ImageType::getImagesTypes(null, true);
        $linoDottImgSizeOptionsQ = array();
        $linoDottImgSizeOptionsDefault = "";
        foreach ($imageTypes as $im) {
            if ($im["products"]) {
                $linoDottImgSizeOptionsQ[] = array(
                    'id' => $im["name"],
                    'name' => $im["name"]
                );   
                if ($linoDottImgSizeOptionsDefault == "") {
                    $linoDottImgSizeOptionsDefault = $im["name"]; 
                }
            }
        }
        $stateList = OrderState::getOrderStates($this->context->language->id);
        $linoDottStateOptions = array();
        $linoDottStateOptionsDefault = $stateList[0]["id_order_state"];
        foreach ($stateList as $state) {
            $linoDottStateOptions[] = array(
                'id' => $state["id_order_state"],
                'name' => $state["name"]
            );
        }

        $linoDottShopsOptions = array();
        $linoDottShopsOptionsDefault = array();
        $shopList = Shop::getShops(true);
        $cpt = 0;
        foreach ($shopList as $shop) {
            $linoDottShopsOptions[] = array(
                'id' => $shop["id_shop"],
                'name' => $shop["name"]
            );
            if ($cpt == 0) {
                $linoDottShopsOptionsDefault = $shop["id_shop"];
            }
            $cpt++;
        }        

        $fields_form = array(
          'form' => array(
            'legend' => array(
              'title' => $this->l('Configurações DOTT Marketplace'),
              'icon'  => 'icon-cogs'
            ),
            'input'  => array(
              array(
                'type'  => 'text',
                'label' => $this->l('Api Key'),
                'name'  => 'LINO_DOTT_KEY',
              ),
              array(
                'type'  => 'text',
                'label' => $this->l('Api URL'),
                'name'  => 'LINO_DOTT_API_URL',
              ),
              array(
                'type'  => 'text',
                'label' => $this->l('Merchant ID'),
                'name'  => 'LINO_DOTT_MERCHANT_ID',
              ),
              array(
                'type' => 'select',
                'label' => $this->l('Idioma de exportação (selecione português)'),
                'name' => 'LINO_DOTT_LANG',
                'required' => true,
                'default_value' => (int)$this->context->country->id,
                'options' => array(
                    'query' => $linoDottLangOptionsQ,
                    'id' => 'id',
                    'name' => 'name'
                )
              ),
                array(
                'type' => 'select',
                'label' => $this->l('Tamanho de imagem'),
                'name' => 'LINO_DOTT_IMG_SIZE',
                'required' => true,
                'default_value' => $linoDottImgSizeOptionsDefault,
                'options' => array(
                    'query' => $linoDottImgSizeOptionsQ,
                    'id' => 'id',
                    'name' => 'name'
                )
              ),
                array(
                'type'  => 'text',
                'label' => $this->l('Tempo de entrega em horas (deixe vazio para usar os valores de Prestashop)'),
                'name'  => 'LINO_DOTT_DELEVERY_TIME',
              ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Expedição separada'),
                    'name' => 'LINO_DOTT_SEPARATE_SHIPPING',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'yes',
                            'value' => 1,
                            'label' => $this->l('Sim')
                        ),
                        array(
                            'id' => 'no',
                            'value' => 0,
                            'label' => $this->l('Não')
                        )
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Importar encomendas Dott no Prestashop (configurar o Cron)'),
                    'name' => 'LINO_DOTT_IMPORT_ORDERS',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'yes',
                            'value' => 1,
                            'label' => $this->l('Sim')
                        ),
                        array(
                            'id' => 'no',
                            'value' => 0,
                            'label' => $this->l('Não')
                        )
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Estatuto das encomendas importadas'),
                    'name' => 'LINO_DOTT_IMPORT_ORDER_STATE',
                    'required' => false,
                    'default_value' => (int)$linoDottStateOptionsDefault,
                    'options' => array(
                        'query' => $linoDottStateOptions,
                        'id' => 'id',
                        'name' => 'name'
                    ),
                ),                
                array(
                    'type' => 'select',
                    'label' => $this->l('Loja utilizada para o CRON de oferta'),
                    'name' => 'LINO_DOTT_CRON_SHOP',
                    'required' => false,
                    'default_value' => (int)$linoDottShopsOptionsDefault,
                    'options' => array(
                        'query' => $linoDottShopsOptions,
                        'id' => 'id',
                        'name' => 'name'
                    ),
                ),

                array(
                    'type' => 'switch',
                    'label' => $this->l('Aceitar automaticamente as encomendas(configurar o Cron)'),
                    'name' => 'LINO_DOTT_CONFIRM_ORDERS',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'yes',
                            'value' => 1,
                            'label' => $this->l('Sim')
                        ),
                        array(
                            'id' => 'no',
                            'value' => 0,
                            'label' => $this->l('Não')
                        )
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Utilizar o sistema logística da Dott'),
                    'name' => 'LINO_DOTT_LOGISTIC_SYSTEM',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'mode_prod',
                            'value' => 1,
                            'label' => $this->l('Sim')
                        ),
                        array(
                            'id' => 'mode_test',
                            'value' => 0,
                            'label' => $this->l('Não')
                        )
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Utilizar as suas próprias faturas'),
                    'name' => 'LINO_DOTT_OWN_INVOICE',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'mode_prod',
                            'value' => 1,
                            'label' => 'Sim'
                        ),
                        array(
                            'id' => 'mode_test',
                            'value' => 0,
                            'label' => 'Nao'
                        )
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Modo de funcionamento PRODUCTION'),
                    'name' => 'LINO_DOTT_MODE',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'mode_prod',
                            'value' => 1,
                            'label' => $this->l('Prod')
                        ),
                        array(
                            'id' => 'mode_test',
                            'value' => 0,
                            'label' => $this->l('Test')
                        )
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Modo Debug'),
                    'name' => 'LINO_DOTT_DEBUG_MODE',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'mode_debug',
                            'value' => 1,
                            'label' => $this->l('Sim')
                        ),
                        array(
                            'id' => 'mode_debug_no',
                            'value' => 0,
                            'label' => $this->l('Não')
                        )
                    ),
                ),
            ),
            'submit' => array(
              'title' => $this->l('Gravar')
            )
          ),
        );

        $helper                           = new HelperForm();
        $helper->show_toolbar             = false;
        $helper->table                    = $this->table;
        $lang                             = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language    = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ?
          Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form                = $fields_form;

        $helper->identifier    = $this->identifier;
        $helper->submit_action = 'submitStoreConf';
        $helper->currentIndex  = $this->context->link->getAdminLink(
            'AdminModules',
            false
        )
                                 . '&configure=' . $this->name . '&module_name=' . $this->name;
        $helper->token         = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars      = array(
          'fields_value' => $this->getConfigFieldsValues(),
          'languages'    => $languages,
          'id_language'  => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));
    }

    public function getConfigFieldsValues()
    {
        $fields                          = array();
        $fields['LINO_DOTT_KEY']         = Tools::getValue("LINO_DOTT_KEY", Configuration::get("LINO_DOTT_KEY"));
        $fields['LINO_DOTT_API_URL']     = Tools::getValue(
            "LINO_DOTT_API_URL",
            Configuration::get("LINO_DOTT_API_URL")
        );
        $fields['LINO_DOTT_MERCHANT_ID'] = Tools::getValue(
            "LINO_DOTT_MERCHANT_ID",
            Configuration::get("LINO_DOTT_MERCHANT_ID")
        );
        $fields['LINO_DOTT_DELEVERY_TIME'] = Tools::getValue(
            "LINO_DOTT_DELEVERY_TIME",
            Configuration::get("LINO_DOTT_DELEVERY_TIME")
        );
        $fields['LINO_DOTT_LOGISTIC_SYSTEM'] = Tools::getValue(
            "LINO_DOTT_LOGISTIC_SYSTEM",
            Configuration::get("LINO_DOTT_LOGISTIC_SYSTEM")
        );
        $fields['LINO_DOTT_OWN_INVOICE'] = Tools::getValue(
            "LINO_DOTT_OWN_INVOICE",
            Configuration::get("LINO_DOTT_OWN_INVOICE")
        );
        $fields['LINO_DOTT_SEPARATE_SHIPPING'] = Tools::getValue(
            "LINO_DOTT_SEPARATE_SHIPPING",
            Configuration::get("LINO_DOTT_SEPARATE_SHIPPING")
        );
        $fields['LINO_DOTT_MODE'] = Tools::getValue(
            "LINO_DOTT_MODE",
            Configuration::get("LINO_DOTT_MODE")
        );
        $fields['LINO_DOTT_CONFIRM_ORDERS'] = Tools::getValue(
            "LINO_DOTT_CONFIRM_ORDERS",
            Configuration::get("LINO_DOTT_CONFIRM_ORDERS")
        );        
        $fields['LINO_DOTT_CRON_SHOP'] = Tools::getValue(
            "LINO_DOTT_CRON_SHOP",
            Configuration::get("LINO_DOTT_CRON_SHOP")
        );
        $fields['LINO_DOTT_IMPORT_ORDERS'] = Tools::getValue(
            "LINO_DOTT_IMPORT_ORDERS",
            Configuration::get("LINO_DOTT_IMPORT_ORDERS")
        );
        $fields['LINO_DOTT_IMPORT_ORDER_STATE'] = Tools::getValue(
            "LINO_DOTT_IMPORT_ORDER_STATE",
            Configuration::get("LINO_DOTT_IMPORT_ORDER_STATE")
        );
        $fields['LINO_DOTT_DEBUG_MODE'] = Tools::getValue(
            "LINO_DOTT_DEBUG_MODE",
            Configuration::get("LINO_DOTT_DEBUG_MODE")
        );
        $fields['LINO_DOTT_LANG'] = Tools::getValue(
            "LINO_DOTT_LANG",
            Configuration::get("LINO_DOTT_LANG")
        );        
        $fields['LINO_DOTT_IMG_SIZE'] = Tools::getValue(
            "LINO_DOTT_IMG_SIZE",
            Configuration::get("LINO_DOTT_IMG_SIZE")
        );
        return $fields;
    }
}
