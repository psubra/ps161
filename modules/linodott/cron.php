<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the GNU General Public License, version 3 (GPL-3.0).
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * @author    Li-Nó Design www.lino-design.com <contact@lino-design.com>
 * @copyright 2019 Li-Nó Design Lda
 * @license   https://opensource.org/licenses/GPL-3.0 GNU General Public License version 3
 *
 * Created by Li-Nó Design Lda
 * User: Li-Nó Design
 * Date: 15/11/2019
 * Time: 15:52
 */

require_once(dirname(__FILE__) . '/../../config/config.inc.php');
require_once(dirname(__FILE__) . '/../../init.php');
require_once(dirname(__FILE__) . '/classes/FeedDott.php');


// Clean log files
$offerFeedDir = _PS_MODULE_DIR_."linodott/logs/cronOferta";
$files = scandir($offerFeedDir);
$ignored = array('.', '..', '.svn', '.htaccess', 'index.php');
foreach ($files as $f) {
    if (!in_array($f, $ignored) && !is_dir($f)) {
        $path = $offerFeedDir . DIRECTORY_SEPARATOR . $f;
        if (filemtime($path) < strtotime('-1 month')) {  // check how long it's been around
            unlink($path);  // remove it
        }
    }
}

$sGetKey = Tools::getValue('cron_key');
$sSecureKey = Configuration::get('LINO_DOTT_CRON_KEY');
$cronShopId = Configuration::get('LINO_DOTT_CRON_SHOP');

// Log file
$logHandle = fopen(_PS_ROOT_DIR_ . '/modules/linodott/logs/cronOferta/CronOferta_'.date("Y-m-d-His").'.log', 'a+');
fputs($logHandle, 'Cron Oferta - CRON Key = '.$sGetKey.', Secure Key =  '.$sSecureKey.', CALLED at ' . date('Y-m-d H:i:s').PHP_EOL);

if ($sGetKey == $sSecureKey) {
    $feedDott = new FeedDott(
        Context::getContext(),
        Configuration::get('LINO_DOTT_API_URL'),
        Configuration::get('LINO_DOTT_KEY'),
        $cronShopId
    );
    $feedDott->generateFeed();
    fputs($logHandle, 'Cron Oferta - File generated - ' . date('Y-m-d H:i:s').PHP_EOL);
    echo "ok";
} else {
    fputs($logHandle, 'Cron Oferta - CronKey and SecureKey are not the same - ' . date('Y-m-d H:i:s').PHP_EOL);
    echo "Wrong CRON KEY";
}

fclose($logHandle);
