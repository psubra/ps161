<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the GNU General Public License, version 3 (GPL-3.0).
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * @author    Li-Nó Design www.lino-design.com <contact@lino-design.com>
 * @copyright 2021 Li-Nó Design Lda
 * @license   https://opensource.org/licenses/GPL-3.0 GNU General Public License version 3
 */

require_once _PS_MODULE_DIR_.'linodott/classes/FeedDott.php';
require_once _PS_MODULE_DIR_.'linodott/classes/CurlRequest.php';
require_once _PS_MODULE_DIR_.'linodott/classes/OrderDott.php';

class LinodottCronorderModuleFrontController extends ModuleFrontControllerCore
{
    public function initContent()
    {
        parent::initContent();
        $cronAction = Tools::getValue('cronaction');
        $cronKey = Tools::getValue('cron_key');
        $sSecureKey = Configuration::get('LINO_DOTT_CRON_KEY');
        $cronShopId = Configuration::get('LINO_DOTT_CRON_SHOP');

        if ($cronAction == "feed") {
            // Clean log files
            $offerFeedDir = _PS_MODULE_DIR_."linodott/logs/cronOferta";
            $files = scandir($offerFeedDir);
            $ignored = array('.', '..', '.svn', '.htaccess', 'index.php');
            foreach ($files as $f) {
                if (!in_array($f, $ignored) && !is_dir($f)) {
                    $path = $offerFeedDir . DIRECTORY_SEPARATOR . $f;
                    if (filemtime($path) < strtotime('-1 month')) {  // check how long it's been around
                        unlink($path);  // remove it
                    }
                }
            }

            // Log file
            $logHandle = fopen(_PS_ROOT_DIR_ . '/modules/linodott/logs/cronOferta/CronOferta_'.date("Y-m-d-His").'.log', 'a+');
            fputs($logHandle, 'Cron Oferta - CRON Key = '.$cronKey.', Secure Key =  '.$sSecureKey.', CALLED at ' . date('Y-m-d H:i:s').PHP_EOL);

            if ($cronKey == $sSecureKey) {
                $feedDott = new FeedDott(
                    $this->context,
                    Configuration::get('LINO_DOTT_API_URL'),
                    Configuration::get('LINO_DOTT_KEY'),
                    $cronShopId
                );
                $feedDott->generateFeed();
                fputs($logHandle, 'Cron Oferta - File generated - ' . date('Y-m-d H:i:s').PHP_EOL);
                echo "ok";
            } else {
                fputs($logHandle, 'Cron Oferta - CronKey and SecureKey are not the same - ' . date('Y-m-d H:i:s').PHP_EOL);
                echo "Wrong CRON KEY";
            }
            fclose($logHandle);
        }
        if ($cronAction == "order") {
            // Clean log files
            $offerFeedDir = _PS_MODULE_DIR_."linodott/logs/cronOrders";
            $files = scandir($offerFeedDir);
            $ignored = array('.', '..', '.svn', '.htaccess', 'index.php');
            foreach ($files as $f) {
                if (!in_array($f, $ignored) && !is_dir($f)) {
                    $path = $offerFeedDir . DIRECTORY_SEPARATOR . $f;
                    if (filemtime($path) < strtotime('-1 month')) {  // check how long it's been around
                        unlink($path);  // remove it
                    }
                }
            }

            // Case multistore, get shop list
            $shopList = Shop::getShops(true, null, true);
            foreach ($shopList as $shopId) {
                Shop::setContext(Shop::CONTEXT_SHOP, $shopId);
                $this->processCronOrders($cronKey, $sSecureKey, $shopId);
            }
        }

        // $this->setTemplate(_PS_MODULE_DIR_.'linodott/views/templates/front/cron.tpl');
        $is17 = (bool)version_compare(_PS_VERSION_, '1.7', '>=');
        if ($is17) {
            $this->setTemplate('module:linodott/views/templates/front/cron.tpl');
        } else {
            $this->setTemplate('cron.tpl');
        }
        // $this->setTemplate('cron.tpl');
        // modules/linodott/views/templates/front/cron.tpl
    }

    private function processCronOrders($cronKey, $sSecureKey, $id_shop)
    {
        // Dott api security
        $apiUrl = Configuration::get('LINO_DOTT_API_URL');
        if (Tools::substr("$apiUrl", -1) != "/") {
            $apiUrl .= "/";
        }
        $bearerToken = Configuration::get('LINO_DOTT_KEY');

        // Settings
        $confirmOrders = Configuration::get('LINO_DOTT_CONFIRM_ORDERS');
        $importOrders = Configuration::get('LINO_DOTT_IMPORT_ORDERS');
        $importOrderState = Configuration::get('LINO_DOTT_IMPORT_ORDER_STATE');

        // $id_shop = Shop::getContextShopID(true);

        if ($cronKey == $sSecureKey) {
            if ($confirmOrders == 1 || $importOrders == 1) {
                $context = Context::getContext();

                // Log file
                $logHandle = fopen(_PS_ROOT_DIR_ . '/modules/linodott/logs/cronOrders/CronOrders_'
                    .date("Y-m-d-His").'.log', 'a+');
                fputs($logHandle, 'Cron Orders - SHOP ID = '.$id_shop.' CALLED at ' . date('Y-m-d H:i:s').PHP_EOL);

                // Get DOTT group if exists
                $dottGroup = GroupCore::searchByName('DOTT');

                // Get first Carrier
                $carriers = Carrier::getCarriers($context->language->id, true);
                $id_carrier = $carriers[0]["id_carrier"];

                // Get payment module
                $paymentmodules = PaymentModule::getInstalledPaymentModules();
                $paymentmodule = $paymentmodules[0];

                // Get Authorized payment orders from Dott
                $getParams = array();
                $getParams['limit'] = 100;
                $getParams['status'] = 'AuthorizedPayment';
                // debug purpose
                $getParams['status'] = 'Processed';
                $path     = "api/order/merchant";
                // Set up URL
                $url = $apiUrl . $path;
                $url = $url."?";
                $sep = "";
                foreach ($getParams as $key => $param) {
                    $url = $url.$sep.$key."=".$param;
                    $sep = "&";
                }
                fputs($logHandle, 'URL: '.$url.PHP_EOL);
                $curl2 = new CurlRequest($url, $bearerToken);

                $response = null;
                try {
                    $response = $curl2->executeGet();
                } catch (Exception $e) {
                    fputs($logHandle, 'Order DOTT API request - error: ' . print_r($e, true).PHP_EOL);
                }
                $data = json_decode(json_encode($response['body']), true);
                if (sizeof($data) > 0 && isset($data['totalRow'])) {
                    $totalRows = $data['totalRow'];
                    fputs($logHandle, 'Order DOTT API request - '.$totalRows.' orders to confirm'.PHP_EOL);
                }

                foreach ($data["data"] as $order) {
                    fputs($logHandle, 'Order DOTT To confirm : '.$order["id"].'-'.$order["orderNr"].PHP_EOL);

                    if ($confirmOrders == 1) {
                        // Call API
                        $orderId = $order["id"];
                        $method                      = "POST";
                        $path                        = "api/order/merchant/" . $orderId . "/confirm";
                        $url = $apiUrl . $path;
                        $curl2 = new CurlRequest($url, $bearerToken);
                        $response = null;
                        try {
                            $response = $curl2->executePost();
                            if ($response["code"] > 250) {
                                fputs($logHandle, 'Order DOTT API error while confirming : '.$order["id"].'-'.$order["orderNr"]." Error: ".$response["code"].PHP_EOL);
                            }
                        } catch (Exception $e) {
                            fputs($logHandle, 'Order DOTT API error while confirming : '.$order["id"].'-'.$order["orderNr"]." Error: ".print_r($e, true).PHP_EOL);
                        }

                        // Insert operation in lino_dott_orders
                        $sql = "INSERT INTO `"._DB_PREFIX_."lino_dott_orders` (`order_id`, `order_number`, `ps_order_id`, `order_state`, `update_date`)
                VALUES('".$order["id"]."', '".$order["orderNr"]."', '', '".OrderDott::ORDER_STATE_CONFIRMED."', NOW())";
                        $res = Db::getInstance()->execute($sql);
                        if (!$res) {
                            fputs($logHandle, 'Order DOTT confirmed but error while inserting in database: '.$order["id"].'-'.$order["orderNr"].PHP_EOL);
                        } else {
                            fputs($logHandle, 'Order DOTT confirmed '.$order["id"].'-'.$order["orderNr"].PHP_EOL);
                        }
                    }
                    if ($importOrders == 1) {
                        $currentOrder = OrderDott::getOrderByIdAndStatus($order["id"], OrderDott::ORDER_STATE_IMPORTED);
                        if (empty($currentOrder)) {
                            // Order has not beean already imported, we import it
                            $psOrderId = OrderDott::createdPSOrder($logHandle, $order, $context, $dottGroup, $id_carrier, $paymentmodule, $importOrderState);
                            $sql = "INSERT INTO `"._DB_PREFIX_."lino_dott_orders` (`order_id`, `order_number`, `ps_order_id`, `order_state`, `update_date`)
                    VALUES('".$order["id"]."', '".$order["orderNr"]."', '".$psOrderId."', '".OrderDott::ORDER_STATE_IMPORTED."', NOW())";
                            $res = Db::getInstance()->execute($sql);
                            if (!$res) {
                                fputs($logHandle, 'Order DOTT imported but error while inserting in database: '.$order["id"].'-'.$order["orderNr"].PHP_EOL);
                            } else {
                                fputs($logHandle, 'Order DOTT imported '.$order["id"].'-'.$order["orderNr"].PHP_EOL);
                            }
                        }
                    }
                }
                fclose($logHandle);
            }
            echo "Executado com sucesso - ShopID ".$id_shop.PHP_EOL.PHP_EOL;
        } else {
            echo "Wrong CRON KEY - ShopID ".$id_shop.PHP_EOL;
        }
    }
}
