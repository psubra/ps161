<?php
/**
* NOTICE OF LICENSE
*
* This file is licenced under the GNU General Public License, version 3 (GPL-3.0).
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* @author    Li-Nó Design www.lino-design.com <contact@lino-design.com>
* @copyright 2019 Li-Nó Design Lda
* @license   https://opensource.org/licenses/GPL-3.0 GNU General Public License version 3
*/

require_once _PS_MODULE_DIR_.'linodott/classes/CurlRequest.php';

class AdminDottReturnsController extends ModuleAdminController
{
    const ACTION_LIST = "returnList";
    const ACTION_VIEW = "returnview";
    const ACTION_CREATE = "returncreate";
    const ACTION_ACCEPT = "returnaccept";
    const ACTION_CANCEL = "returncancel";
    const ACTION_RECEIVE = "returnreceive";
    const ACTION_REFUND = "returnrefund";
    const ITEMS_BY_PAGE = 30;

    private $apiUrl;
    private $bearerToken;

    public function __construct()
    {
        $this->modulename  = 'linodott';
        $this->bootstrap   = true;
        $this->lang        = true;
        $this->apiUrl      = Configuration::get('LINO_DOTT_API_URL');
        $this->bearerToken = Configuration::get('LINO_DOTT_KEY');
        if (Tools::substr("$this->apiUrl", -1) != "/") {
            $this->apiUrl .= "/";
        }
        parent::__construct();
    }

    public function initContent()
    {
        parent::initContent();

        $dottaction     = Tools::getValue('dottaction');
        $returnMsg      = Tools::getValue('returnMsg');
        $errorMsg       = Tools::getValue('errorMsg');
        $page       = Tools::getValue('page');
        if (empty($page)) {
            $page = 1;
        }
        $method         = "GET";
        $message        = "";
        $postParams     = array();
        $redirectParams = array();
        $totalRows = 0;
        $nbPages = 0;
        if ($dottaction == self::ACTION_VIEW) {
            $returnId = Tools::getValue('returnId');
            if ($returnId == null) {
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminDottReturns', true)
                                     . "&action=" . self::ACTION_LIST);
            }
            $path     = "api/return/" . $returnId;
            $template = "linodott/views/templates/admin/returns/returnDetail.tpl";
        } elseif ($dottaction == self::ACTION_CREATE) {
            $returnId = Tools::getValue('returnId');
            $docId    = Tools::getValue('docId');
            if ($returnId == null || $docId == null) {
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminDottReturns', true)
                                     . "&action=" . self::ACTION_LIST);
            }
            $path     = "api/order/merchant/" . $returnId . "/document/" . $docId;
            $template = "linodott/views/templates/admin/orders/orderDetail.tpl";
        } elseif ($dottaction == self::ACTION_ACCEPT) {
            $returnId = Tools::getValue('returnId');
            if ($returnId == null) {
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminDottReturns', true)
                                     . "&action=" . self::ACTION_LIST);
            }
            $method                      = "POST";
            $path                        = "api/return/" . $returnId . "/accept";
            $redirectParams['returnMsg'] = $this->module->l('Your return has been accepted', 'linodott');
            $redirectParams['returnId']  = $returnId;
            $redirectParams['action']    = self::ACTION_VIEW;
        } elseif ($dottaction == self::ACTION_CANCEL) {
            $returnId = Tools::getValue('returnId');
            if ($returnId == null) {
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminDottReturns', true)
                                     . "&action=" . self::ACTION_LIST);
            }
            $method                      = "POST";
            $path                        = "api/return/" . $returnId . "/cancel";
            $redirectParams['returnMsg'] = $this->module->l('Your return has been cancelled', 'linodott');
            $redirectParams['returnId']  = $returnId;
            $redirectParams['action']    = self::ACTION_VIEW;
        } elseif ($dottaction == self::ACTION_RECEIVE) {
            $returnId = Tools::getValue('returnId');
            $orderId = Tools::getValue('orderId');
            if ($returnId == null) {
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminDottReturns', true)
                                     . "&action=" . self::ACTION_LIST);
            }
            $method                      = "POST";
            $path                        = "api/return/" . $returnId . "/receive";
            $redirectParams['returnMsg'] = $this->module->l('Your return has been updated to receive', 'linodott');
            $redirectParams['returnId']  = $returnId;
            $redirectParams['orderId']  = $orderId;
            $redirectParams['action']    = self::ACTION_REFUND;
        } elseif ($dottaction == self::ACTION_REFUND) {
            $returnId = Tools::getValue('returnId');
            $orderId  = Tools::getValue('orderId');
            if ($returnId == null || orderId == null) {
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminDottReturns', true)
                                     . "&action=" . self::ACTION_LIST);
            }
            $method                        = "POST";
            $path                          = "api/refund/return";
            $postParams["merchantOrderId"] = $orderId;
            $postParams["returnId"]        = $returnId;
            $redirectParams['returnMsg']   = $this->module->l('Your refund has been requested', 'linodott');
            $redirectParams['returnId']    = $returnId;
            $redirectParams['action']      = self::ACTION_VIEW;
        } else {
            $path     = "api/return";
            $template = "linodott/views/templates/admin/returns/returnList.tpl";
        }

        // Only if configuration filled
        $data = array();
        $data["data"] = array();
        $data["par"] = 0;
        if ($this->apiUrl != "" && $this->bearerToken != "") {
            $url = $this->apiUrl . $path;
            if (sizeof($postParams) > 0) {
                $url = $url."?";
                $sep = "";
                foreach ($postParams as $key => $param) {
                    $url = $url.$sep.$key."=".$param;
                    $sep = "&";
                }
            }
            $curl2 = new CurlRequest($url, $this->bearerToken);

            $response = null;
            try {
                if ($method == "POST") {
                    $curl2->setParams($postParams);
                    $response = $curl2->executePost();
                    if ($response["code"] > 250) {
                        $errorMsg = $this->module->l('An error happened..', 'linodott');
                    }
                } else {
                    $response = $curl2->executeGet();
                }
            } catch (Exception $e) {
                $errorMsg = $e->getMessage();
            }

            if (sizeof($redirectParams) > 0) {
                $redirectParams['errorMsg'] = $errorMsg;
                $redirectParamsStr          = "";
                foreach ($redirectParams as $key => $value) {
                    $redirectParamsStr .= "&" . $key . "=" . $value;
                }
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminDottReturns', true)
                                     . $redirectParamsStr);
            }
            $data = json_decode(json_encode($response['body']), true);
            if (sizeof($data) > 0 && isset($data['totalRow'])) {
                $totalRows = $data['totalRow'];
                if ($totalRows > 0) {
                    $nbPages = ceil($totalRows / $this::ITEMS_BY_PAGE);
                }
            }
        } else {
            $errorMsg = $this->module->l('Please configure the API key and API Url first', 'linodott');
        }

        // Smarty on fetch template
        $this->context->smarty->assign(array(
          'data'      => $data,
          'totalRows' => $totalRows,
          'page'      => $page,
          'nbPages'   => $nbPages,
          'message'   => $message,
          'returnMsg' => $returnMsg,
          'errorMsg'  => $errorMsg
        ));
        $content = $this->context->smarty->fetch(_PS_MODULE_DIR_ . $template);

        // Smarty on general layout
        $this->context->smarty->assign(array(
          'content' => $this->content . $content,
        ));
    }
}
