<?php
/**
* NOTICE OF LICENSE
*
* This file is licenced under the GNU General Public License, version 3 (GPL-3.0).
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* @author    Li-Nó Design www.lino-design.com <contact@lino-design.com>
* @copyright 2019 Li-Nó Design Lda
* @license   https://opensource.org/licenses/GPL-3.0 GNU General Public License version 3
*/

require_once _PS_MODULE_DIR_.'linodott/classes/CurlRequest.php';

class AdminDottOrdersController extends ModuleAdminController
{
    const ACTION_LIST = "orderList";
    const ACTION_VIEW = "vieworder";
    const ACTION_VIEW_DOCUMENT = "viewdocument";
    const ACTION_ORDER_CONFIRM = "orderconfirm";
    const ACTION_ORDER_CANCEL = "ordercancel";
    const ACTION_ORDER_PROCESS = "orderprocess";
    const ACTION_UPLOAD_DOC = "uploaddocument";
    const ITEMS_BY_PAGE = 30;

    private $apiUrl;
    private $bearerToken;

    public function __construct()
    {
        $this->modulename  = 'linodott';
        $this->bootstrap   = true;
        $this->lang        = true;
        $this->apiUrl      = Configuration::get('LINO_DOTT_API_URL');
        $this->bearerToken = Configuration::get('LINO_DOTT_KEY');
        if (Tools::substr("$this->apiUrl", -1) != "/") {
            $this->apiUrl .= "/";
        }
        parent::__construct();
    }

    public function initContent()
    {
        parent::initContent();

        $dottaction     = Tools::getValue('dottaction');
        $returnMsg      = Tools::getValue('returnMsg');
        $errorMsg       = Tools::getValue('errorMsg');
        $page           = Tools::getValue('page');
        if (empty($page)) {
            $page = 1;
        }
        $method         = "GET";
        $message        = "";
        $postParams     = array();
        $getParams     = array();
        $redirectParams = array();
        $isFile         = false;
        if ($dottaction == self::ACTION_VIEW) {
            $orderId = Tools::getValue('orderId');
            if ($orderId == null) {
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminDottOrders', true)
                                     . "&action=" . self::ACTION_LIST);
            }
            $path     = "api/order/merchant/" . $orderId;
            $template = "linodott/views/templates/admin/orders/orderDetail.tpl";
        } elseif ($dottaction == self::ACTION_VIEW_DOCUMENT) {
            $orderId = Tools::getValue('orderId');
            $docId   = Tools::getValue('docId');
            $docName   = Tools::getValue('docName');
            if ($orderId == null || $docId == null) {
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminDottOrders', true)
                                     . "&dottaction=" . self::ACTION_LIST);
            }
            if (empty($docName)) {
                $docName = "Dott_document.pdf";
            }
            $path     = "api/order/merchant/" . $orderId . "/document/" . $docId;
            $template = "linodott/views/templates/admin/orders/orderDetail.tpl";
            $isFile   = true;
        } elseif ($dottaction == self::ACTION_ORDER_CONFIRM) {
            $orderId = Tools::getValue('orderId');
            if ($orderId == null) {
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminDottOrders', true)
                                     . "&action=" . self::ACTION_LIST);
            }
            $method                      = "POST";
            $path                        = "api/order/merchant/" . $orderId . "/confirm";
            $redirectParams['returnMsg'] = $this->module->l('Your order has been confirmed', 'linodott');
            $redirectParams['orderId']   = $orderId;
            $redirectParams['action']    = self::ACTION_VIEW;
        } elseif ($dottaction == self::ACTION_ORDER_CANCEL) {
            $orderId = Tools::getValue('orderId');
            if ($orderId == null) {
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminDottOrders', true)
                                     . "&dottaction=" . self::ACTION_LIST);
            }
            $method                      = "POST";
            $path                        = "api/order/merchant/" . $orderId . "/cancel";
            $redirectParams['returnMsg'] = $this->module->l('Your order has been cancelled', 'linodott');
            $redirectParams['orderId']   = $orderId;
            $redirectParams['dottaction']    = self::ACTION_VIEW;
        } elseif ($dottaction == self::ACTION_ORDER_PROCESS) {
            $orderId = Tools::getValue('orderId');
            if ($orderId == null) {
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminDottOrders', true)
                                     . "&dottaction=" . self::ACTION_LIST);
            }
            $method                      = "POST";
            $path                        = "api/order/merchant/" . $orderId . "/process";
            $redirectParams['returnMsg'] = $this->module->l('Your order has been updated to proceccing', 'linodott');
            $redirectParams['orderId']   = $orderId;
            $redirectParams['dottaction']    = self::ACTION_VIEW;
        } elseif ($dottaction == self::ACTION_UPLOAD_DOC) {
            $orderId = Tools::getValue('orderId');
            $documentType = Tools::getValue('documentType');
            if ($orderId == null || $documentType == null) {
                $redirectParams['returnMsg'] = $this->module->l('Document Typr missing', 'linodott');
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminDottOrders', true)
                    . "&dottaction=" . self::ACTION_VIEW."&orderId=".$orderId."&returnMsg=".$redirectParams['returnMsg']);
            }
            // File upload
            $fileUpload = $_FILES['uploadDottFile'];
            if (empty($fileUpload['name']) || empty($fileUpload['tmp_name'])) {
                $redirectParams['returnMsg'] = $this->module->l('File upload missing', 'linodott');
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminDottOrders', true)
                    . "&dottaction=" . self::ACTION_VIEW."&orderId=".$orderId."&returnMsg=".$redirectParams['returnMsg']);
            }
            $extension = pathinfo($fileUpload['name'], PATHINFO_EXTENSION);
            $allowed = array('.doc', '.docx', '.pdf', '.png', '.jpeg', '.jpg');
            if (in_array($extension, $allowed)) {
                $redirectParams['returnMsg'] = $this->module->l('File extension not allowed', 'linodott');
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminDottOrders', true)
                    . "&dottaction=" . self::ACTION_VIEW."&orderId=".$orderId."&returnMsg=".$redirectParams['returnMsg']);
            }
            $filename = uniqid().basename($fileUpload['name']);
            $filename = str_replace(' ', '-', $filename);
            $filename = Tools::strtolower($filename);
            $filename = filter_var($filename, FILTER_SANITIZE_STRING);
            $uploader = new UploaderCore();
            $dest =_PS_ROOT_DIR_."/modules/linodott/upload/";
            $uploader->setSavePath($dest);
            $fileUpload['name'] = $filename;
            $uploader->upload($fileUpload, $filename);
            $fileContent = Tools::file_get_contents($dest.$filename);
            $postParams["document"] = array();
            $postParams["document"]["fileName"] = $filename;
            $postParams["document"]["contentBase64"] = base64_encode($fileContent);

            unlink($dest.$filename);
            $method                      = "POST";
            $path                        = "api/order/merchant/" . $orderId . "/document/upload/".$documentType;
            $redirectParams['returnMsg'] = $this->module->l('Your document has been uploaded with success', 'linodott');
            $redirectParams['orderId']   = $orderId;
            $redirectParams['dottaction']    = self::ACTION_VIEW;
        } else {
            $getParams['limit'] = $this::ITEMS_BY_PAGE;
            $getParams['offset'] = ($page - 1) * $this::ITEMS_BY_PAGE;
            $path     = "api/order/merchant";
            $template = "linodott/views/templates/admin/orders/orderList.tpl";
        }

        // Only if configuration filled
        $data = array();
        $data["data"] = array();
        $dataDocument = array();
        $totalRows = 0;
        $nbPages = 0;
        if ($this->apiUrl != "" && $this->bearerToken != "") {
            $url = $this->apiUrl . $path;
            if (sizeof($getParams) > 0) {
                $url = $url."?";
                $sep = "";
                foreach ($getParams as $key => $param) {
                    $url = $url.$sep.$key."=".$param;
                    $sep = "&";
                }
            }
            $curl2 = new CurlRequest($url, $this->bearerToken);
            $fp = null;
            if ($isFile) {
                header('Content-Transfer-Encoding: binary');
                header('Content-Type: application/pdf');
                header('Content-Disposition: attachment; filename="' . utf8_decode($docName) . '"');
                $response = $curl2->executeGetFile();
                echo $response;
                exit;
            }

            $isDebug = Configuration::get('LINO_DOTT_DEBUG_MODE') == 1 ? true : false;
            if ($isDebug) {
                $logHandle = fopen(_PS_ROOT_DIR_ . '/modules/linodott/logs/orderList/Orders_'
                    .date("Y-m-d-His").'.log', 'a+');
                fputs($logHandle, 'Order list - CALLED at ' . date('Y-m-d H:i:s').PHP_EOL);
                fputs($logHandle, 'URL: '.$url.PHP_EOL);
                fputs($logHandle, 'Bearer: '.$this->bearerToken.PHP_EOL);
            }

            $response = null;
            try {
                if ($method == "POST") {
                    $curl2->setParams($postParams);
                    $response = $curl2->executePost();
                    if ($response["code"] > 250) {
                        $errorMsg = $this->module->l('An error happened..', 'linodott');
                    }
                } else {
                    $response = $curl2->executeGet();
                }
            } catch (Exception $e) {
                if ($isDebug) {
                    fputs($logHandle, 'Result Final: ' . print_r($e, true).PHP_EOL);
                }
                $errorMsg = $e->getMessage();
            }
            if ($isFile) {
                fclose($fp);
            }

            if (sizeof($redirectParams) > 0) {
                $redirectParams['errorMsg'] = $errorMsg;
                $redirectParamsStr          = "";
                foreach ($redirectParams as $key => $value) {
                    $redirectParamsStr .= "&" . $key . "=" . $value;
                }
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminDottOrders', true)
                                     . $redirectParamsStr);
            }
            $data = json_decode(json_encode($response['body']), true);
            if (sizeof($data) > 0 && isset($data['totalRow'])) {
                $totalRows = $data['totalRow'];
                if ($totalRows > 0) {
                    $nbPages = ceil($totalRows / $this::ITEMS_BY_PAGE);
                }
            }

            // Get document
            if ($dottaction == self::ACTION_VIEW) {
                $path     = "api/order/merchant/" . $orderId . "/document";
                $curl2    = new CurlRequest($this->apiUrl . $path, $this->bearerToken);
                $response = null;
                try {
                    $response     = $curl2->executeGet();
                    $responsejson = json_decode(json_encode($response['body']), true);
                    if (sizeof($responsejson['data']) > 0) {
                        $dataDocument = $responsejson['data'];
                    }
                } catch (Exception $e) {
                    $errorMsg = $e->getMessage();
                }
            }
            if ($isDebug) {
                fclose($logHandle);
            }
        } else {
            $errorMsg = $this->module->l('Please configure the API key and API Url first', 'linodott');
        }

        $logFiles = $this->getLastXFiles(_PS_MODULE_DIR_."linodott/logs/orderList");

        // Smarty on fetch template
        $this->context->smarty->assign(array(
          'data'      => $data,
          'totalRows' => $totalRows,
          'page'      => $page,
          'nbPages'   => $nbPages,
          'message'   => $message,
          'returnMsg' => $returnMsg,
          'errorMsg'  => $errorMsg,
          'documents' => $dataDocument,
          'ownInvoice' => Configuration::get('LINO_DOTT_OWN_INVOICE'),
          'logFiles' => $logFiles,
          'isDebug'   => Configuration::get('LINO_DOTT_DEBUG_MODE')
        ));
        $content = $this->context->smarty->fetch(_PS_MODULE_DIR_ . $template);

        // Smarty on general layout
        $this->context->smarty->assign(array(
          'content' => $this->content . $content,
        ));
    }

    private function getLastXFiles($dir)
    {
        $offerFeedDir = $dir;
        $allFiles = array();
        $files = scandir($offerFeedDir);
        $ignored = array('.', '..', '.svn', '.htaccess', 'index.php');
        foreach ($files as $f) {
            if (!in_array($f, $ignored) && !is_dir($f)) {
                $path = $dir . DIRECTORY_SEPARATOR . $f;
                if (filemtime($path) < strtotime('-1 month')) {  // check how long it's been around
                    unlink($path);  // remove it
                } else {
                    $allFiles[$f] = filemtime($offerFeedDir . '/' . $f);
                }
            }
        }
        arsort($allFiles);
        $resultFinal = array_slice($allFiles, 0, 10);
        return $resultFinal;
    }
}
