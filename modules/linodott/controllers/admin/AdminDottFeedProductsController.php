<?php
/**
* NOTICE OF LICENSE
*
* This file is licenced under the GNU General Public License, version 3 (GPL-3.0).
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* @author    Li-Nó Design www.lino-design.com <contact@lino-design.com>
* @copyright 2019 Li-Nó Design Lda
* @license   https://opensource.org/licenses/GPL-3.0 GNU General Public License version 3
*/

require_once _PS_MODULE_DIR_.'linodott/classes/CurlRequest.php';

class AdminDottFeedProductsController extends ModuleAdminController
{
    const ACTION_LIST = "orderList";
    const ACTION_VIEW = "vieworder";
    const ACTION_VIEW_DOCUMENT = "viewdocument";
    const ACTION_ORDER_CONFIRM = "orderconfirm";
    const ACTION_ORDER_CANCEL = "ordercancel";
    const ACTION_ORDER_PROCESS = "orderprocess";

    private $apiUrl;
    private $bearerToken;
    public $returnMsg;
    public $errorMessage;
    public $page;
    public $resultsPerPage;
    public $filterPageChanged;
    public $priceVariationDirection;
    public $priceVariationQuantity;
    public $priceVariationType;
    public $totalResults;
    public $orderCol;
    public $orderWay;
    public $results;
    public $brands;
    public $brand;
    public $suppliers;
    public $supplier;
    public $categories;
    public $category;
    public $priceLow;
    public $priceUp;
    public $stockLow;
    public $stockUp;
    public $productEnabled;
    public $ean;
    public $reference;

    public function __construct()
    {
        $this->modulename  = 'linodott';
        $this->bootstrap   = true;
        $this->lang        = true;
        $this->results      = array();
        $this->filterPageChanged = false;
        $this->brands      = array();
        $this->suppliers      = array();
        $this->categories      = array();
        $this->apiUrl      = Configuration::get('LINO_DOTT_API_URL');
        $this->bearerToken = Configuration::get('LINO_DOTT_KEY');
        if (Tools::substr("$this->apiUrl", -1) != "/") {
            $this->apiUrl .= "/";
        }

        parent::__construct();
    }


    public function init()
    {
        parent::init();
        $this->page           = Tools::getValue('page');
        $this->orderCol       = Tools::getValue('orderCol');
        $this->orderWay       = Tools::getValue('orderWay');
        $this->priceVariationDirection = Tools::getValue('priceVariationDirection');
        $this->priceVariationQuantity = Tools::getValue('priceVariationQuantity');
        $this->priceVariationType = Tools::getValue('priceVariationType');
        if (empty($this->priceVariationDirection)) {
            $this->priceVariationDirection = Configuration::get('LINO_DOTT_PRODUCTS_PRICE_VARIATION_DIR');
            if (empty($this->priceVariationDirection)) {
                $this->priceVariationDirection = "";
            }
        }
        if (empty($this->priceVariationQuantity)) {
            $this->priceVariationQuantity = Configuration::get('LINO_DOTT_PRODUCTS_PRICE_VARIATION_QT');
            if (empty($this->priceVariationQuantity)) {
                $this->priceVariationQuantity = 0;
            }
        }
        if (empty($this->priceVariationType)) {
            $this->priceVariationType = Configuration::get('LINO_DOTT_PRODUCTS_PRICE_VARIATION_TYPE');
            if (empty($this->priceVariationType)) {
                $this->priceVariationType = "perc";
            }
        }
        Configuration::updateValue('LINO_DOTT_PRODUCTS_PRICE_VARIATION_DIR', $this->priceVariationDirection);
        Configuration::updateValue('LINO_DOTT_PRODUCTS_PRICE_VARIATION_QT', $this->priceVariationQuantity);
        Configuration::updateValue('LINO_DOTT_PRODUCTS_PRICE_VARIATION_TYPE', $this->priceVariationType);
        if (Tools::isSubmit('productDottFilter')) {
            $this->ean = Tools::getValue('ean');
            Configuration::updateValue('LINO_DOTT_PRODUCTS_FILTER_EAN', $this->ean);
            $this->reference = Tools::getValue('reference');
            Configuration::updateValue('LINO_DOTT_PRODUCTS_FILTER_REFERENCE', $this->reference);
            $this->priceLow = Tools::getValue('priceLow');
            Configuration::updateValue('LINO_DOTT_PRODUCTS_FILTER_PR_LOW', $this->priceLow);
            $this->priceUp = Tools::getValue('priceUp');
            Configuration::updateValue('LINO_DOTT_PRODUCTS_FILTER_PR_UP', $this->priceUp);
            $this->stockLow = Tools::getValue('stockLow');
            Configuration::updateValue('LINO_DOTT_PRODUCTS_FILTER_ST_LOW', $this->stockLow);
            $this->stockUp = Tools::getValue('stockUp');
            Configuration::updateValue('LINO_DOTT_PRODUCTS_FILTER_ST_UP', $this->stockUp);
            $this->productEnabled = Tools::getValue('productEnabled');
            Configuration::updateValue('LINO_DOTT_PRODUCTS_FILTER_PROD_ENABLED', $this->productEnabled);
            $this->brand = Tools::getValue('brand');
            if (empty($this->brand)) {
                $this->brand = array();
                Configuration::updateValue('LINO_DOTT_PRODUCTS_FILTER_BRAND', "");
            } else {
                Configuration::updateValue('LINO_DOTT_PRODUCTS_FILTER_BRAND', empty($this->brand) ? "" : implode(";", $this->brand));
            }
            $this->supplier = Tools::getValue('supplier');
            if (empty($this->supplier)) {
                $this->supplier = array();
                Configuration::updateValue('LINO_DOTT_PRODUCTS_FILTER_SUPPLIER', "");
            } else {
                Configuration::updateValue('LINO_DOTT_PRODUCTS_FILTER_SUPPLIER', empty($this->supplier) ? "" : implode(";", $this->supplier));
            }
            $this->category = Tools::getValue('category');
            if (empty($this->category)) {
                $this->category = array();
                Configuration::updateValue('LINO_DOTT_PRODUCTS_FILTER_CATEGORY', "");
            } else {
                $categoryString = implode(";", $this->category);
                Configuration::updateValue('LINO_DOTT_PRODUCTS_FILTER_CATEGORY', empty($this->category) ? "" : $categoryString);
            }
            $this->page = 1;
            $this->filterPageChanged = true;
        } else {
            $this->ean = Configuration::get('LINO_DOTT_PRODUCTS_FILTER_EAN');
            $this->reference = Configuration::get('LINO_DOTT_PRODUCTS_FILTER_REFERENCE');
            $this->priceLow = Configuration::get('LINO_DOTT_PRODUCTS_FILTER_PR_LOW');
            $this->priceUp = Configuration::get('LINO_DOTT_PRODUCTS_FILTER_PR_UP');
            $this->stockLow = Configuration::get('LINO_DOTT_PRODUCTS_FILTER_ST_LOW');
            $this->stockUp = Configuration::get('LINO_DOTT_PRODUCTS_FILTER_ST_UP');
            $this->productEnabled = Configuration::get('LINO_DOTT_PRODUCTS_FILTER_PROD_ENABLED');
            if (Configuration::get('LINO_DOTT_PRODUCTS_FILTER_BRAND') == null || Configuration::get('LINO_DOTT_PRODUCTS_FILTER_BRAND') == "") {
                $this->brand = array();
            } else {
                $this->brand = explode(";", Configuration::get('LINO_DOTT_PRODUCTS_FILTER_BRAND'));
            }
            // $this->brand = empty(Configuration::get('LINO_DOTT_PRODUCTS_FILTER_BRAND')) ? array() : explode(";", Configuration::get('LINO_DOTT_PRODUCTS_FILTER_BRAND'));
            if (Configuration::get('LINO_DOTT_PRODUCTS_FILTER_SUPPLIER') == null || Configuration::get('LINO_DOTT_PRODUCTS_FILTER_SUPPLIER') == "") {
                $this->supplier = array();
            } else {
                $this->supplier = explode(";", Configuration::get('LINO_DOTT_PRODUCTS_FILTER_SUPPLIER'));
            }
            // $this->supplier = empty(Configuration::get('LINO_DOTT_PRODUCTS_FILTER_SUPPLIER')) ? array() : explode(";", Configuration::get('LINO_DOTT_PRODUCTS_FILTER_SUPPLIER'));
            $categories = Configuration::get('LINO_DOTT_PRODUCTS_FILTER_CATEGORY');
            if (Configuration::get('LINO_DOTT_PRODUCTS_FILTER_CATEGORY') == null || Configuration::get('LINO_DOTT_PRODUCTS_FILTER_CATEGORY') == "") {
                $this->category = array();
            } else {
                $this->category = explode(";", Configuration::get('LINO_DOTT_PRODUCTS_FILTER_CATEGORY'));
            }
            // $this->category = empty(Configuration::get('LINO_DOTT_PRODUCTS_FILTER_CATEGORY')) ? array() : explode(";", Configuration::get('LINO_DOTT_PRODUCTS_FILTER_CATEGORY'));
        }
        if (Tools::isSubmit('productDottList')) {
            if (Configuration::get('LINO_DOTT_PRODUCTS_RES_PAGE') != Tools::getValue('resultsPerPage')) {
                $this->page = 1;
                $this->filterPageChanged = true;
            }
            Configuration::updateValue('LINO_DOTT_PRODUCTS_RES_PAGE', Tools::getValue('resultsPerPage'));
        }
        if ($this->page == null) {
            $this->page = 1;
        }

        $this->resultsPerPage  = Configuration::get('LINO_DOTT_PRODUCTS_RES_PAGE');
        if (empty($this->resultsPerPage)) {
            Configuration::updateValue('LINO_DOTT_PRODUCTS_RES_PAGE', 30);
            $this->resultsPerPage  = Configuration::get('LINO_DOTT_PRODUCTS_RES_PAGE');
        }
        if ($this->orderCol == null) {
            $this->orderCol = "id_product";
        }
        if ($this->orderWay == null) {
            $this->orderWay = "asc";
        }

        $this->totalResults = sizeof($this->getAllProducts(
            $this->context->language->id,
            0,
            -1,
            $this->orderCol,
            $this->orderWay,
            $this->category,
            $this->supplier,
            $this->brand,
            $this->ean,
            $this->reference,
            $this->priceLow,
            $this->priceUp,
            $this->stockLow,
            $this->stockUp,
            $this->productEnabled
        ));

        $this->results = $this->getAllProducts(
            $this->context->language->id,
            $this->resultsPerPage * ($this->page - 1),
            $this->resultsPerPage,
            $this->orderCol,
            $this->orderWay,
            $this->category,
            $this->supplier,
            $this->brand,
            $this->ean,
            $this->reference,
            $this->priceLow,
            $this->priceUp,
            $this->stockLow,
            $this->stockUp,
            $this->productEnabled
        );

        $this->brands = Manufacturer::getManufacturers();
        $this->suppliers = Supplier::getSuppliers();
        // $this->categories = Category::getAllCategoriesName();
        $this->categories = $this->getAllCategoriesName();
    }

    public function initContent()
    {
        parent::initContent();

        // Get current selected List
        $dbProductList = $this->getDbProductList();
        $productsList = explode(",", $dbProductList);

        $productDottValueListArr = array();
        foreach ($this->results as &$res) {
            $res['image']    = ProductCore::getCover($res["id_product"]);
            $productDottValueListArr[] = $res["id_product"];

            // create array combinations with key = id_product
            /** @var ProductCore $product */
            $product             = new Product($res["id_product"], $this->context->language->id);
            $res['combinations'] = $product->getAttributesResume($this->context->language->id);
            $res['combinations_imgs'] = $product->getCombinationImages($this->context->language->id);
            $combSelected = 0;
            if ($res['combinations']) {
                foreach ($res['combinations'] as $comb) {
                    $combKey = $res["id_product"]."_".$comb["id_product_attribute"];
                    $productDottValueListArr[] = $combKey;
                    if (in_array($combKey, $productsList)) {
                        $combSelected++;
                    }
                }
            }
            $res['combSelected'] = $combSelected;
        }
        $productDottValueList = implode(",", $productDottValueListArr);

        // Existing files
        $offerFeedFiles = $this->getLastXFiles(_PS_MODULE_DIR_."linodott/logs/feedOferta");
        $catalogFeedFiles  = $this->getLastXFiles(_PS_MODULE_DIR_."linodott/feedCatalog");
        $logFiles  = $this->getLastXFiles(_PS_MODULE_DIR_."linodott/logs/feedProduct");
        $cronOrderslogFiles = $this->getLastXFiles(_PS_MODULE_DIR_."linodott/logs/cronOrders");
        $cronOfertalogFiles = $this->getLastXFiles(_PS_MODULE_DIR_."linodott/logs/cronOferta");

        // Pagination
        $pageMax = $this->totalResults / $this->resultsPerPage;
        $pageMax = ceil($pageMax);

        // Smarty on fetch template
        $this->context->smarty->assign(array(
          'data'        => $this->results,
          'productList' => $productsList,
          'productDottValueList' => $productDottValueList,
          'offerFeedFiles' => $offerFeedFiles,
          'priceVariationDirection' => $this->priceVariationDirection,
          'priceVariationQuantity' => $this->priceVariationQuantity,
          'priceVariationType' => $this->priceVariationType,
          'catalogFeedFiles' => $catalogFeedFiles,
          'logFiles' => $logFiles,
          'cronOrderslogFiles' => $cronOrderslogFiles,
          'cronOfertalogFiles' => $cronOfertalogFiles,
          'page'        => $this->page,
          'resultsPerPage' => $this->resultsPerPage,
          'totalResults' => $this->totalResults,
          'pageMax'     => $pageMax,
          'brands'      => $this->brands,
          'brand'      => $this->brand,
          'suppliers'   => $this->suppliers,
          'supplier'   => $this->supplier,
          'categories'  => $this->categories,
          'category'    => $this->category,
          'ean'         => $this->ean,
          'reference'   => $this->reference,
          'priceLow'    => $this->priceLow,
          'priceUp'     => $this->priceUp,
          'stockLow'    => $this->stockLow,
          'stockUp'     => $this->stockUp,
          'productEnabled'  => $this->productEnabled,
          'orderCol'    => $this->orderCol,
          'orderWay'    => $this->orderWay,
          'returnMsg'   => $this->returnMsg,
          'errorMsg'    => $this->errorMessage,
          'isDebug'     => Configuration::get('LINO_DOTT_DEBUG_MODE')
        ));
        $template = "linodott/views/templates/admin/products/productList.tpl";
        $content  = $this->context->smarty->fetch(_PS_MODULE_DIR_ . $template);

        // Smarty on general layout
        $this->context->smarty->assign(array(
          'content' => $this->content . $content,
        ));
    }

    public function postProcess()
    {
        $this->returnMsg = "";
        $this->errorMessage = "";
        $shopId = (int)$this->context->shop->id;
        if (Tools::isSubmit('productDottList') || Tools::isSubmit('productDottFilter')) {
            $values = Tools::getValue('product_dott');
            if (!Tools::isSubmit('productDottFilter') && !$values) {
                $values = array();
            }
            $valueListStr = Tools::getValue('product_dott_value_list');
            $valueList = explode(",", $valueListStr);
            if (!$this->filterPageChanged && is_array($values)) {
                $dbProductList = $this->getDbProductList();
                $existingProductsList = explode(",", $dbProductList);
                // We update only the current page values
                foreach ($this->results as $res) {
                    if (in_array($res["id_product"], $valueList)) {
                        if (in_array($res["id_product"], $values)
                            && !in_array($res["id_product"], $existingProductsList)) {
                            // If product added, we add it
                            $existingProductsList[] = $res["id_product"];
                        }
                        if (!in_array($res["id_product"], $values)
                            && in_array($res["id_product"], $existingProductsList)) {
                            // If product removed, we remove it
                            if (($key = array_search($res["id_product"], $existingProductsList)) !== false) {
                                unset($existingProductsList[$key]);
                            }
                        }
                    }

                    // Combination
                    $product = new Product($res["id_product"], $this->context->language->id);
                    $res['combinations'] = $product->getAttributesResume($this->context->language->id);
                    if ($res['combinations']) {
                        foreach ($res['combinations'] as $comb) {
                            $combKey = $res["id_product"]."_".$comb["id_product_attribute"];
                            if (in_array($combKey, $valueList)) {
                                if (in_array($combKey, $values) && !in_array($combKey, $existingProductsList)) {
                                    // If product comb. added, we add it
                                    $existingProductsList[] = $combKey;
                                }
                                if (!in_array($combKey, $values) && in_array($combKey, $existingProductsList)) {
                                    // If product removed, we remove it
                                    if (($key = array_search($combKey, $existingProductsList)) !== false) {
                                        unset($existingProductsList[$key]);
                                    }
                                }
                            }
                        }
                    }
                }
                $this->updateDbProductList(implode(",", $existingProductsList));

                // Regenerate Catalog file
                $feedDott = new FeedDott($this->context, $this->apiUrl, $this->bearerToken, $shopId);
                $feedDott->generateCatalogFile();
                $this->returnMsg = $feedDott->getReturnMsg();
                $this->errorMessage = $feedDott->getErrorMessage();
            }
            $this->filterPageChanged = false;
        }

        // Case manually submit feed oferta
        if (Tools::isSubmit('feedOferta')) {
            $feedDott = new FeedDott($this->context, $this->apiUrl, $this->bearerToken, $shopId);
            $feedDott->generateFeed();
            $this->returnMsg = $feedDott->getReturnMsg();
            $this->errorMessage = $feedDott->getErrorMessage();
        }
        if (Tools::isSubmit('feedCatalog')) {
            $feedDott = new FeedDott($this->context, $this->apiUrl, $this->bearerToken, $shopId);
            $feedDott->generateCatalogFile();
            $this->returnMsg = $feedDott->getReturnMsg();
            $this->errorMessage = $feedDott->getErrorMessage();
        }

        if (Tools::isSubmit('productDottSaveFiltered') || Tools::isSubmit('productDottSaveNotFiltered')) {
            $existingProductsList = array();
            $isSelected = true;
            if (Tools::isSubmit('productDottSaveNotFiltered')) {
                $isSelected = false;
            }
            $productList = $this->getAllProducts(
                $this->context->language->id,
                0,
                -1,
                $this->orderCol,
                $this->orderWay,
                $this->category,
                $this->supplier,
                $this->brand,
                $this->ean,
                $this->reference,
                $this->priceLow,
                $this->priceUp,
                $this->stockLow,
                $this->stockUp,
                true
            );
            $dbProductList = $this->getDbProductList();
            $existingProductsList = explode(",", $dbProductList);
            foreach ($productList as $res) {
                // We add or remove id_product
                if ($isSelected) {
                    if (!in_array($res["id_product"], $existingProductsList)) {
                        $existingProductsList[] = $res["id_product"];
                    }
                } else {
                    if (in_array($res["id_product"], $existingProductsList)) {
                        if (($key = array_search($res["id_product"], $existingProductsList)) !== false) {
                            unset($existingProductsList[$key]);
                        }
                    }
                }

                // Combination
                $product = new Product($res["id_product"], $this->context->language->id);
                $res['combinations'] = $product->getAttributesResume($this->context->language->id);
                if (is_array($res['combinations'])) {
                    foreach ($res['combinations'] as $comb) {
                        $combKey = $res["id_product"]."_".$comb["id_product_attribute"];
                        if ($isSelected) {
                            if (!in_array($combKey, $existingProductsList)) {
                                $existingProductsList[] = $combKey;
                            }
                        } else {
                            if (in_array($combKey, $existingProductsList)) {
                                if (($key = array_search($combKey, $existingProductsList)) !== false) {
                                    unset($existingProductsList[$key]);
                                }
                            }
                        }
                    }
                }
            }
            $this->updateDbProductList(implode(",", $existingProductsList));
            $feedDott = new FeedDott($this->context, $this->apiUrl, $this->bearerToken, $shopId);
            $feedDott->generateCatalogFile();
            $this->returnMsg = $this->module->l('Products saved!', 'linodott');
        }
        if (Tools::isSubmit('selectAll')) {
            $existingProductsList = array();
            $totalProducts = $this->getAllProducts(
                $this->context->language->id,
                0,
                -1,
                $this->orderCol,
                $this->orderWay,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                true
            );
            foreach ($totalProducts as $res) {
                // If product added, we add it
                $existingProductsList[] = $res["id_product"];
                // Combination
                $product = new Product($res["id_product"], $this->context->language->id);
                $res['combinations'] = $product->getAttributesResume($this->context->language->id);
                if ($res['combinations']) {
                    foreach ($res['combinations'] as $comb) {
                        $combKey = $res["id_product"]."_".$comb["id_product_attribute"];
                        $existingProductsList[] = $combKey;
                    }
                }
            }
            $this->updateDbProductList(implode(",", $existingProductsList));
            $feedDott = new FeedDott($this->context, $this->apiUrl, $this->bearerToken, $shopId);
            $feedDott->generateCatalogFile();
            $this->returnMsg = $this->module->l('Products saved!', 'linodott');
        }
        if (Tools::isSubmit('unSelectAll')) {
            $this->updateDbProductList("");
            $feedDott = new FeedDott($this->context, $this->apiUrl, $this->bearerToken, $shopId);
            $feedDott->generateCatalogFile();
            $this->returnMsg = $this->module->l('Products saved!', 'linodott');
        }
        if (Tools::isSubmit('uploadFile')) {
            $uploadFile = _PS_MODULE_DIR_.'linodott/upload/'.basename($_FILES['csvupdate']['name']);
            $hasError = false;
            switch ($_FILES['csvupdate']['error']) {
                case UPLOAD_ERR_OK:
                    break;
                case UPLOAD_ERR_NO_FILE:
                    $this->errorMsg = "Ficheiro vazio";
                    $hasError = true;
                    break;
                case UPLOAD_ERR_INI_SIZE: // same
                case UPLOAD_ERR_FORM_SIZE:
                    $this->errorMsg = "Ficheiro grande demais";
                    $hasError = true;
                    break;
                default:
                    $this->errorMessage = "Erro durante o cargamento do ficheiro";
                    $hasError = true;
                    break;
            }
            $ext = pathinfo($_FILES['csvupdate']['name'], PATHINFO_EXTENSION);
            if ($ext !== 'csv') {
                $this->errorMessage = "Ficheiro tem de ser *.csv";
                $hasError = true;
            }
            if ($_FILES['csvupdate']['size'] > 15000000) {
                $this->errorMessage = "Ficheiro grande demais (> 10 Mo)";
                $hasError = true;
            }
            if (!$hasError && !move_uploaded_file($_FILES['csvupdate']['tmp_name'], $uploadFile)) {
                $this->errorMessage = "Erro durante o upload do ficheiro";
                $hasError = true;
            }
            if (!$hasError) {
                // Read file
                $fh = fopen($uploadFile, "r");
                $existingProductsList = array();
                while (!feof($fh)) {
                    $line = fgets($fh);
                    $idProduct = trim(str_replace(";", "", $line));
                    if (!is_numeric($idProduct)) {
                        continue;
                    }
                    $product = new Product($idProduct, $this->context->language->id);
                    if ($product != null) {
                        $existingProductsList[] = $idProduct;
                        $combs = $product->getAttributesResume($this->context->language->id);
                        foreach ($combs as $comb) {
                            $combKey = $idProduct."_".$comb["id_product_attribute"];
                            $existingProductsList[] = $combKey;
                        }
                    }
                }
                fclose($fh);
                unlink($uploadFile);
                $this->updateDbProductList(implode(",", $existingProductsList));
                $feedDott = new FeedDott($this->context, $this->apiUrl, $this->bearerToken, $shopId);
                $feedDott->generateCatalogFile();
                $this->returnMsg = $this->module->l('Products saved!', 'linodott');
            }
        }
    }

    /**
     * Get all available products
     *
     * @param int $id_lang Language id
     * @param int $start Start number
     * @param int $limit Number of products to return
     * @param string $order_by Field for ordering
     * @param string $order_way Way for ordering (ASC or DESC)
     * @return array Products details
     */
    public function getAllProducts(
        $id_lang,
        $start,
        $limit,
        $order_by,
        $order_way,
        $id_category = false,
        $id_supplier = false,
        $id_brand = false,
        $ean = false,
        $reference = false,
        $price_low = false,
        $price_up = false,
        $stock_low = false,
        $stock_up = false,
        $productEnabled = false
    ) {
        $isDebug = Configuration::get('LINO_DOTT_DEBUG_MODE') == 1 ? true : false;

        if ($isDebug) {
            $logHandle = fopen(_PS_ROOT_DIR_ . '/modules/linodott/logs/feedProduct/Feed_Product_'
                .date("Y-m-d-His").'.log', 'a+');
            fputs($logHandle, 'Feed Product - CALLED at ' . date('Y-m-d H:i:s').PHP_EOL);
        }

        if (!Validate::isOrderBy($order_by) || !Validate::isOrderWay($order_way)) {
            die(Tools::displayError());
        }
        if ($order_by == 'id_product'
            || $order_by == 'reference'
            || $order_by == 'ean13'
            || $order_by == 'price'
            || $order_by == 'date_add'
            || $order_by == 'date_upd'
        ) {
            $order_by_prefix = 'p';
        } elseif ($order_by == 'name') {
            $order_by_prefix = 'pl';
        } elseif ($order_by == 'position') {
            $order_by_prefix = 'c';
        }

        if (strpos($order_by, '.') > 0) {
            $order_by = explode('.', $order_by);
            $order_by_prefix = $order_by[0];
            $order_by = $order_by[1];
        }
        $isBrand = false;
        $brandQuery = "";
        if (!empty($id_brand) && sizeof($id_brand) > 0) {
            $isBrand = true;
            $brandQuery = " AND (";
            $sep = "";
            foreach ($id_brand as $sup) {
                $brandQuery .= $sep."m.`id_manufacturer` = ".(int)$sup;
                $sep = " OR ";
            }
            $brandQuery .= " )";
        }

        $isSupplier = false;
        $supplierQuery = "";
        if (!empty($id_supplier) && sizeof($id_supplier) > 0) {
            $isSupplier = true;
            $supplierQuery = " AND (";
            $sep = "";
            foreach ($id_supplier as $sup) {
                $supplierQuery .= $sep."s.`id_supplier` = ".(int)$sup;
                $sep = " OR ";
            }
            $supplierQuery .= " )";
        }

        $isCategories = false;
        $catQuery = "";
        if (!empty($id_category) && sizeof($id_category) > 0) {
            $isCategories = true;
            $catQuery = " AND (";
            $sep = "";
            foreach ($id_category as $sup) {
                $catQuery .= $sep."c.`id_category` = ".(int)$sup;
                $sep = " OR ";
            }
            $catQuery .= " )";
        }

        // Products active or not
        if ($productEnabled == "active") {
            $productEnabled = "1";
        } elseif ($productEnabled == "inactive") {
            $productEnabled = "0";
        } else {
            $productEnabled = false;
        }

        $sql = 'SELECT DISTINCT(p.id_product), p.*, product_shop.*, pl.* '.
            ($isBrand ? ", m.id_manufacturer, m.`name` AS manufacturer_name " : "").
            ($isSupplier ? ", s.id_supplier, s.`name` AS supplier_name " : "").'
				FROM `'._DB_PREFIX_.'product` p
				'.Shop::addSqlAssociation('product', 'p').'
				LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` '.Shop::addSqlRestrictionOnLang('pl').')'.
            ($isBrand ? "LEFT JOIN `"._DB_PREFIX_."manufacturer` m ON (m.`id_manufacturer` = p.`id_manufacturer`)" : "").
            ($isSupplier ? "LEFT JOIN `"._DB_PREFIX_."supplier` s ON (s.`id_supplier` = p.`id_supplier`)" : "").
            ($isCategories ? 'LEFT JOIN `'._DB_PREFIX_.'category_product` c ON (c.`id_product` = p.`id_product`)' : '').
            'WHERE pl.`id_lang` = '.(int)$id_lang.
            ($isCategories ? $catQuery : '').
            ($isSupplier ? $supplierQuery : '').
            ($isBrand ? $brandQuery : '').
            ($ean ? " AND p.`ean13` like '%".$ean."%'" : "").
            ($reference ? " AND p.`reference` like '%".$reference."%'" : "").
            ($price_low ? ' AND product_shop.`price` >= '.(float)$price_low : '').
            ($price_up ? ' AND product_shop.`price` <= '.(float)$price_up : '').
//            ($stock_low ? ' AND p.`quantity` >= '.(float)$stock_low : '').
//            ($stock_up ? ' AND p.`quantity` <= '.(float)$stock_up : '').
            ($productEnabled !== false ? ' AND product_shop.`active` = '.$productEnabled : '').
                ' ORDER BY '.(isset($order_by_prefix) ? pSQL($order_by_prefix).'.' : '').'`'.pSQL($order_by).'` '.pSQL($order_way);
            /* ($limit > 0 ? ' LIMIT '.(int)$start.','.(int)$limit : '') */ ;
        $rq = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
        if ($order_by == 'price') {
            Tools::orderbyPrice($rq, $order_way);
        }

        if ($isDebug) {
            fputs($logHandle, 'Query: ' . $sql.PHP_EOL);
            fputs($logHandle, 'Result: ' . print_r($rq, true).PHP_EOL);
        }

        $filterRes = array();
        foreach ($rq as &$row) {
            $row = Product::getTaxesInformations($row);
            $row['quantity'] = ProductCore::getQuantity($row["id_product"]);
            $isStockLow = $stock_low != "" ? true : false;
            $isStockUp = $stock_up != "" ? true : false;
            if ($isStockLow || $isStockUp) {
                if ($isStockLow && $isStockUp && $row['quantity'] >= $stock_low && $row['quantity'] <= $stock_up) {
                    $filterRes[] = $row;
                } elseif ($isStockLow && !$isStockUp && $row['quantity'] >= $stock_low) {
                    $filterRes[] = $row;
                } elseif (!$isStockLow && $isStockUp && $row['quantity'] <= $stock_up) {
                    $filterRes[] = $row;
                }
            } else {
                // No quantity filter
                $filterRes[] = $row;
            }
        }
        $final = array();
        if ($limit != -1) {
            $cpt = 0;
            foreach ($filterRes as $rowFiltered) {
                if ($cpt >= $start && $cpt < ($start + $limit)) {
                    $final[] = $rowFiltered;
                }
                $cpt++;
            }
        } else {
            $final = $filterRes;
        }

        if ($isDebug) {
            fputs($logHandle, 'Result Final: ' . print_r($rq, true).PHP_EOL);
            fclose($logHandle);
        }

        return ($final);
    }

    private function getLastXFiles($dir)
    {
        $offerFeedDir = $dir;
        $allFiles = array();
        $files = scandir($offerFeedDir);
        $ignored = array('.', '..', '.svn', '.htaccess', 'index.php');
        foreach ($files as $f) {
            if (!in_array($f, $ignored) && !is_dir($f)) {
                $path = $dir . DIRECTORY_SEPARATOR . $f;
                if (filemtime($path) < strtotime('-1 month')) {  // check how long it's been around
                    unlink($path);  // remove it
                } else {
                    $allFiles[$f] = filemtime($offerFeedDir . '/' . $f);
                }
            }
        }
        arsort($allFiles);
        $resultFinal = array_slice($allFiles, 0, 10);
        return $resultFinal;
    }

    public function getAllCategoriesName(
        $root_category = null,
        $id_lang = false,
        $active = true,
        $groups = null,
        $use_shop_restriction = true,
        $sql_filter = '',
        $sql_sort = '',
        $sql_limit = ''
    ) {
        if (isset($root_category) && !Validate::isInt($root_category)) {
            die(Tools::displayError());
        }

        if (!Validate::isBool($active)) {
            die(Tools::displayError());
        }

        if (isset($groups) && Group::isFeatureActive() && !is_array($groups)) {
            $groups = (array)$groups;
        }

        $cache_id = 'Category::getAllCategoriesName_'.md5((int)$root_category.(int)$id_lang.(int)$active.(int)$use_shop_restriction
                .(isset($groups) && Group::isFeatureActive() ? implode('', $groups) : ''));

        if (!Cache::isStored($cache_id)) {
            $result = Db::getInstance()->executeS('
				SELECT c.id_category, cl.name
				FROM `'._DB_PREFIX_.'category` c
				'.($use_shop_restriction ? Shop::addSqlAssociation('category', 'c') : '').'
				LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON c.`id_category` = cl.`id_category`'.Shop::addSqlRestrictionOnLang('cl').'
				'.(isset($groups) && Group::isFeatureActive() ? 'LEFT JOIN `'._DB_PREFIX_.'category_group` cg ON c.`id_category` = cg.`id_category`' : '').'
				'.(isset($root_category) ? 'RIGHT JOIN `'._DB_PREFIX_.'category` c2 ON c2.`id_category` = '.(int)$root_category.' AND c.`nleft` >= c2.`nleft` AND c.`nright` <= c2.`nright`' : '').'
				WHERE 1 '.$sql_filter.' '.($id_lang ? 'AND `id_lang` = '.(int)$id_lang : '').'
				'.($active ? ' AND c.`active` = 1' : '').'
				'.(isset($groups) && Group::isFeatureActive() ? ' AND cg.`id_group` IN ('.implode(',', $groups).')' : '').'
				'.(!$id_lang || (isset($groups) && Group::isFeatureActive()) ? ' GROUP BY c.`id_category`' : '').'
				'.($sql_sort != '' ? $sql_sort : ' ORDER BY c.`level_depth` ASC').'
				'.($sql_sort == '' && $use_shop_restriction ? ', category_shop.`position` ASC' : '').'
				'.($sql_limit != '' ? $sql_limit : ''));
            Cache::store($cache_id, $result);
        } else {
            $result = Cache::retrieve($cache_id);
        }
        return $result;
    }


    private function getDbProductList()
    {
        $res = Db::getInstance()->getRow("Select value from `"._DB_PREFIX_."lino_dott` where name='products_list'");
        if (is_array($res) && sizeof($res) > 0) {
            return $res["value"];
        }
        return "";
    }
    private function updateDbProductList($productList)
    {
        return Db::getInstance()->execute("update `"._DB_PREFIX_."lino_dott` set value='".$productList."' where name='products_list'");
    }
}
