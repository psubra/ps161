<?php
/**
* NOTICE OF LICENSE
*
* This file is licenced under the GNU General Public License, version 3 (GPL-3.0).
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* @author    Li-Nó Design www.lino-design.com <contact@lino-design.com>
* @copyright 2019 Li-Nó Design Lda
* @license   https://opensource.org/licenses/GPL-3.0 GNU General Public License version 3
*/

require_once _PS_MODULE_DIR_.'linodott/classes/CurlRequest.php';

class AdminDottRefundsController extends ModuleAdminController
{
    const ACTION_LIST = "refundList";
    const ACTION_VIEW = "refundview";
    const ITEMS_BY_PAGE = 30;

    private $apiUrl;
    private $bearerToken;

    public function __construct()
    {
        $this->modulename  = 'linodott';
        $this->bootstrap   = true;
        $this->lang        = true;
        $this->apiUrl      = Configuration::get('LINO_DOTT_API_URL');
        $this->bearerToken = Configuration::get('LINO_DOTT_KEY');
        if (Tools::substr("$this->apiUrl", -1) != "/") {
            $this->apiUrl .= "/";
        }
        parent::__construct();
    }

    public function initContent()
    {
        parent::initContent();

        $dottaction     = Tools::getValue('dottaction');
        $returnMsg      = Tools::getValue('returnMsg');
        $errorMsg       = Tools::getValue('errorMsg');
        $page       = Tools::getValue('page');
        if (empty($page)) {
            $page = 1;
        }
        $method         = "GET";
        $message        = "";
        $postParams     = array();
        $redirectParams = array();
        if ($dottaction == self::ACTION_VIEW) {
            $refundId = Tools::getValue('refundId');
            if ($refundId == null) {
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminDottRefunds', true)
                                     . "&action=" . self::ACTION_LIST);
            }
            $path     = "api/refund/" . $refundId;
            $template = "linodott/views/templates/admin/refunds/refundDetail.tpl";
        } else {
            $path     = "api/refund";
            $template = "linodott/views/templates/admin/refunds/refundList.tpl";
        }

        // Only if configuration filled
        $data = array();
        $data["data"] = array();
        $totalRows = 0;
        $nbPages = 0;
        if ($this->apiUrl != "" && $this->bearerToken != "") {
            $url = $this->apiUrl . $path;
            if (sizeof($postParams) > 0) {
                $url = $url."?";
                $sep = "";
                foreach ($postParams as $key => $param) {
                    $url = $url.$sep.$key."=".$param;
                    $sep = "&";
                }
            }
            $curl2 = new CurlRequest($url, $this->bearerToken);
            $response = null;
            try {
                if ($method == "POST") {
                    $curl2->setParams($postParams);
                    $response = $curl2->executePost();
                    if ($response["code"] > 250) {
                        $errorMsg = $this->module->l('An error happened..', 'linodott');
                    }
                } else {
                    $response = $curl2->executeGet();
                }
            } catch (Exception $e) {
                $errorMsg = $e->getMessage();
            }

            if (sizeof($redirectParams) > 0) {
                $redirectParams['errorMsg'] = $errorMsg;
                $redirectParamsStr          = "";
                foreach ($redirectParams as $key => $value) {
                    $redirectParamsStr .= "&" . $key . "=" . $value;
                }
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminDottRefunds', true) . $redirectParamsStr);
            }
            $data = json_decode(json_encode($response['body']), true);
            if (sizeof($data) > 0 && isset($data['totalRow'])) {
                $totalRows = $data['totalRow'];
                if ($totalRows > 0) {
                    $nbPages = ceil($totalRows / $this::ITEMS_BY_PAGE);
                }
            }
        } else {
            $errorMsg = $this->module->l('Please configure the API key and API Url first', 'linodott');
        }

        // Smarty on fetch template
        $this->context->smarty->assign(array(
          'data'        => $data,
          'totalRows' => $totalRows,
          'page'      => $page,
          'nbPages'   => $nbPages,
          'message'     => $message,
          'returnMsg'   => $returnMsg,
          'errorMsg'    => $errorMsg,
          'returnToken' => Tools::getAdminTokenLite('AdminDottReturns')
        ));
        $content = $this->context->smarty->fetch(_PS_MODULE_DIR_ . $template);

        // Smarty on general layout
        $this->context->smarty->assign(array(
          'content' => $this->content . $content,
        ));
    }
}
