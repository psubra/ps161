<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the GNU General Public License, version 3 (GPL-3.0).
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * @author    Li-Nó Design www.lino-design.com <contact@lino-design.com>
 * @copyright 2019 Li-Nó Design Lda
 * @license   https://opensource.org/licenses/GPL-3.0 GNU General Public License version 3
 */

/**
 * Class OrderDott
 * @package Emarketing
 */
class OrderDott
{
    /**
     * @var false|resource|null
     */
    private $apiUrl = null;
    private $bearerToken = null;

    const ORDER_STATE_CONFIRMED = "Confirmed";
    const ORDER_STATE_IMPORTED = "Imported";

    /**
     * CurlRequest constructor.
     *
     * @param $url
     */
    public function __construct($context, $apiUrl, $bearerToken)
    {
        $this->context = $context;
        $this->apiUrl = $apiUrl;
        $this->bearerToken = $bearerToken;
    }

    public static function getAllOrders()
    {
        return Db::getInstance()->executeS('
				SELECT ldo.*
				FROM `'._DB_PREFIX_.'lino_dott_orders` ldo');
    }

    public static function getOrderByIdAndStatus($idDottOrder, $orderStatus)
    {
        return Db::getInstance()->executeS("
				SELECT ldo.*
				FROM `"._DB_PREFIX_."lino_dott_orders` ldo
				WHERE `order_id` = '".$idDottOrder."'
				AND `order_state` = '".$orderStatus."' ");
    }

    public static function createdPSOrder($logHandle, $dottOrder, $context, $dottGroup, $id_carrier, $paymentmodule, $importOrderState)
    {
        if ($dottGroup != null) {
            $hasDottGroup = true;
        }
        $hasError = false;
        $order = new Order();
        // PS Reference
        $order->reference = Order::generateReference();
        // my extended field on Order
        $order->id_shop = $context->shop->id;
        $order->id_shop_group = $context->shop->id_shop_group;

        // Customer
        $customer = new Customer();
        $userName = $dottOrder['customer']['username'];
        if (empty($userName)) {
            $userName = "DottUserName";
        }
        $firstName = Tools::ucwords($dottOrder['customer']['firstName']);
        if (empty($firstName)) {
            $firstName = "DottFirstname";
        }
        $lastName = Tools::ucwords($dottOrder['customer']['lastName']);
        if (empty($lastName)) {
            $lastName = "DottLastname";
        }
        $fakeEmail = $userName."@dott-customer.com";
        $customer->getByEmail($fakeEmail);
        if (!Validate::isLoadedObject($customer)) {
            // Create a new one
            $customer = new Customer();
            $customer->firstname = $firstName;
            $customer->lastname = $lastName;
            $customer->email = $fakeEmail;
            $customer->passwd = Tools::passwdGen(20, 'NO_NUMERIC');
            $customer->is_guest = 0;
            $customer->active = 1;
            if ($hasDottGroup) {
                $customer->id_default_group = $dottGroup['id_group'];
            }
            try {
                $customer->add();
            } catch (Exception $e) {
                fputs($logHandle, 'Order DOTT error while creating customer in Prestashop: '.$dottOrder["id"].'-'.$dottOrder["orderNr"].", error= ".$e->getMessage().PHP_EOL.$e->getTraceAsString().PHP_EOL);
                $hasError = true;
            }
        }

        // Add shipping address
        $existingShippingAddress = OrderDott::getCustomerAddressByName($customer->id, $dottOrder['shippingAddress']['name']);
        $id_address_delivery = 0;
        if (!$hasError && empty($existingShippingAddress)) {
            $shippingAddress = new Address();
            $shippingAddress->alias = substr($dottOrder['shippingAddress']['name'], 0, 31);
            $shippingAddress->lastname = Tools::ucwords($lastName);
            $shippingAddress->firstname =  Tools::ucwords($firstName);
            $shippingAddress->address1 = $dottOrder['shippingAddress']['address'];
            $shippingAddress->address2 = $dottOrder['shippingAddress']['address2'];
            $shippingAddress->postcode = $dottOrder['shippingAddress']['zipcode'];
            $countryId = Country::getIdByName($context->language->id, $dottOrder['shippingAddress']['country']);
            if ($countryId) {
                $shippingAddress->id_country = $countryId;
            }
            $shippingAddress->city = $dottOrder['shippingAddress']['city'];
            $shippingAddress->id_customer = $customer->id;
            try {
                $shippingAddress->add();
            } catch (Exception $e) {
                fputs($logHandle, 'Order DOTT error while creating shipping address in Prestashop: '.$dottOrder["id"].'-'.$dottOrder["orderNr"].", error= ".$e->getMessage().PHP_EOL.$e->getTraceAsString().PHP_EOL);
                $hasError = true;
            }
            $id_address_delivery = $shippingAddress->id;
        } else {
            $id_address_delivery = $existingShippingAddress;
        }

        // Add billing address
        $existingBillingAddress = OrderDott::getCustomerAddressByName($customer->id, $dottOrder['billingAddress']['name']);
        $id_address_billing = 0;
        if (!$hasError && empty($existingBillingAddress)) {
            $billingAddress = new Address();
            $billingAddress->alias = substr($dottOrder['billingAddress']['name'], 0, 31);
            $billingAddress->lastname = Tools::ucwords($lastName);
            $billingAddress->firstname =  Tools::ucwords($firstName);
            $billingAddress->address1 = $dottOrder['billingAddress']['address'];
            $billingAddress->address2 = $dottOrder['billingAddress']['address2'];
            $billingAddress->postcode = $dottOrder['billingAddress']['zipcode'];
            $countryId = Country::getIdByName($context->language->id, $dottOrder['billingAddress']['country']);
            if ($countryId) {
                $billingAddress->id_country = $countryId;
            }
            $billingAddress->city = $dottOrder['billingAddress']['city'];
            $billingAddress->id_customer = $customer->id;
            try {
                $billingAddress->add();
            } catch (Exception $e) {
                fputs($logHandle, 'Order DOTT error while creating billing address in Prestashop: '.$dottOrder["id"].'-'.$dottOrder["orderNr"].", error= ".$e->getMessage().PHP_EOL.$e->getTraceAsString().PHP_EOL);
                $hasError = true;
            }
            $id_address_billing = $billingAddress->id;
        } else {
            $id_address_billing = $existingBillingAddress;
        }

        if (!$hasError) {
            // Create Cart
            $cart = new Cart();
            $cart->id_shop_group = $context->shop->id_shop_group;
            $cart->id_shop = $context->shop->id;
            $cart->id_customer = $customer->id;
            $cart->id_carrier = $id_carrier;
            $cart->id_address_delivery = $id_address_delivery;
            $cart->id_address_invoice = $id_address_billing;
            $cart->id_currency = $context->currency->id;
            $cart->id_lang = $context->language->id;
            try {
                $cart->add();
            } catch (Exception $e) {
                fputs($logHandle, 'Order DOTT error while creating cart in Prestashop: '.$dottOrder["id"].'-'.$dottOrder["orderNr"].", error= ".$e->getMessage().PHP_EOL.$e->getTraceAsString().PHP_EOL);
                $hasError = true;
            }
        }

        if (!$hasError) {
            // Add products to cart
            $itemProductByEan = array();
            foreach ($dottOrder['orderItems'] as $item) {
                $existingProductId = Product::getIdByEan13($item['gtin']);
                $existingProductAttrId = null;
                if (empty($existingProductId)) {
                    $existingProduct = OrderDott::getIdProductByCombinationEan($item['gtin']);
                    if (sizeof($existingProduct) > 0) {
                        $existingProductId = $existingProduct["existingProductId"];
                        $existingProductAttrId = $existingProduct["existingProductAttrId"];   
                    }
                }
                if (!empty($existingProductId)) {
                    $itemProductByEan[$item['gtin']] = $existingProductId;
                    $existingProduct = new Product($existingProductId);
                    $cart->updateQty($item["quantity"], $existingProduct->id, $existingProductAttrId);
                }
            }
        }

        if (!$hasError) {
            $order->id_address_delivery = $id_address_delivery;
            $order->id_address_invoice = $id_address_billing;
            $order->id_cart = $cart->id;
            $order->id_currency = $context->currency->id;
            $order->id_lang = $context->language->id;
            $order->id_customer = $customer->id;
            $order->id_carrier = $id_carrier;
            // $order->payment = 'DOTT - '.$dottOrder['merchantOrderPayment']['paymentMethod'];
            $order->payment = 'DOTT';
            if(!empty($paymentmodule) && !empty($paymentmodule['name'])) {
                $order->module = $paymentmodule['name'];   
            } else {
                $order->module = "DOTT";
            }
            $order->total_paid = round($dottOrder['merchantOrderPayment']['itemsTotal'] + $dottOrder['merchantOrderPayment']['marketplaceDiscount'], 2);
            $order->total_paid_tax_incl = round($dottOrder['merchantOrderPayment']['itemsTotal'] + $dottOrder['merchantOrderPayment']['marketplaceDiscount'], 2);
            $order->total_paid_real = round($dottOrder['merchantOrderPayment']['itemsTotal'] + $dottOrder['merchantOrderPayment']['marketplaceDiscount'], 2);
            $order->total_products_wt  = round($dottOrder['merchantOrderPayment']['itemsTotal'] + $dottOrder['merchantOrderPayment']['marketplaceDiscount'], 2);

            $order->total_paid_tax_excl = round($dottOrder['merchantOrderPayment']['itemsTotalTaxExcl'] + $dottOrder['merchantOrderPayment']['marketplaceDiscountTaxExcl'], 2);
            $order->total_products  = round($dottOrder['merchantOrderPayment']['itemsTotalTaxExcl'] + $dottOrder['merchantOrderPayment']['marketplaceDiscountTaxExcl'], 2);

            $order->total_shipping  = $dottOrder['merchantOrderPayment']['shipping'];
            $order->total_shipping_tax_excl  = $dottOrder['merchantOrderPayment']['shippingTaxExcl'];

            $order->total_discounts_tax_incl  = $dottOrder['merchantOrderPayment']['marketplaceDiscount'];
            $order->total_discounts_tax_excl  = $dottOrder['merchantOrderPayment']['marketplaceDiscountTaxExcl'];
            $order->product_list = $cart->getProducts();
            $order->conversion_rate = $context->currency->conversion_rate;
            $order->secure_key = $customer->secure_key;
            try {
                $order->add();
            } catch (Exception $e) {
                fputs($logHandle, 'Order DOTT error while creating order in Prestashop: '.$dottOrder["id"].'-'.$dottOrder["orderNr"].", error= ".$e->getMessage().PHP_EOL.$e->getTraceAsString().PHP_EOL);
                $hasError = true;
            }
        }


        // Order Detail and history
        if (!$hasError) {
            try {
                // Order details
                $orderDetail = new OrderDetail(null, null, Context::getContext());
                $products = $cart->getProducts();
                $orderDetail->createList($order, $cart, $importOrderState, $products);

                $orderDetailList = OrderDetail::getList($order->id);
                foreach ($orderDetailList as $orderDetail) {
                    $productId = -1;
                    $unit_price_tax_incl = 0;
                    $unit_price_tax_excl = 0;
                    $total_price_tax_incl = 0;
                    $total_price_tax_excl = 0;
                    foreach ($dottOrder['orderItems'] as $item) {
                        if (array_key_exists($item["gtin"], $itemProductByEan)) {
                            $productId = $itemProductByEan[$item["gtin"]];
                            $marketplaceDiscountTaxExcl = $item["marketplaceDiscount"] * (1 - 0.23);
                            $unit_price_tax_incl = round($item["amount"] + $item["marketplaceDiscount"], 2);
                            $unit_price_tax_excl = round($item["amountTaxTaxExcl"] + $marketplaceDiscountTaxExcl, 2);
                            $total_price_tax_incl = round($unit_price_tax_incl * $item["quantity"], 2);
                            $total_price_tax_excl = round($unit_price_tax_excl * $item["quantity"], 2);
                        }
                    }
                    if ($productId != -1) {
                        $sql = 'UPDATE `'._DB_PREFIX_.'order_detail`
                        SET `unit_price_tax_incl` = '.(float)$unit_price_tax_incl.',
                        `unit_price_tax_excl` = '.(float)$unit_price_tax_excl.',
                        `total_price_tax_incl` = '.(float)$total_price_tax_incl.',
                        `total_price_tax_excl` = '.(float)$total_price_tax_excl.'
                        WHERE `id_order_detail`= '.(int)$orderDetail['id_order_detail'].'
                        LIMIT 1';
                        Db::getInstance()->execute($sql);
                    }
                }
            } catch (Exception $e) {
                fputs($logHandle, 'Order DOTT error while creating order detail: '.$dottOrder["id"].'-'.$dottOrder["orderNr"].", error= ".$e->getMessage().PHP_EOL.$e->getTraceAsString().PHP_EOL);
                $hasError = true;
            }
        }

        if (!$hasError) {
            try {
                $history = new OrderHistory();
                $history->id_order = (int)$order->id;
                $history->id_employee = 0;
                $history->changeIdOrderState((int)$importOrderState, $order->id);
            } catch (Exception $e) {
                fputs($logHandle, 'Order DOTT error while creating order history: '.$dottOrder["id"].'-'.$dottOrder["orderNr"].", error= ".$e->getMessage().PHP_EOL.$e->getTraceAsString().PHP_EOL);
                $hasError = true;
            }
        }
        return $order->id;
    }

    public static function getCustomerAddressByName($id_customer, $alias)
    {
        $query = new DbQuery();
        $query->select('id_address');
        $query->from('address');
        $query->where('alias = \''.pSQL($alias).'\'');
        $query->where('id_customer = '.(int)$id_customer);
        $query->where('deleted = 0');

        return Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($query);
    }

    public static function getIdProductByCombinationEan($ean)
    {
        $existingProduct = array();
        $res = Db::getInstance()->executeS('
			SELECT pa.id_product, pa.id_product_attribute 
			FROM '._DB_PREFIX_.'product_attribute pa
			WHERE pa.ean13 LIKE \'%'.$ean.'\' 
		');
        if(!empty($res) && sizeof($res) > 0) {
            $existingProduct["existingProductId"] = $res[0]["id_product"];
            $existingProduct["existingProductAttrId"] = $res[0]["id_product_attribute"];
        }
        return $existingProduct;
    }
}
