<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the GNU General Public License, version 3 (GPL-3.0).
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * @author    Li-Nó Design www.lino-design.com <contact@lino-design.com>
 * @copyright 2019 Li-Nó Design Lda
 * @license   https://opensource.org/licenses/GPL-3.0 GNU General Public License version 3
 */

require_once _PS_MODULE_DIR_.'linodott/classes/CurlRequest.php';
/**
 * Class CurlRequest
 * @package Emarketing
 */
class FeedDott
{
    /**
     * @var false|resource|null
     */
    private $context = null;
    private $apiUrl = null;
    private $bearerToken = null;
    private $shopId = null;
    private $returnMsg;
    private $errorMessage;

    /**
     * CurlRequest constructor.
     *
     * @param $url
     */
    public function __construct($context, $apiUrl, $bearerToken, $shopId)
    {
        $this->context = $context;
        $this->apiUrl = $apiUrl;
        $this->bearerToken = $bearerToken;
        $this->shopId = $shopId;
    }

    public function generateFeed()
    {
        $separator = ";";
        $this->returnMsg = "Feed Oferta file generated with success";
        $this->errorMessage = "";
        $filename = "oferta.csv";
        $logHandle = fopen(_PS_ROOT_DIR_ . '/modules/linodott/logs/feedOferta/Feed_Oferta_'
            .date("Y-m-d-His").'.log', 'a+');
        fputs($logHandle, 'Feed Oferta - CALLED at ' . date('Y-m-d H:i:s').PHP_EOL);
        $order_xml_file = _PS_ROOT_DIR_."/modules/linodott/feedOferta/".$filename;
        $order_file_handle = fopen($order_xml_file, 'w');
        $out = "";
        $lastUpdate = date("Y-m-d H:i:s");
        $dbProductList = $this->getDbProductList();
        $productsList = explode(",", $dbProductList);
        $deleveryTime = Configuration::get('LINO_DOTT_DELEVERY_TIME');
        $separateShipping = Configuration::get('LINO_DOTT_SEPARATE_SHIPPING');
        $priceVariationDir = Configuration::get('LINO_DOTT_PRODUCTS_PRICE_VARIATION_DIR');
        $priceVariationQuantity = Configuration::get('LINO_DOTT_PRODUCTS_PRICE_VARIATION_QT');
        $priceVariationType = Configuration::get('LINO_DOTT_PRODUCTS_PRICE_VARIATION_TYPE');
        $shipping = Configuration::get('LINO_DOTT_LOGISTIC_SYSTEM');
        if ($shipping !== "1") {
            $shipping = "true";
        } else {
            $shipping = "false";
        }
        $separateShippingStr = 'false';
        if ($separateShipping == 1) {
            $separateShippingStr = 'true';
        }
        // Specific Dott Group price
        // $shopId = (int)$this->context->shop->id;
        $hasDottGroup = false;
        $dottGroup = GroupCore::searchByName('DOTT');
        if ($dottGroup != null) {
            $hasDottGroup = true;
        }

        // DOTT Header file
        $out .= "\"EAN13\"".$separator;
        // $out .= "UPC".$separator;
        $out .= "\"SKU\"".$separator;
        // $out .= "name".$separator;
        $out .= "\"Stock\"".$separator;
        $out .= "\"Price\"".$separator;
        $out .= "\"Old Price\"".$separator;
        $out .= "\"Inicio de promocao\"".$separator;
        $out .= "\"Fim de promocao\"".$separator;
        $out .= "\"Shipping\"".$separator;
        $out .= "\"Tax\"".$separator;
        $out .= "\"Tax rate\"".$separator;
        $out .= "\"Tempo de preparacao\"".$separator;
        $out .= "\"condition\"".$separator;
        $out .= "\"int_id00_offer_separate_shipping\"".$separator;
        $out .= "\"Last Update\"";
        $out .= PHP_EOL;

        fputs($logHandle, 'Feed Oferta - ' . sizeof($productsList)." products selected".PHP_EOL);

        try {
            foreach ($productsList as $val) {
                if ($val == "") {
                    fputs($logHandle, 'Feed Oferta - continue'.PHP_EOL);
                    continue;
                }
                fputs($logHandle, 'Feed Oferta - product ' . $val.PHP_EOL);
                $idProduct = $val;
                $hasCompo = false;
                $idCompo = -1;
                if (strpos($val, "_") > 0) {
                    // Composition case
                    $valAr = explode("_", $val);
                    $idProduct = $valAr[0];
                    $idCompo = $valAr[1];
                    $hasCompo = true;
                }
                /** @var ProductCore $product */
                $product = new Product($idProduct, $this->context->language->id);
                
                fputs($logHandle, 'Feed Oferta - Producto ' .$idProduct.PHP_EOL);
                if ($product != null && $product->reference != "" /*&& $product->ean13 != "" */) {
                    fputs($logHandle, 'Feed Oferta - Producto Selected ' .($product != null?$product->reference:"null prod").PHP_EOL);
                    $compos = $product->getAttributesResume($this->context->language->id);
                    $product->ean13;
                    if (!$hasCompo && !$compos) {
                        fputs($logHandle, 'Feed Oferta - Producto without compo ' .$idProduct.PHP_EOL);
                        $out .= '"'.$product->ean13.'"'.$separator;
                        // $out .= $product->upc.$separator;
                        $out .= '"'.$product->reference.'"'.$separator;
                        // $out .= utf8_encode($product->name[$this->context->language->id]).$separator;
                        if (!$product->active) {
                            $out .= '"0"'.$separator;
                        } else {
                            $out .= '"'.$product->quantity.'"'.$separator;   
                        }
                        $specificPrice = null;
                        $from = "0000-00-00";
                        $to = "0000-00-00";
                        if ($hasDottGroup) {
                            $specificPrice = SpecificPriceCore::getSpecificPrice(
                                $idProduct,
                                $this->shopId,
                                0,
                                0,
                                $dottGroup['id_group'],
                                1
                            );
                        }
                        // Case Dott Group
                        if (!empty($specificPrice) && $specificPrice['id_group'] == $dottGroup['id_group']) {
                            $initialPriceHt = round($product->getPrice(false), 2);
                            if ($specificPrice["reduction_type"] == "amount" && $specificPrice["price"] == -1) {
                                $finalPriceHt = $initialPriceHt - $specificPrice["reduction"];
                            } elseif ($specificPrice["reduction_type"] == "percentage" && $specificPrice["price"] == -1) {
                                $finalPriceHt = $initialPriceHt * (1 - $product->specificPrice["reduction"]);
                            } else {
                                $finalPriceHt = $specificPrice["price"];
                            }
                            $from = $specificPrice['from'];
                            $to = $specificPrice['to'];
                            $finalPriceTtc = round($finalPriceHt * (1+ $product->tax_rate/100), 2);
                        } else {
                            if (is_array($product->specificPrice) && sizeof($product->specificPrice) > 0) {
                                $from = $product->specificPrice['from'];
                                $to = $product->specificPrice['to'];
                            }
                            $finalPriceTtc = round($product->getPrice(true), 2);
                        }
                        $originalPriceTtc = round($product->getPriceWithoutReduct(false), 2);

                        if ($priceVariationQuantity > 0) {
                            if ($priceVariationType == "perc") {
                                if ($priceVariationDir == "more") {
                                    $finalPriceTtc = round($finalPriceTtc * (1 + $priceVariationQuantity / 100), 2);
                                    $originalPriceTtc = round($originalPriceTtc * (1 + $priceVariationQuantity / 100), 2);
                                } elseif ($priceVariationDir == "less") {
                                    $finalPriceTtc = round($finalPriceTtc * (1 - $priceVariationQuantity / 100), 2);
                                    $originalPriceTtc = round($originalPriceTtc * (1 - $priceVariationQuantity / 100), 2);
                                }
                            } else {
                                if ($priceVariationDir == "more") {
                                    $finalPriceTtc = round($finalPriceTtc + $priceVariationQuantity, 2);
                                    $originalPriceTtc = round($originalPriceTtc + $priceVariationQuantity, 2);
                                } elseif ($priceVariationDir == "less") {
                                    $finalPriceTtc = round($finalPriceTtc - $priceVariationQuantity, 2);
                                    $originalPriceTtc = round($originalPriceTtc - $priceVariationQuantity, 2);
                                }
                            }
                        }
                        $out .= '"'.$finalPriceTtc.'"'.$separator; // Price
                        $out .= '"'.$originalPriceTtc.'"'.$separator; // Old price
                        if (!empty($from) && $from != "0000-00-00" && $from != "0000-00-00 00:00:00") {
                            $fromDott = date(DATE_ISO8601, strtotime($from));
                        } else {
                            $fromDott = "";
                        }
                        if (!empty($to) && $to != "0000-00-00" && $to != "0000-00-00 00:00:00") {
                            $toDott = date(DATE_ISO8601, strtotime($to));
                        } else {
                            $toDott = "";
                        }
                        $out .= '"'.$fromDott.'"'.$separator; // From
                        $out .= '"'.$toDott.'"'.$separator; // To
                        $out .= '"'.$shipping.'"'.$separator; // shipping
                        if ($product->tax_rate > 0) {
                            $out .= '"true"'.$separator;
                        } else {
                            $out .= '"false"'.$separator;
                        }
                        $out .= '"'.$product->tax_rate.'"'.$separator;
                        // Tempo de preparação
                        if (!empty($deleveryTime)) {
                            $out .= '"'.$deleveryTime.'"'.$separator;
                        } else {
                            $out .= '"'.'"'.$separator;
                        }
                        $out .= '"'.$product->condition .'"'. $separator;
                        $out .= '"'.$separateShippingStr .'"'. $separator;
                        $out .= '"'.$lastUpdate.'"';
                        $out .= PHP_EOL;
                    } elseif ($idCompo != -1) {
                        // Compo
                        fputs($logHandle, 'Feed Oferta - Producto With compo' .$idProduct.PHP_EOL);
                        foreach ($compos as $comp) {
                            if ($comp["id_product_attribute"] == $idCompo) {
                                if ($comp["ean13"] != "") {
                                    $out .= '"'.$comp["ean13"].'"'.$separator;
                                } else {
                                    $out .= '"'.$product->ean13.'"'.$separator;
                                }
//                        if ($comp["upc"] != "") {
//                            $out .= $comp["upc"].$separator;
//                        } else {
//                            $out .= $product->upc.$separator;
//                        }
                                if ($comp["reference"] != "") {
                                    $out .= '"'.$comp["reference"].'"'.$separator;
                                } else {
                                    $out .= '"'.$product->reference.'"'.$separator;
                                }
                                //                        $out .= utf8_encode($product->name[$this->context->language->id]).$separator;
                                if (!$product->active) {
                                    $out .= '"0"'.$separator;
                                } else {
                                    $out .= '"'.$comp["quantity"].'"'.$separator;
                                }
                                $specificPrice = null;
                                $from = "0000-00-00";
                                $to = "0000-00-00";
                                if ($hasDottGroup) {
                                    $specificPrice = SpecificPriceCore::getSpecificPrice(
                                        $idProduct,
                                        $this->shopId,
                                        0,
                                        0,
                                        $dottGroup['id_group'],
                                        1
                                    );
                                }
                                // Case Dott Group
                                if (!empty($specificPrice) && $specificPrice['id_group'] == $dottGroup['id_group']) {
                                    $initialPriceHt = round($product->getPrice(false, $comp["id_product_attribute"]), 2);
                                    if ($specificPrice["reduction_type"] == "amount" && $specificPrice["price"] == -1) {
                                        $finalPriceHt = $initialPriceHt - $specificPrice["reduction"];
                                    } elseif ($specificPrice["reduction_type"] == "percentage" && $specificPrice["price"] == -1) {
                                        $finalPriceHt = $initialPriceHt * (1 - $product->specificPrice["reduction"]);
                                    } else {
                                        $finalPriceHt = $specificPrice["price"];
                                    }
                                    $from = $specificPrice['from'];
                                    $to = $specificPrice['to'];
                                    $finalPriceTtc = round($finalPriceHt * (1+ $product->tax_rate/100), 2);
                                } else {
                                    if (is_array($product->specificPrice) && sizeof($product->specificPrice) > 0) {
                                        $from = $product->specificPrice['from'];
                                        $to = $product->specificPrice['to'];
                                    }
                                    $finalPriceTtc = round($product->getPrice(true, $comp["id_product_attribute"]), 2);
                                }
                                $originalPriceTtc = round($product->getPriceWithoutReduct(false, $comp["id_product_attribute"]), 2);

                                if ($priceVariationQuantity > 0) {
                                    if ($priceVariationType == "perc") {
                                        if ($priceVariationDir == "more") {
                                            $finalPriceTtc = round($finalPriceTtc * (1 + $priceVariationQuantity / 100), 2);
                                            $originalPriceTtc = round($originalPriceTtc * (1 + $priceVariationQuantity / 100), 2);
                                        } elseif ($priceVariationDir == "less") {
                                            $finalPriceTtc = round($finalPriceTtc * (1 - $priceVariationQuantity / 100), 2);
                                            $originalPriceTtc = round($originalPriceTtc * (1 - $priceVariationQuantity / 100), 2);
                                        }
                                    } else {
                                        if ($priceVariationDir == "more") {
                                            $finalPriceTtc = round($finalPriceTtc + $priceVariationQuantity, 2);
                                            $originalPriceTtc = round($originalPriceTtc + $priceVariationQuantity, 2);
                                        } elseif ($priceVariationDir == "less") {
                                            $finalPriceTtc = round($finalPriceTtc - $priceVariationQuantity, 2);
                                            $originalPriceTtc = round($originalPriceTtc - $priceVariationQuantity, 2);
                                        }
                                    }
                                }
                                $out .= '"'.$finalPriceTtc.'"'.$separator; // Price
                                $out .= '"'.$originalPriceTtc.'"'.$separator; // Old price
                                if (!empty($from) && $from != "0000-00-00" && $from != "0000-00-00 00:00:00") {
                                    $fromDott = date(DATE_ISO8601, strtotime($from));
                                } else {
                                    $fromDott = "";
                                }
                                if (!empty($to) && $to != "0000-00-00" && $to != "0000-00-00 00:00:00") {
                                    $toDott = date(DATE_ISO8601, strtotime($to));
                                } else {
                                    $toDott = "";
                                }
                                $out .= '"'.$fromDott.'"'.$separator; // From
                                $out .= '"'.$toDott.'"'.$separator; // To
                                $out .= '"'.$shipping.'"'.$separator; // shipping

                                if ($product->tax_rate > 0) {
                                    $out .= '"'."true".'"'.$separator;
                                } else {
                                    $out .= '"'."false".'"'.$separator;
                                }
                                $out .= '"'.$product->tax_rate.'"'.$separator;
                                // Tempo de preparação
                                if (!empty($deleveryTime)) {
                                    $out .= '"'.$deleveryTime.'"'.$separator;
                                } else {
                                    $out .= '"'.'"'.$separator;
                                }
                                $out .= '"'.$product->condition.'"'.$separator;
                                $out .= '"'.$separateShippingStr.'"'.$separator;
                                $out .= '"'.$lastUpdate.'"';
                                $out .= PHP_EOL;
                                break;
                            }
                        }
                    }
                }
            }
            fwrite($order_file_handle, $out);
            fclose($order_file_handle);
        } catch(Exception $e) {
            fputs($logHandle, 'Feed Oferta - Error : ' . $e->getTraceAsString().PHP_EOL);
        }

//        $order_file_handle = fopen($order_xml_file, "r");
//
//        // Send it to DOTT API
//        $path     = "api/feed?filename=";
//        // $path     .= "staging/21950/05bfd1b3-8055-41c2-baa7-1ca2f362b83c.csv";
//        $mode = Configuration::get("LINO_DOTT_MODE");
//        if ($mode == 0) {
//            $path     .= "staging/";
//        } else {
//            $path     .= "production/";
//        }
//        $path     .= Configuration::get("LINO_DOTT_MERCHANT_ID")."/".Configuration::get("LINO_DOTT_FEED_FILENAME");
//        $path .= "&merchant=".Configuration::get("LINO_DOTT_MERCHANT_ID");
//        fputs($logHandle, PHP_EOL .'Call: '.$this->apiUrl .$path.PHP_EOL);
//        $curl2 = new CurlRequest($this->apiUrl . $path, $this->bearerToken);
//
//        $isDebug = Configuration::get('LINO_DOTT_DEBUG_MODE') == 1 ? true : false;
//        $response = null;
//        try {
//            $curl2->setParams($out);
//            $response = $curl2->executePut($order_file_handle, $order_xml_file);
//            $responseInfo = print_r($response, true);
//            fputs($logHandle, PHP_EOL . "Response: " . $responseInfo . PHP_EOL);
//            if ($response['code'] >= 500) {
//                $this->errorMessage = 'Error while sending feed file: ' . $response['code'];
//                fputs($logHandle, PHP_EOL . $this->errorMessage . PHP_EOL . $out . PHP_EOL);
//            } elseif ($response['code'] == 415) {
//                $this->errorMessage = 'Error while sending feed file: ' . $response['body']->name
//                    . '; Code=' . $response['body']->code . '; ' . $response['body']->detail;
//                fputs($logHandle, PHP_EOL . $this->errorMessage . PHP_EOL . $out . PHP_EOL);
//            } else {
//                $this->returnMsg = 'Feed sent with success.';
//            }
//            if ($isDebug) {
//                $this->returnMsg .= "Code: ".$response['code'];
//            }
//        } catch (Exception $e) {
//            $this->errorMessage = $e->getMessage();
//            fputs($logHandle, PHP_EOL .'Error: '.$e->getTraceAsString().PHP_EOL);
//        } finally {
//            fputs($logHandle, PHP_EOL .'Feed oferta sent!'.PHP_EOL.$out.PHP_EOL);
//            fclose($logHandle);
//            fclose($order_file_handle);
//
//        }
//        if ($this->errorMessage != "") {
//            return false;
//        }
        return true;
    }


    public function generateCatalogFile()
    {
        $separator = ";";
        $this->returnMsg = "Catalog file generated with success";
        $this->errorMessage = "";
        $filename = "catalog.csv";
        $catalogHandle = null;
        try {
            $catalogHandle = fopen(_PS_ROOT_DIR_ . '/modules/linodott/feedCatalog/'.$filename, 'w+');
            // $logHandle = fopen(_PS_ROOT_DIR_ . '/modules/linodott/log/feedCatalog/log_'.$filename, 'a+');
            // fputs($logHandle, 'Feed Catalog - CALLED at ' . date('Y-m-d H:i:s').PHP_EOL);
            $out = "";
            $lastUpdate = date("Y-m-d H:i:s");
            $dbProductList = $this->getDbProductList();
            $productsList = explode(",", $dbProductList);
            $deleveryTime = Configuration::get('LINO_DOTT_DELEVERY_TIME');
            $separateShipping = Configuration::get('LINO_DOTT_SEPARATE_SHIPPING');
            $priceVariationDir = Configuration::get('LINO_DOTT_PRODUCTS_PRICE_VARIATION_DIR');
            $priceVariationQuantity = Configuration::get('LINO_DOTT_PRODUCTS_PRICE_VARIATION_QT');
            $priceVariationType = Configuration::get('LINO_DOTT_PRODUCTS_PRICE_VARIATION_TYPE');
            $imageSize = Configuration::get('LINO_DOTT_IMG_SIZE');
            
            $exportLangId = Configuration::get('LINO_DOTT_LANG');
            if ($exportLangId == null || $exportLangId === "") {
                $exportLangId = $this->context->language->id;
            }
            $separateShippingStr = 'false';
            if ($separateShipping == 1) {
                $separateShippingStr = 'true';
            }

            // Specific Dott Group price
            $hasDottGroup = false;
            $dottGroup = GroupCore::searchByName('DOTT');
            if ($dottGroup != null) {
                $hasDottGroup = true;
            }

            // DOTT Header file
            $out .= "\"EAN13\"".$separator;
            // $out .= "UPC".$separator;
            $out .= "\"SKU/REF\"".$separator;
            $out .= "\"name\"".$separator;
            $out .= "\"description\"".$separator;
            $out .= "\"description_short\"".$separator;
            $out .= "\"hierarchy\"".$separator;
            $out .= "\"child_designation\"".$separator;
            $out .= "\"marca\"".$separator;
            $out .= "\"keywords\"".$separator;
            $out .= "\"width\"".$separator;
            $out .= "\"height\"".$separator;
            $out .= "\"depth\"".$separator;
            $out .= "\"weight\"".$separator;
            $out .= "\"price\"".$separator;
            $out .= "\"tax_rate\"".$separator;
            $out .= "\"stock\"".$separator;
            $out .= "\"categories\"".$separator;
            $out .= "\"images\"".$separator;
            $out .= "\"delivery_time\"".$separator;
            $out .= "\"condition\"".$separator;
            $out .= "\"int_id00_offer_separate_shipping\"".$separator;
            $out .= "\"last_update\"";
            $out .= PHP_EOL;
            $cpt = 0;
            foreach ($productsList as $val) {
                if ($val == "") {
                    continue;
                }
                $idProduct = $val;
                $hasCompo = false;
                if (strpos($val, "_") > 0) {
                    // Composition case
                    $valAr = explode("_", $val);
                    $idProduct = $valAr[0];
                    $idCompo = $valAr[1];
                    $hasCompo = true;
                }
                /** @var ProductCore $product */
                $product = new Product($idProduct, $exportLangId);

                if ($product != null && $product->reference != "" /* && $product->ean13 != "" */) {
                    // Parent product
                    if (!$hasCompo) {
                        $out .= '"'.$product->ean13 .'"'. $separator;
                        // $out .= $product->upc . $separator;
                        $out .= '"'.$product->reference .'"'. $separator;
                        $out .= '"'.$product->name[$exportLangId] .'"'. $separator;
                        $out .= '"'.preg_replace(
                            "/\r|\n/",
                            "",
                            strip_tags($product->description[$exportLangId])
                        )
                            .'"'
                            . $separator;
                        $out .= '"'.preg_replace(
                            "/\r|\n/",
                            "",
                            strip_tags($product->description_short[$exportLangId])
                        )
                            .'"'
                            . $separator;
                        $out .= '"'."parent" .'"'. $separator;
                        $out .= '"'.'"'.$separator; // Child designation
                        $out .= '"'.$product->manufacturer_name.'"'.$separator;
                        // $out .= '"'.$product->brand_name.'"'.$separator;
                        $metaKey = "";
                        $sep = "";
                        if ($product->tags && in_array($exportLangId, $product->tags) && $product->tags[$exportLangId] != null && is_array($product->tags[$exportLangId])) {
                            foreach ($product->tags[$exportLangId] as $key) {
                                if ($key != "") {
                                    $metaKey .= $sep.$key;
                                    $sep = ",";
                                }
                            }
                        }
                        $out .= '"'.$metaKey.'"'.$separator;
                        $out .= '"'.$product->width.'"'.$separator;
                        $out .= '"'.$product->height.'"'.$separator;
                        $out .= '"'.$product->depth.'"'.$separator;
                        $out .= '"'.$product->weight.'"'.$separator;

                        $specificPrice = null;
                        if ($hasDottGroup) {
                            $specificPrice = SpecificPriceCore::getSpecificPrice(
                                $idProduct,
                                $this->shopId,
                                0,
                                0,
                                $dottGroup['id_group'],
                                1
                            );
                        }
                        // Case Dott Group
                        if (!empty($specificPrice) && $specificPrice['id_group'] == $dottGroup['id_group']) {
                            $initialPriceHt = round($product->getPrice(false), 2);
                            if ($specificPrice["reduction_type"] == "amount" && $specificPrice["price"] == -1) {
                                $finalPriceHt = $initialPriceHt - $specificPrice["reduction"];
                            } elseif ($specificPrice["reduction_type"] == "percentage" && $specificPrice["price"] == -1) {
                                $finalPriceHt = $initialPriceHt * (1 - $product->specificPrice["reduction"]);
                            } else {
                                $finalPriceHt = $specificPrice["price"];
                            }
                            $finalPriceTtc = round($finalPriceHt * (1+ $product->tax_rate/100), 2);
                        } else {
                            $finalPriceTtc = round($product->getPrice(true), 2);
                        }

                        if ($priceVariationQuantity > 0) {
                            if ($priceVariationType == "perc") {
                                if ($priceVariationDir == "more") {
                                    $finalPriceTtc = round($finalPriceTtc * (1 + $priceVariationQuantity / 100), 2);
                                } elseif ($priceVariationDir == "less") {
                                    $finalPriceTtc = round($finalPriceTtc * (1 - $priceVariationQuantity / 100), 2);
                                }
                            } else {
                                if ($priceVariationDir == "more") {
                                    $finalPriceTtc = round($finalPriceTtc + $priceVariationQuantity, 2);
                                } elseif ($priceVariationDir == "less") {
                                    $finalPriceTtc = round($finalPriceTtc - $priceVariationQuantity, 2);
                                }
                            }
                        }
                        $out .= '"'.$finalPriceTtc .'"'. $separator; // Price
                        $out .= '"'.$product->tax_rate .'"'. $separator;
                        $out .= '"'.$product->quantity .'"'. $separator;
                        // Categories
                        $categories = $product->getCategories();
                        $catSep = '"';
                        foreach ($categories as $idCat) {
                            $cat = new Category($idCat);
                            $out .= $catSep . $cat->name[$exportLangId];
                            $catSep = ",";
                        }
                        $out .= '"'.$separator;
                        // images
                        $images = $product->getImages($exportLangId);
                        $imgSep = '"';
                        $link = new Link();
                        foreach ($images as $img) {
                            $out .= $imgSep . $link->getImageLink(
                                $product->link_rewrite[$exportLangId],
                                $img['id_image'],
                                ImageType::getFormatedName($imageSize)
                            );
                            $imgSep = ",";
                        }
                        $out .= '"'.$separator;

                        // additional_delivery_times --> ou dans le product ?? Dans Stock ? sinon delivery_time de Stock
                        if (!empty($deleveryTime)) {
                            $out .= '"'.$deleveryTime .'"'. $separator;
                        } else {
                            $out .= '"'.'"'.$separator;
                        }
                        $out .= '"'.$product->condition .'"'. $separator;
                        $out .= '"'.$separateShippingStr .'"'. $separator;
                        $out .= '"'.$lastUpdate.'"';
                        $out .= PHP_EOL;
                        $cpt++;
                    } else {
                        // Compo
                        $compos = $product->getAttributesResume($exportLangId);
                        $composImg = $product->getCombinationImages($exportLangId);
                        foreach ($compos as $comp) {
                            $cpt++;
                            if ($comp["id_product_attribute"] == $idCompo) {
                                if ($comp["ean13"] != "") {
                                    $out .= '"'.$comp["ean13"].'"'.$separator;
                                } else {
                                    $out .= '"'.$product->ean13.'"'.$separator;
                                }
//                            if ($comp["upc"] != "") {
//                                $out .= $comp["upc"].$separator;
//                            } else {
//                                $out .= $product->upc.$separator;
//                            }
                                if ($comp["reference"] != "") {
                                    $out .= '"'.$comp["reference"].'"'.$separator;
                                } else {
                                    $out .= '"'.$product->reference.'"'.$separator;
                                }
                                $out .= '"'.$product->name[$exportLangId].'"'.$separator;
                                $out .= '"'.preg_replace("/\r|\n/", "", strip_tags($product->description[$exportLangId])).'"'.$separator;
                                $out .= '"'.preg_replace("/\r|\n/", "", strip_tags($product->description_short[$exportLangId])).'"'.$separator;
                                $out .= '"'."child".'"'.$separator;
                                $out .= '"'.$comp["attribute_designation"].'"'.$separator;
                                // $out .= '"'.$product->supplier_name.'"'.$separator;
                                $out .= '"'.$product->manufacturer_name.'"'.$separator;
                                $metaKey = "";
                                $sep = "";
                                if ($product->tags && in_array($exportLangId, $product->tags) && $product->tags[$exportLangId] != null && is_array($product->tags[$exportLangId])) {
                                    foreach ($product->tags[$exportLangId] as $key) {
                                        if ($key != "") {
                                            $metaKey .= $sep.$key;
                                            $sep = ",";
                                        }
                                    }
                                }
                                $out .= '"'.$metaKey.'"'.$separator;
                                $out .= '"'.$product->width.'"'.$separator;
                                $out .= '"'.$product->height.'"'.$separator;
                                $out .= '"'.$product->depth.'"'.$separator;
                                $out .= '"'.$product->weight.'"'.$separator;

                                // Price
                                // Case Dott Group
                                if (!empty($specificPrice) && $specificPrice['id_group'] == $dottGroup['id_group']) {
                                    $initialPriceHt = round($product->getPrice(false, $comp["id_product_attribute"]), 2);
                                    if ($specificPrice["reduction_type"] == "amount" && $specificPrice["price"] == -1) {
                                        $finalPriceHt = $initialPriceHt - $specificPrice["reduction"];
                                    } elseif ($specificPrice["reduction_type"] == "percentage" && $specificPrice["price"] == -1) {
                                        $finalPriceHt = $initialPriceHt * (1 - $product->specificPrice["reduction"]);
                                    } else {
                                        $finalPriceHt = $specificPrice["price"];
                                    }
                                    $finalPriceTtc = round($finalPriceHt * (1+ $product->tax_rate/100), 2);
                                } else {
                                    $finalPriceTtc = round($product->getPrice(true, $comp["id_product_attribute"]), 2);
                                }

                                $out .= '"'.$finalPriceTtc.'"'.$separator; // end price
                                $out .= '"'.$product->tax_rate.'"'.$separator;
                                $out .= '"'.$comp["quantity"].'"'.$separator;
                                // Categories
                                $categories = $product->getCategories();
                                $catSep = '"';
                                foreach ($categories as $idCat) {
                                    $cat = new Category($idCat);
                                    $out .= $catSep.$cat->name[$exportLangId];
                                    $catSep = ",";
                                }
                                $out .= '"'.$separator;
                                // images
                                if (sizeof($composImg[$comp["id_product_attribute"]]) > 0) {
                                    $images = $composImg[$comp["id_product_attribute"]];
                                } else {
                                    $images = $product->getImages($exportLangId);
                                }
                                $imgSep = '"';
                                $link = new Link();
                                foreach ($images as $img) {
                                    $out .= $imgSep.$link->getImageLink($product->link_rewrite[$exportLangId], $img['id_image'], ImageType::getFormatedName($imageSize));
                                    $imgSep = ",";
                                }
                                $out .= '"'.$separator;
                                // additional_delivery_times --> product ?? Stock ? sinon delivery_time de Stock
                                if (!empty($deleveryTime)) {
                                    $out .= '"'.$deleveryTime.'"'.$separator;
                                } else {
                                    $out .= '"'.'"'.$separator;
                                }
                                $out .= '"'.$product->condition.'"'.$separator;
                                $out .= '"'.$separateShippingStr.'"'.$separator;
                                $out .= '"'.$lastUpdate.'"';
                                $out .= PHP_EOL;
                                break;
                            }
                        }
                    }
                }

                // Write every 200 lines
                if ($cpt > 200) {
                    // $ansiOut = mb_convert_encoding($out, "Windows-1252");
                    // $ansiOut = mb_convert_encoding($out, "UTF-8");
                    $ansiOut = $out;
                    fwrite($catalogHandle, $ansiOut);
                    $out = "";
                    $cpt = 0;
                }
            }
            // $ansiOut = mb_convert_encoding($out, "Windows-1252");
            // $ansiOut = mb_convert_encoding($out, "UTF-8");
            $ansiOut = $out;
            fwrite($catalogHandle, $ansiOut);
            fclose($catalogHandle);
        } catch (Exception $e) {
            $this->errorMessage = "Error : ".$e->getMessage();
        }
        return true;
    }


    /**
     * @return mixed
     */
    public function getReturnMsg()
    {
        return $this->returnMsg;
    }

    /**
     * @param mixed $returnMsg
     */
    public function setReturnMsg($returnMsg)
    {
        $this->returnMsg = $returnMsg;
    }

    /**
     * @return mixed
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * @param mixed $errorMessage
     */
    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;
    }

    private function getDbProductList()
    {
        $res = Db::getInstance()->getRow("Select value from `"._DB_PREFIX_."lino_dott` where name='products_list'");
        if (is_array($res) && sizeof($res) > 0) {
            return $res["value"];
        }
        return "";
    }
}
