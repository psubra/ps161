<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the GNU General Public License, version 3 (GPL-3.0).
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * @author    Li-Nó Design www.lino-design.com <contact@lino-design.com>
 * @copyright 2019 Li-Nó Design Lda
 * @license   https://opensource.org/licenses/GPL-3.0 GNU General Public License version 3
 */

/**
 * Class CurlRequest
 * @package Emarketing
 */
class CurlRequest
{
    /**
     * @var false|resource|null
     */
    private $handle = null;

    private $authorization = null;

    private $params = null;

    /**
     * CurlRequest constructor.
     *
     * @param $url
     */
    public function __construct($url, $token)
    {
        $this->handle        = curl_init($url);
        $this->authorization = "Authorization: Bearer " . $token;
    }

    /**
     * @param $name
     * @param $value
     */
    public function setOption($name, $value)
    {
        curl_setopt($this->handle, $name, $value);
    }


    public function executeGet()
    {
        $this->setOption(CURLOPT_CUSTOMREQUEST, "GET");
        $this->setOption(CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: application/json; charset=utf-8',
            $this->authorization
        ));
        return $this->execute();
    }

    public function executePost()
    {
        $this->setOption(CURLOPT_CUSTOMREQUEST, "POST");
        $this->setOption(CURLOPT_POSTFIELDS, json_encode($this->params));
        $this->setOption(CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: application/json; charset=utf-8',
            $this->authorization
        ));

        return $this->execute();
    }
    public function executePut()
    {
        $this->setOption(CURLOPT_CUSTOMREQUEST, "PUT");
        $this->setOption(CURLOPT_POSTFIELDS, $this->params);
        $this->setOption(CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: text/plain; charset=utf-8',
            $this->authorization
        ));
        return $this->execute();
    }

    /**
     * @return array
     * @throws Exception
     */
    public function execute()
    {

        $this->setOption(CURLOPT_RETURNTRANSFER, true);
        $this->setOption(CURLOPT_FOLLOWLOCATION, 1);

        // Debug
        $this->setOption(CURLOPT_VERBOSE, true);

        // SSL Verification disabled
        $this->setOption(CURLOPT_SSL_VERIFYHOST, false);
        $this->setOption(CURLOPT_SSL_VERIFYPEER, false);

        $resp   = curl_exec($this->handle);
        $return = array(
          'body' => json_decode($resp),
          'code' => curl_getinfo($this->handle, CURLINFO_HTTP_CODE)
        );

        $curlError = curl_errno($this->handle);
        curl_close($this->handle);
        if ($curlError) {
            throw new Exception($curlError);
        }

        return $return;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function executeGetFile()
    {
        $this->setOption(CURLOPT_CUSTOMREQUEST, "GET");

        $this->setOption(CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: application/json; charset=utf-8',
            $this->authorization
        ));

        $this->setOption(CURLOPT_RETURNTRANSFER, true);
        $this->setOption(CURLOPT_FOLLOWLOCATION, 1);

        // Debug
        // $this->setOption(CURLOPT_VERBOSE, true);

        // SSL Verification disabled
        $this->setOption(CURLOPT_SSL_VERIFYHOST, false);
        $this->setOption(CURLOPT_SSL_VERIFYPEER, false);

        $resp   = curl_exec($this->handle);

        $curlError = curl_errno($this->handle);
        curl_close($this->handle);
        if ($curlError) {
            throw new Exception($curlError);
        }

        return $resp;
    }

    /**
     * @param $name
     *
     * @return mixed
     */
    public function getInfo($name)
    {
        return curl_getinfo($this->handle, $name);
    }

    /**
     *
     */
    public function close()
    {
        curl_close($this->handle);
    }

    /**
     * @return null
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param null $params
     */
    public function setParams($params)
    {
        $this->params = $params;
    }
}
